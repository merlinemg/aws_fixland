(function($) {
    $(document).ready(function($) {
        $('form').on('submit', function(event) {
            var status = $('#id_status').val();
            if (status && status === 'void') {
                if (confirm('Вы подтверждаете статус заказа void?')) {
                    return true;
                } else {
                    return false;
                }
            }
        }.bind($));
    })
})(django.jQuery)