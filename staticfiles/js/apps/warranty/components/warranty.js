/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import moment from 'moment';

import EmailInput from '../../common/components/email';
import PhoneInput from '../../common/components/phoneInput';
import ErrorMessage from '../../common/components/errorMessage';
import Loading from '../../common/components/loading';
import actions from '../actions';
import warrantyStore from '../store';
import orderStore from '../orderStore';


let OrdersTableRow = React.createClass({
    select() {
        this.props.select(this.props.order);
    },
    render() {
        const order = this.props.order;
        return (
            <tr>
                <td><span>{order.completed_at && moment(order.completed_at).format('YYYY-MM-DD')}</span></td>
                <td><span>{order.order_name}</span></td>
                <td>
                    <button className="button view-btn coverage-button" onClick={this.select}>{gettext('Выбрать')}</button>
                </td>
            </tr>
        )
    }
})

let OrdersTable = React.createClass({
    render() {
        return (
            <table>
                <tbody>
                    {this.props.orders.map(order => {
                        return <OrdersTableRow order={order} select={this.props.select}/>
                    })}
                </tbody>
            </table>
        )
    }
})


module.exports = React.createClass({
    actions: actions,
    mixins: [
        Reflux.connect(warrantyStore, 'store'),
        Reflux.connect(orderStore, 'orderStore'),
        Reflux.ListenerMixin,
        Router.Navigation
    ],
    componentWillMount() {
        actions.cleanOrder();
    },
    componentDidMount() {
        this.listenTo(actions.findOrders.completed, () => {
            this.setState({showOrders: true});
        });
        this.listenTo(actions.findOrders.failed, () => {
            this.transitionTo('orderServerProblem')
        });
        this.listenTo(actions.fillOrder.completed, () => {
            this.transitionTo('repairs');
        });
    },
    setFormItem(name, event) {
        actions.setFormItem(name, event.target.value);
    },
    setPhone(value) {
        actions.setFormItem('phone', value);
    },
    selectOrder(order) {
        actions.fillOrder(order);
    },
    submit() {
        this.setState({showOrders: false});
        actions.findOrders();
    },
    render() {
        let ajax = this.state.store.ajax;
        let {phone, email, errors, orders} = this.state.store;
        if (!errors) errors = {};
        return (
            <div className="content step">
                <h3>{gettext('Заявка на гарантийный ремонт')}</h3>
                <div className="wrapperDataInput">
                    <div className="wrpInptError identError">
                        <PhoneInput completed={this.setPhone} value={phone} error={errors.phone} />
                    </div>
                    <div className="wrpInptError identError">
                        <EmailInput onChange={this.setFormItem.bind(this, 'email')} value={email} error={errors.email} />
                    </div>
                </div>
                {ajax && <Loading />}
                {!ajax && <button className="button button-green center-button selectRepair" onClick={this.submit}>{gettext('Найти')}</button>}
                {this.state.showOrders && (
                    orders && orders.length ? (
                        <OrdersTable orders={orders} select={this.selectOrder}/>
                    ) : (
                        <ErrorMessage message={gettext('Не найденно')} />
                    )
                )}
            </div>
        )
    }
});
