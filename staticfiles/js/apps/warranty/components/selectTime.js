import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';

import DateTimePicker from '../../common/components/dateTimePicker';
import RepairsRequiredMixin from '../../repair/mixins';
import actions from '../actions';
import orderStore from '../orderStore';

let Link = Router.Link;


module.exports = React.createClass({
    mixins: [
        Reflux.connect(orderStore),
        Router.Navigation
    ],
    componentWillMount() {
        if (!this.state.userModel) this.transitionTo('warranty');
    },
    componentDidMount() {
        this.listenTo(actions.selectAsSoonAsPossible.completed, () => {
            this.transitionTo('selectAddress');
        });
    },
    getInitialState() {
        return {
            showDateTimePicker: false
        };
    },
    setAsSoonAsPossible () {
        actions.selectAsSoonAsPossible();
    },
    showDateTimePicker () {
        this.setState({
            showDateTimePicker: true
        });
    },
    render() {
        let showDateTimePicker = this.state.showDateTimePicker || this.state.startDateTime || this.state.endDateTime;
        let dateTimePickerProps = {
            startDateTime: this.state.startDateTime,
            endDateTime: this.state.endDateTime,
            setStartDateTime: actions.setStartDateTime,
            setEndDateTime: actions.setEndDateTime,
        }
        return (
            <div className='content step'>
                <h3>{gettext('К какому времени?')}</h3>
                <div className='wrapperselectTime'>
                    <button onClick={this.setAsSoonAsPossible} className='coverpage-button sellTimeBtn'>{gettext('Как можно скорее')}</button>
                    <span className='spanSeparator'>{gettext('или')}</span>
                    <button onClick={this.showDateTimePicker} className='coverpage-button sellTimeBtn'>{gettext('Указать удобное время')}</button>
                </div>
                {showDateTimePicker && <DateTimePicker {...dateTimePickerProps}/>}
                {this.state.startDateTime && this.state.endDateTime ? (
                    <Link to='selectAddress' className='button button-green center-button selectRepair slider-btn'>{gettext('Готово')}</Link>
                ) : <span></span>}
            </div>
        )
    }
});
