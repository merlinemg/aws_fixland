import React from 'react';
import Router from 'react-router';
import Reflux from 'reflux';

import store from '../orderStore';
import actions from '../actions';
import GetGeolocation from './getGeolocation';


import OrderStep2Mixin from '../../common/components/orderStep2Mixin';

module.exports = React.createClass({
    GetGeolocation: GetGeolocation,
    actions: actions,
    mixins: [
        Reflux.connect(store),
        Reflux.listenTo(store, 'storeTrigger'),
        Reflux.ListenerMixin,
        Router.Navigation,
        OrderStep2Mixin
    ],
    componentWillMount() {
        if (!this.state.userModel) this.transitionTo('warranty');
    },
    componentDidMount() {
        this.listenTo(actions.createWarantyOrder.completed, () => {
            this.transitionTo('orderSucess');
        });
        this.listenTo(actions.createWarantyOrder.failed, () => {
            this.transitionTo('orderServerProblem')
        });
    },
    storeTrigger(data) {
        if (data.addressUpdated) {
            actions.createWarantyOrder();
        }
    },
});
