/* globals gettext */

import actions from '../actions';
import React from 'react';

import SelectDeviceMixin from '../../common/components/selectDeviceMixin';

var SelectDevice = React.createClass({
    actions: actions,
    title: gettext('Что вам нужно починить?'),
    mixins: [SelectDeviceMixin]
});

module.exports = SelectDevice;
