/* globals gettext */

import _ from 'underscore';
import actions from '../actions';
import React from 'react';
import addons from 'react-addons';
import Reflux from 'reflux';
import Router from 'react-router';
import store from '../store';
import Loading from '../../common/components/loading';
import Money from '../../common/components/money.js';
import utils from '../utils';

var Link = Router.Link;


var CalculateCostButton = React.createClass({
    getParams() {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            color: this.props.params.color
        };
    },
    render() {
        return (<Link to='orderStep1' params={this.getParams()} className="sellRapair-btn floatRight">
            {gettext('Далее')}
        </Link>);
    }
});

var CalculateCostButtonNotActive = React.createClass({
    render() {
        return (<a to='orderStep1' className="sellRapair-btn sellRapair-btn-nonActive">
            {gettext('Далее')}
        </a>);
    }
});

var SelectRepair = React.createClass({
    mixins: [Reflux.connect(store)],
    componentWillMount() {
        actions.setDevice(this.getParams());
        if (this.props.params.model !== this.state.modelCache) {
            actions.cleanRepairs();
        }
        actions.setModelCache(this.props.params.model);

        var params = this.props.params;
        this.model = _.findWhere(_.findWhere(window.DEVICES, {slug: params.device}).models, {slug: params.model});
    },
    getRepairs(){
        return this.model.repairs
    },
    showAllRepairs() {
        actions.setShowAllDevices();
        this.setState({
            repairs: this.getRepairs()
        });
    },
    getParams() {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            color: this.props.params.color
        };
    },
    handleClick(repair) {
        actions.updateRepair(repair);
    },
    getTotal() {
        let grouped_repairs = this.model.grouped_repairs;
        if (!grouped_repairs.length) {
            return _.reduce(this.state.selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'');
        } else {
            let selectedRepairs = this.state.selectedRepairs;
            _.forEach(grouped_repairs, (grouped_repair) => {
                let items_in_group = _.filter(this.state.selectedRepairs, (item) => {
                    return grouped_repair.repair_list.indexOf(item.repair_id) != -1;
                });
                if (items_in_group.length === grouped_repair.repair_list.length) {
                    selectedRepairs = _.without(selectedRepairs, ...items_in_group);
                    selectedRepairs.push(grouped_repair);
                }
            });
            return _.reduce(selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'');
        }
    },
    render() {
        var selectedRepairIds = _.pluck(this.state.selectedRepairs, 'key');
        var repairs = this.getRepairs();
        if (!this.state.isAllDisplayed) {
            repairs = _.filter(repairs, repair => repair.priority > 50);
        }
        return (
            <div>
                <div className="content step wrpSelRepair">
                    <h3>{gettext('Что у вас сломалось?')}</h3>
                    <h5 className="descrSelRepair">{gettext('Цена включает в себя стоимость работ, запчастей, выезда мастера и гарантию на 3 года')}</h5>
                    {repairs.map(repair => {
                        var cx = addons.classSet;
                        var classes = cx({
                            deviceRepair: true,
                            selectBtn: true,
                            active: selectedRepairIds.indexOf(repair.key) !== -1
                        });
                        return (
                            <button onClick={this.handleClick.bind(this, repair)} className={classes}>{repair.name}
                                <div className='priceDiv'>
                                    <Money className="wrpSpanPrice" amount={utils.getRepairPrice(repair)}/>
                            </div></button>
                        );
                    })}
                    {!this.state.isAllDisplayed ? <button onClick={this.showAllRepairs} className="deviceRepair selectBtn">{gettext('Другие поломки')}</button> : <span className="noneSpan"></span>}
                </div>
                <div className='repairFooter'>
                    {selectedRepairIds.length ? (
                        <div>
                            <CalculateCostButton {...this.props} />
                            <p className='totalAmount'>{gettext('Итого')}:<span className='usd'> $ </span>{this.getTotal()}<span></span></p>
                        </div>
                    ) : <CalculateCostButtonNotActive /> }
                </div>
            </div>
        );
    }
});

module.exports = SelectRepair;
