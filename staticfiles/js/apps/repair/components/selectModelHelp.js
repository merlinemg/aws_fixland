import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';
import SelectModelHelpMixin from '../../common/components/selectModelHelpMixin';

var SelectModelHelp = React.createClass({
    actions: actions,
    nextRoute: 'selectColor',
    mixins: [Reflux.connect(store), SelectModelHelpMixin]
});

module.exports = SelectModelHelp;
