import actions from '../actions';
import React from 'react';
import Router from 'react-router';
import Reflux from 'reflux';
import _ from 'underscore';
import store from '../store';
import GetGeolocation from './getGeolocation';
import utils from '../utils';


import OrderStep2Mixin from '../../common/components/orderStep2Mixin';

module.exports = React.createClass({
    GetGeolocation: GetGeolocation,
    actions: actions,
    mixins: [
        Reflux.connect(store),
        Reflux.listenTo(store, 'storeTrigger'),
        Router.Navigation,
        OrderStep2Mixin
    ],
    storeTrigger(data) {
        if (data.addressUpdated) {
            const   newRegion = this.state.parsedAddress.region,
                    oldRegion = window.CLIENT_REGION.dadata_name
            if (newRegion !== oldRegion ) {
                if (_.find(this.state.selectedRepairs, (item) => {
                    return utils.getRepairPrice(item, newRegion) !== utils.getRepairPrice(item, oldRegion)
                })) {
                    this.transitionTo('regionDiff');
                    return;
                }
            }
            this.setState({sendingOrder: true});
            actions.placeOrder();
        }
    },
    componentWillMount() {
        if (!this.state.userName || !this.state.userPhone || !this.state.selectedRepairs.length || !(this.state.isSoonAsPossible === true || this.state.startDateTime || this.state.endDateTime)) {
            location.replace('/');
        }
    }
});
