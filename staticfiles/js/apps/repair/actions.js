import Reflux from 'reflux';

var actions = Reflux.createActions([
    'updateRepair',
    'getUserGeoLocation',
    'updateUserAddress',
    'setAsSoonAsPossible',
    'setOwnPhoneNumber',
    'setFriendPhoneNumber',
    'setUserName',
    'setUserPhone',
    'setUserEmail',
    'placeOrder',
    'cleanOrder',
    'setDevice',
    'setStartDateTime',
    'setEndDateTime',
    'setShowAllDevices',
    'setLastInputAddress',
    'setParsedAddress',
    'setParsedName',
    'cleanRepairs',
    'setModelCache',
    'checkCouponCode',
    'removeCoupon',
    'checkForRepeatedOrderCoupon',
    'saveComment',
]);

module.exports = actions;
