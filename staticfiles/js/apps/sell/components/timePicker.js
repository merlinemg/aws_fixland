import actions from '../actions';
import React from 'react';
import Reflux from 'reflux';
import store from '../store';

import TimePickerMixin from '../../common/components/timePickerMixin';

var TimePicker = React.createClass({
    actions: actions,
    mixins: [Reflux.connect(store), TimePickerMixin]
});

module.exports = TimePicker;
