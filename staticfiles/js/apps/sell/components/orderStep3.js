import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';

import OrderStep3Mixin from '../../common/components/orderStep3Mixin';


var OrderStep3 = React.createClass({
    actions: actions,
    mixins: [Reflux.connect(store), OrderStep3Mixin],
    componentWillMount() {
        if (!(this.state.userAddress !== '' && (this.state.isSoonAsPossible === true || this.state.startDateTime || this.state.endDateTime))) {
            location.replace('/');
        }
    }
});

module.exports = OrderStep3;
