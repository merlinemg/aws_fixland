/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import _ from 'underscore';
import BreadcrumbsMixin from '../../breadcrumbs/breadcrumbsMixin';

var Breadcrumbs = React.createClass({
    mixins: [Reflux.connect(store), BreadcrumbsMixin],
    getModelSell() {
        var sell, model, device;
        window.DEVICES.forEach( (deviceItem) => {
            deviceItem.models.forEach( (modelItem) => {
                var sellItem = _.findWhere(modelItem.sell, {key: this.state.modelSell});
                if (sellItem) {
                    sell = sellItem;
                    device = deviceItem;
                    model = modelItem;
                }
            });
        });
        return {device, model, sell};
    },
    getPrice() {
        return this.getModelSell().sell.price;
    },
    getFullDevice() {
        var params = this.props.params;
        var device, model, versionSlug, statusSlug, memorySlug;
        if (['orderStep1', 'orderStep2', 'orderStep3'].indexOf(this.props.route) !== -1) {
            var modelSell = this.getModelSell();
            device = modelSell.device;
            model = modelSell.model;
            versionSlug = modelSell.sell.version || '-';
            statusSlug = modelSell.sell.status;
            memorySlug = modelSell.sell.memory;
        } else {
            device = _.findWhere(window.DEVICES, {slug: params.device});
            model = _.findWhere(device.models, {slug: params.model});
            versionSlug = params.version;
            statusSlug = params.status;
            memorySlug = params.memory;
        }
        var version = _.findWhere(model.versions, {slug: versionSlug}),
            status = _.findWhere(window.STATUSES, {slug: statusSlug}),
            memory = _.findWhere(model.memory_list, {slug: memorySlug}),
            versionName = version ? version.name : '';
        return {
            title: gettext('Устройство'),
            text: `${[device.name, model.name, memory.name, versionName].join(' ')} (${status.name})`,
            link: `/${device.slug}/${model.slug}/${memorySlug}/${versionSlug}/${statusSlug}`
        };
    }
});

module.exports = Breadcrumbs;
