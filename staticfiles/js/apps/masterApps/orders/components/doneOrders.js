/* globals gettext */

import React from 'react';
import store from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Router from 'react-router';
import Loading from '../../../common/components/loading.js';
import Table from './ordersTable';

var Link = Router.Link;

var Orders = React.createClass({
    mixins: [Reflux.connect(store)],

    componentWillMount() {
        actions.getMasterDoneOrders();
    },

    render() {
        return (
            <div className='content step orderTable'>
                <h3>{gettext('Старые заказы')}</h3>
                <div className='container'>
                    {this.state.success ? <Table orders={this.state.orders} emptyDataText={gettext('У Вас еще нет выполненных заказов')}/> : <Loading />}
                    <Link to='orders' className='button button-blue center-button selectRepair' >{gettext('Мои заказы')}</Link>
                </div>
            </div>
        );
    }

});

module.exports = Orders;

