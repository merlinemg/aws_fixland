/* globals gettext */

import React from 'react';
import store from '../store';
import Reflux from 'reflux';
import Router from 'react-router';
import actions from '../actions';
import Table from './ordersTable';
import Loading from '../../../common/components/loading.js';

var Link = Router.Link;

var Orders = React.createClass({
    mixins: [Reflux.connect(store)],

    componentWillMount() {
        actions.getMasterOrders();
    },

    render() {
        return (
            <div className='content step orderTable'>
                <h3>{gettext('Мои заказы')}</h3>
                <div className='container'>
                    {this.state.success ? <Table orders={this.state.orders} emptyDataText={gettext('У вас нет заказов в работе')} /> : <Loading />}
                    <Link to='doneOrders' className="button button-blue center-button selectRepair" >{gettext('Старые заказы')}</Link>
                </div>
            </div>
        );
    }

});

module.exports = Orders;
