import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getMasterOrders',
    'getMasterOrder',
    'getMasterDoneOrders',
    'completeOrder',
    'rejectOrder',
    'cleanUpdatePartsAvailabilityId',
]);

module.exports = actions;
