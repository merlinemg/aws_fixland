/* globals  jQuery */

import Reflux from 'reflux';
import actions from './actions';
import Utils from '../../common/utils';
import _ from 'underscore';

var masterPlaces = [];
var cachedCities = {};
var cachedMetroStations = {};

var store = Reflux.createStore({
    listenables: [actions],
    savedMasterPlaces: [],
    toDelete: [],

    loadData() {
        jQuery.when(
            this.getMasterPlaces(),
            this.getRegions()
        ).then( () => {
            this.trigger({loaded: true});
        }, () => {
            this.trigger({error: true});
        });
    },

    getMasterPlaces() {
        let deferred = new jQuery.Deferred();
        let promises = [];
        Utils.ajaxGet({
            url: '/api/v1/master-places/'
        }).then((response) => {
            this.savedMasterPlaces = response;
            masterPlaces = [];
            _.each(this.savedMasterPlaces, (masterPlace) => {
                let place = _.findWhere(masterPlaces, {region: masterPlace.region});
                if (!place) {
                    masterPlaces.push({
                        region: masterPlace.region,
                        city: masterPlace.city ? [masterPlace.city] : [],
                        metro_stations: masterPlace.metro_stations,
                        all_metro: masterPlace.all_metro
                    });
                    let promise = this.getCitiesByRegion(masterPlace.region);
                    promises.push(promise);
                } else {
                    if (masterPlace.city) {
                        place.city.push(masterPlace.city);
                    }
                    if (masterPlace.metro_stations) {
                        place.metro_stations = _.union(place.metro_stations, masterPlace.metro_stations);
                    }
                }
                let promiseMetro = this.getMetroStationsByCity(masterPlace.city)
                promises.push(promiseMetro);
            });
            if (masterPlaces && masterPlaces.length) {
                this.trigger({masterPlaces: masterPlaces});
            } else {
                this.addPlace();
            }
            jQuery.when.apply(null, promises).then( () => {
                deferred.resolve(response);
            });
        }).fail(() => {
            deferred.reject();
        });
        return deferred;
    },

    getRegions() {
        let deferred = new jQuery.Deferred();
        Utils.ajaxGet({
            url: '/api/v1/regions/'
        }).then((response) => {
            this.trigger({regions: response});
            deferred.resolve(response);
        }).fail(() => {
            deferred.reject();
        });
        return deferred;
    },

    getCitiesByRegion(regionId) {
        let deferred = new jQuery.Deferred();
        if (cachedCities[regionId]) {
            this.trigger({cities: cachedCities});
        }
        Utils.ajaxGet({
            url: '/api/v1/cities/?region_id=' + regionId
        }).then((response) => {
            cachedCities[regionId] = response;
            this.trigger({cities: cachedCities});
            deferred.resolve(response);
        }).fail(() => {
            this.trigger({error: true, cities: cachedCities});
            deferred.reject();
        });
        return deferred;
    },

    getMetroStationsByCity(cities) {
        let deferred = new jQuery.Deferred();
        let promises = [];
        if (_.isNumber(cities)) {
            cities = [cities];
        }
        _.each(cities, (cityId) => {
            let deferred = new jQuery.Deferred();
            if (cachedMetroStations[cityId]) {
                this.trigger({metroStations: cachedMetroStations});
                return;
            }
            Utils.ajaxGet({
                url: '/api/v1/metro-stations/?city_id=' + cityId
            }).then((response) => {
                cachedMetroStations[cityId] = response;
                this.trigger({metroStations: cachedMetroStations});
                deferred.resolve(response);
            }).fail(() => {
                this.trigger({error: true, metroStations: []});
                deferred.reject();
            });
            promises.push(deferred);
        });
        jQuery.when.apply(null, promises).then( () => {
            deferred.resolve();
        });
        return deferred;
    },

    addPlace() {
        masterPlaces.push({});
        this.trigger({'masterPlaces': masterPlaces});
    },

    setSelectedFields(index, type, value, clearFields) {
        clearFields = (clearFields) ? clearFields : [];
        index = parseInt(index);
        if (type === 'region' && masterPlaces[index]['city']) {
            if (masterPlaces[index]['city'].length > 0) {
                _.each(masterPlaces[index]['city'], (cityId) => {
                    this.pushToDeleteIfSaved(cityId);
                });
            } else {
                let savedMasterPlace = _.findWhere(this.savedMasterPlaces, {region: masterPlaces[index].region});
                if (savedMasterPlace) {
                    this.toDelete.push(savedMasterPlace.id);
                    this.savedMasterPlaces.splice(this.savedMasterPlaces.indexOf(savedMasterPlace));
                }
            }
        } else if (type === 'city') {
            _.each(_.difference(masterPlaces[index][type], value), (cityId) => {
                this.pushToDeleteIfSaved(cityId);
                _.each(masterPlaces[index].metro_stations, (metroId, i)=> {
                    if (_.findWhere(cachedMetroStations[cityId], {id: metroId})) {
                        masterPlaces[index].metro_stations.splice(i, 1);
                    }
                });
            });
        }
        _.each(clearFields, (e) => {
            masterPlaces[index][e] = [];
        });
        masterPlaces[index][type] = value;
        this.trigger({success: false, error: false, masterPlaces: masterPlaces});
    },

    pushToDeleteIfSaved(cityId) {
        let savedMasterPlace = _.findWhere(this.savedMasterPlaces, {city: cityId});
        if (savedMasterPlace) {
            this.toDelete.push(savedMasterPlace.id);
            this.savedMasterPlaces.splice(this.savedMasterPlaces.indexOf(savedMasterPlace), 1);
        }
    },

    deleteMasterPlace(index) {
        let place = masterPlaces[parseInt(index)];
        if (!_.isEmpty(place)) {
            if (_.isNumber(place.city)) {
                this.pushToDeleteIfSaved(place.city);
            } else {
                if (place.city.length) {
                    _.each(place.city, (cityId) => {
                        this.pushToDeleteIfSaved(cityId);
                    });
                } else {
                    let savedMasterPlace = _.findWhere(this.savedMasterPlaces, {region: parseInt(place.region)});
                    if (savedMasterPlace) {
                        this.toDelete.push(savedMasterPlace.id);
                        this.savedMasterPlaces.splice(this.savedMasterPlaces.indexOf(savedMasterPlace), 1);
                    }
                }
            }
        }
        masterPlaces.splice(parseInt(index), 1);
        this.trigger({masterPlaces: masterPlaces});
    },

    saveMasterRegion(data) {
        let savedMasterPlace = null;
        if (this.savedMasterPlaces.length) {
            let savedRegionPlaces = _.filter(this.savedMasterPlaces, {region: parseInt(data.region)});
            if (savedRegionPlaces.length == 1 && !savedRegionPlaces[0].city) {
                savedMasterPlace = savedRegionPlaces[0];
            } else {
                savedMasterPlace = _.findWhere(this.savedMasterPlaces, {city: parseInt(data.city)});
            }
        }
        let deferred = new jQuery.Deferred()
        if (!savedMasterPlace) {
            Utils.ajaxPost({
                url: '/api/v1/master-places/',
                data: data
            }).then((response) => {
                this.savedMasterPlaces.push(response);
                deferred.resolve(response);
            }).fail(() => {
                deferred.reject();
                this.trigger({error: true});
            });
        } else {
            data.id = savedMasterPlace.id;
            this.savedMasterPlaces[this.savedMasterPlaces.indexOf(savedMasterPlace)] = data;
            if (!data.city) {
                data.city = null;
            }
            Utils.ajaxPut({
                url: `/api/v1/master-places/${data.id}`,
                data: data
            }).then((response) => {
                deferred.resolve(response);
            }).fail(() => {
                deferred.reject();
            });
        }
        return deferred;
    },

    saveMasterRegions() {
        let promises = [];
        _.each(masterPlaces, (place, placeIndex) => {
            if (!place.region) {
                return;
            }
            if (!place.city || !place.city.length || place.city.length === 1) {
                let data = jQuery.extend({}, place);
                data.city = place.city[0];
                if (data.all_metro) {
                    data.metro_stations = [];
                }
                let promise = this.saveMasterRegion(data);
                promises.push(promise);
                return;
            }
            _.each(place.city, (city) => {
                let data = jQuery.extend({}, place);
                data.city = city;
                let propperMetroStationList = [];

                propperMetroStationList = _.filter(place.metro_stations, (metro) => { // eslint-disable-line camelcase
                    return _.contains(_.pluck(cachedMetroStations[city], 'id'), metro);
                });
                if (!data.all_metro) {
                    data.metro_stations = propperMetroStationList; // eslint-disable-line camelcase
                } else if (cachedMetroStations[city] && cachedMetroStations[city].length) {
                    data.metro_stations = [];
                }
                let promise = this.saveMasterRegion(data, placeIndex);
                promises.push(promise);
            });
        });
        _.each(this.toDelete, (placeId, index) => {
            let deferred = new jQuery.Deferred()
            Utils.ajaxDelete({
                url: `/api/v1/master-places/${placeId}/`
            }).then(() => {
                this.toDelete.splice(index, 1);
                deferred.resolve();
            });
            promises.push(deferred);
        });
        if (promises.length) {
            jQuery.when.apply(null, promises).then( () => {
                this.trigger({
                    saved: true,
                    ajaxRequest: false
                });
            });
        } else {
            this.trigger({ajaxRequest: false});
        }
    }
});

module.exports = store;
