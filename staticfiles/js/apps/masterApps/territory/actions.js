import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getCitiesByRegion',
    'getMetroStationsByCity',
    'getRegions',
    'clearMetroStations',
    'addPlace',
    'getMasterPlaces',
    'setSelectedFields',
    'deleteMasterPlace',
    'saveMasterRegions'
]);

module.exports = actions;
