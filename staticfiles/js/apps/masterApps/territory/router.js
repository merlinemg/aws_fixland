import React from 'react';
import ReactRouter from 'react-router';
import MasterSetupTerritory from './components/masterSetupTerritory';


var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var signupRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='setup' path='/' handler={MasterSetupTerritory}/>
    </ReactRouter.Route>
);

module.exports = signupRoutes;
