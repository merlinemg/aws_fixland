/* globals jQuery gettext */
import React from 'react/addons';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';
import _ from 'underscore';
import ConfirmMessage from '../../../common/components/confirmMessage';
import ErrorMessage from '../../../common/components/errorMessage';
import Loading from '../../../common/components/loading.js';


var SearchRegion = React.createClass({
    getInitialState() {
        return {
            regions: []
        };
    },
    updateCities(e) {
        actions.setSelectedFields(this.props.index, 'region', e.target.value, ['metro_stations', 'city']);
        if (e.target.value) {
            actions.getCitiesByRegion(e.target.value, this.props.index);
        }
    },
    isRegionDisabled(region) {
        return _.find(this.props.placeList, (place) => {
            return parseInt(place.region) === region.id;
        }) ? 'disabled' : '';
    },
    render() {
        return (
            <div>
                <div className="one-half">
                    <label><h5 className="textLeft">{this.props.labelText}</h5></label>
                </div>
                <div className="two-half wrpFormInputs balanceInput">
                    <select name={this.props.name} className="inputData" id="selectRegion" onChange={this.updateCities} value={this.props.placeList[this.props.index].region}>
                        <option value=""> ---- </option>
                        {this.props.regions.map((region) => {
                            return (
                                <option value={region.id} disabled={this.isRegionDisabled(region)}>{region.name}</option>
                            );
                        })}
                    </select>
                </div>
            </div>
        );
    }
});


var SelectCities = React.createClass({
    getDefaultProps() {
        return {
            cities: []
        };
    },
    options: [],
    componentDidMount() {
        var elem = jQuery(`.chosen-select-city${this.props.index}`);
        elem.chosen({
            no_results_text: gettext('Не найденно'),
            width: '100%',
        });
        elem.on('change', function(evt, params) {
            let values = [];
            let e = jQuery(`.chosen-select-city${this.props.index}`);
            _.each(e.find('option'), (option) => {
                if (option.selected) {
                    values.push(parseInt(option.value));
                }
            });
            actions.getMetroStationsByCity(values);
            actions.setSelectedFields(this.props.index, 'city', values);
        }.bind(this));
    },

    componentDidUpdate(nextProps) {
        jQuery('body').click();
        if (this.props.cities != nextProps.cities) {
            jQuery(`.chosen-select-city${this.props.index}`).trigger('chosen:updated');
        }
    },
    render() {
        return (
            <div className="multipleSelWrp">
                <div className="one-half">
                    <label><h5 className="textLeft">{this.props.labelText}</h5></label>
                </div>
                <div className="two-half wrpFormInputs balanceInput ">
                    <select multiple={true} name={this.props.name} className={`inputData chosen-select-city${this.props.index}`} value={this.props.selectedPlace.city} data-placeholder={gettext('Все населенные пункты')}>
                        {this.props.cities.map((element) => {
                            return (
                                <option value={element.id}>{element.name}</option>
                            );
                        })}
                    </select>
                </div>
            </div>
        );
    }
});

var MetroStationsInput = React.createClass({
    options: [],
    updateOneColorLine(metro_id, name) {
        let metro = _.findWhere(this.props.metroStations, {id: parseInt(metro_id)});
        let elements = jQuery('.inputMerto .search-choice');
        let element = _.findWhere(elements, {innerText: name});
        jQuery(element).css({
            'background': '#' + metro.line_color,
            'color': '#fff'
        })
    },
    updateAllColorLine() {
        let e = jQuery(`.chosen-select-metro${this.props.index}`);
        _.each(e.find('option'), (option, index) => {
            if (option.selected) {
                this.updateOneColorLine(option.value, option.text);
            }
        });
    },
    componentDidMount() {
        var elem = jQuery(`.chosen-select-metro${this.props.index}`);
        elem.chosen({
            no_results_text: gettext('Не найденно'),
            width: '100%',
        });
        elem.on('change', function(evt, params) {
            let values = [];
            let e = jQuery(`.chosen-select-metro${this.props.index}`);
            e.parent().find('.chosen-container ul li input').blur();
            jQuery('body').click();
            _.each(e.find('option'), (option, index) => {
                if (option.selected) {
                    this.updateOneColorLine(option.value, option.text);
                    values.push(parseInt(option.value));
                }
            });
            actions.setSelectedFields(this.props.index, 'metro_stations', values);
        }.bind(this));
        this.updateAllColorLine();
    },
    componentDidUpdate(nextProps) {
        jQuery('body').click();
        if (this.props.metroStations.length != nextProps.metroStations.length) {
            jQuery(`.chosen-select-metro${this.props.index}`).trigger('chosen:updated');
        }
        let e = jQuery(`.chosen-select-metro${this.props.index}`);
        _.each(e.find('option'), (option, index) => {
            if (option.selected) {
                this.updateOneColorLine(option.value, option.text);
            }
        });
    },
    render() {
        return (
            <select multiple={true} name={this.props.name} className={`inputData chosen-select-metro${this.props.index}`} defaultValue={this.props.selectedPlace.metro_stations} data-placeholder=" ">
                {this.props.metroStations.map((element) => {
                    return (
                        <option value={element.id}>{element.name.replace(/(^|\s)метро(\s|$)/img, '')}</option>
                    );
                })}
            </select>
        )
    }
});

var SelectMetroStations = React.createClass({
    handleAllMetroClick(event) {
        actions.setSelectedFields(this.props.index, 'all_metro', !this.props.all_metro);
    },
    render() {
        return (
            <div className="multipleSelWrp">
                <div className="one-half">
                    <label><h5 className="textLeft">{this.props.labelText}</h5></label>
                </div>
                <div className="wrpFormInputs inputMerto">
                    {!this.props.all_metro && (
                        <div className="overflVis">
                            <MetroStationsInput {...this.props}/>
                        </div>
                    )}
                    <div className="checkboxMetroWrp">
                        <input ref="all_metro" type="checkbox" checked={this.props.all_metro} onClick={this.handleAllMetroClick}/>
                        <span>{gettext('Все станции метро')}</span>
                    </div>
                </div>
            </div>
        );
    }
});

var RegionForm = React.createClass({
    getDefaultProps() {
        return {
            cities: {},
            metroStations: {}
        };
    },
    deleteMasterPlace(index) {
        actions.deleteMasterPlace(index);
    },
    canDelete() {
        return this.props.placeList.length > 1;
    },
    getMetroStationsForSelectedCities(place) {
        return _.compact(_.flatten(_.map(place.city, (e) => { return this.props.metroStations[e]; })))
    },
    render() {
        let deleteIcon = {
            'backgroundRepeat': 'no-repeat',
            'backgroundSize': '18px 18px',
            'width': '18px',
            'cursor': 'pointer'
        };
        return (
            <div className="overflVis">
                {this.props.placeList.map((place, index) => {
                    return (
                        <div className="overflVis">
                            <br/>
                            {this.canDelete() && <div onClick={this.deleteMasterPlace.bind(this, index)} className="delete-list" style={deleteIcon}>&nbsp;</div>}
                            <SearchRegion
                                labelText={gettext('Регион')}
                                placeList={this.props.placeList}
                                regions={this.props.regions}
                                index={index} />
                            <SelectCities
                                labelText={gettext('Населенные пункты')}
                                dataType='city'
                                selectedPlace={place}
                                cities={this.props.cities[place.region]}
                                index={index} />
                            {this.getMetroStationsForSelectedCities(place).length > 0 && <SelectMetroStations
                                labelText={gettext('Станции метро')}
                                dataType="metro_stations"
                                selectedPlace={place}
                                metroStations={this.getMetroStationsForSelectedCities(place)}
                                all_metro={place.all_metro}
                                index={index} />}
                        </div>
                    );
                })}
            </div>
        );
    }
});

var MasterSetupTerritory = React.createClass({
    mixins: [Reflux.connect(store)],
    getInitialState() {
        return {
            'elementNumber': 1,
            'masterPlaces': [],
            'placeList': [],
            'success': false,
            'error': false
        };
    },
    savePlaces() {
        this.setState({ajaxRequest: true});
        actions.saveMasterRegions();
    },
    addRegion() {
        actions.addPlace();
    },
    componentDidMount() {
        store.loadData();
    },
    render() {
        return (
            <div className="step content">
                {(this.state.saved && !this.state.error)? <ConfirmMessage message={gettext('Территория успешно сохранена')} />: ''}
                {(this.state.error) ? <ErrorMessage message={gettext('Ошибка')} /> : ''}

                <h3>{gettext('Моя территория')}</h3>
                {this.state.loaded ? (
                    <form method="post" className="sideMarginForm territoryForm overflVis">
                        <div className="overflVis">
                            <RegionForm placeList={this.state.masterPlaces} regions={this.state.regions} cities={this.state.cities} metroStations={this.state.metroStations} updatedIndex={this.state.updatedIndex} />
                        </div>
                        <div>
                            <button onClick={this.addRegion} className='button button-blue center-button selectRepair' type='button'>{gettext('Еще регион')}</button>
                        </div>
                        <div>
                            {this.state.ajaxRequest ? <Loading /> : <button onClick={this.savePlaces} className='button button-green center-button selectRepair' type='button'>{gettext('Сохранить')}</button>}
                        </div>
                    </form>
                ) : <Loading />}
            </div>
        );
    }

});

module.exports = MasterSetupTerritory;
