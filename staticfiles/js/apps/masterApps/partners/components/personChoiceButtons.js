import React from 'react';
import store from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Router from 'react-router';

var Link = Router.Link;

var PersonChoiceButtons = React.createClass({
    render() {
        return (
            <div className='content'>
                <div className='container'>
                    <Link to='naturalPersonRequisites' className='button button-blue center-button selectRepair' >{gettext('Физическое лицо')}</Link>
                    <Link to='legalPersonRequisites' className='button button-blue center-button selectRepair' >{gettext('Юридическое лицо')}</Link>
                </div>
            </div>
        );
    }

});
module.exports = PersonChoiceButtons;