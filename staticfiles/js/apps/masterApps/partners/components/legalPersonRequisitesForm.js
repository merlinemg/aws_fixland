import React from 'react';
import {legalPersonStore} from '../store';
import Reflux from 'reflux';
import actions from '../actions';
import Router from 'react-router';
import Loading from '../../../common/components/loading.js';
import FormInput from '../components/formInput.js'

let store = legalPersonStore;

var LegalPersonRequisites = React.createClass({
    mixins: [Reflux.connect(store, 'store')],

    saveCheckboxValueToStore(name, event){
        actions.setFormFieldValue(name, event.target.checked);
    },
    handleInputBlur(name, event) {
        actions.setFormFieldValue(name, event.target.value)
    },
    submit(){
        actions.submit();
    },
    render() {
        let {errors} = this.state.store;
        if (!errors) errors = {};
        let form = this.state.form;
        return (
            <div className='content step wrpMasSign'>
                <h3>{gettext('Ввод реквизитов для юридического лица')}</h3>
                <div className='wrapperInputAddr'>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'organizationName')} placeholder='Наименование организации/ИП' id='organizationName' value={store.organizationName} error={errors.organizationName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'taxpayerId')} placeholder='ИНН' id='taxpayerId' value={store.taxpayerId} error={errors.taxpayerId}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'primaryStateRegistrationNumber')} placeholder='ОГРН' id='primaryStateRegistrationNumber' value={store.primaryStateRegistrationNumber} error={errors.primaryStateRegistrationNumber}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'phoneNumber')} placeholder='Телефон' ref='phoneComponent' inputRef='phoneNumber' id='phoneNumber' value={store.phoneNumber} error={errors.phoneNumber}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'email')} placeholder='E-mail' id='email' value={store.email} error={errors.email}/>
                    <h4>{gettext('Руководитель организации')}</h4>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'directorLastName')} placeholder='Фамилия' id='directorLastName' value={store.directorLastName} error={errors.directorLastName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'directorFirstName')} placeholder='Имя' id='directorFirstName' value={store.directorFirstName} error={errors.directorFirstName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'directorMiddleName')} placeholder='Отчество' id='directorMiddleName' value={store.directorMiddleName} error={errors.directorMiddleName}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'position')} placeholder='Должность' id='position' value={store.position} error={errors.position}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'charterOrPowerOfAttorney')} placeholder='Действует на основании устава или доверенности №' id='charterOrPowerOfAttorney' value={store.charterOrPowerOfAttorney} error={errors.charterOrPowerOfAttorney}/>
                    <h4>{gettext('Юридический адрес')}</h4>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalCountry')} placeholder='Страна' id='legalCountry' value={store.legalCountry} error={errors.legalCountry}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalRegion')} placeholder='Регион' id='legalRegion' value={store.legalRegion} error={errors.legalRegion}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalCity')} placeholder='Город' id='legalCity' value={store.legalCity} error={errors.legalCity}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalZipCode')} placeholder='Индекс' id='legalZipCode' value={store.legalZipCode} error={errors.legalZipCode}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalStreet')} placeholder='Улица' id='legalStreet' value={store.legalStreet} error={errors.legalStreet}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalHouse')} placeholder='Дом' id='legalHouse' value={store.legalHouse} error={errors.legalHouse}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'legalApartment')} placeholder='Квартира/Офис' id='legalApartment' value={store.legalApartment} error={errors.legalApartment}/>
                    <h4>{gettext('Фактический адрес')}</h4>
                    <span><input type="checkbox" onBlur={this.saveCheckboxValueToStore.bind(this, 'isTheSameAddress')}  id='isTheSameAddress' checked={store.isTheSameAddress}/> {gettext('Фактический адрес совпадает с юридическим')}</span>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualCountry')} placeholder='Страна' id='actualCountry' value={store.actualCountry} error={errors.actualCountry}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualRegion')} placeholder='Регион' id='actualRegion' value={store.actualRegion} error={errors.actualRegion}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualCity')} placeholder='Город' id='actualCity' value={store.actualCity} error={errors.actualCity}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualZipCode')} placeholder='Индекс' id='actualZipCode' value={store.actualZipCode} error={errors.actualZipCode}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualStreet')} placeholder='Улица' id='actualStreet' value={store.actualStreet} error={errors.actualStreet}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualHouse')} placeholder='Дом' id='actualHouse' value={store.actualHouse} error={errors.actualHouse}/>
                    <FormInput onBlur={this.handleInputBlur.bind(this, 'actualApartment')} placeholder='Квартира/Офис' id='actualApartment' value={store.actualApartment} error={errors.actualApartment}/>
                    <button className="button button-green center-button selectRepair" onClick={this.submit}>{gettext('Сохранить')}</button>
                </div>
            </div>
        );
    },
    componentDidMount() {
       jQuery(this.refs.phoneComponent.refs.phoneNumber.getDOMNode()).inputmask({
            mask: '+7(999) 999-9999'
        });
    }
});

module.exports = LegalPersonRequisites;


