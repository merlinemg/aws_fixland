import React from 'react';
import actions from '../actions';

import SelectModelMixin from '../../../common/components/selectModelMixin';

var SelectModel = React.createClass({
    actions: actions,
    nextRoute: 'parts',
    mixins: [SelectModelMixin]
});

module.exports = SelectModel;
