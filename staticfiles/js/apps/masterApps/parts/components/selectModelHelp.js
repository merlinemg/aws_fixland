import React from 'react';
import actions from '../actions';
import SelectModelHelpMixin from '../../../common/components/selectModelHelpMixin';

var SelectModelHelp = React.createClass({
    actions: actions,
    nextRoute: 'parts',
    mixins: [SelectModelHelpMixin]
});

module.exports = SelectModelHelp;
