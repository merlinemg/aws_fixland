/* global gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';

import _ from 'underscore';
import actions from '../actions';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import Table from '../../common/components/partOrderStep1Table.js';


module.exports = React.createClass({
    mixins: [Reflux.connect(store), Router.Navigation],
    componentWillMount() {
        actions.getMasterParts(this.props.params.model);
        actions.getModelParts(this.props.params.model);
    },
    getDeviceName() {
        var device = _.findWhere(window.DEVICES, {slug: this.props.params.device});
        var model = _.findWhere(device.models, {slug: this.props.params.model});
        return `${device.name} ${model.name}`;
    },
    toPartOrderStep2() {
        actions.cleanVendorsPartsCart();
        this.transitionTo('partOrderStep2', this.props.params);
    },
    isAnyChanged(){
        return this.state.partsCart.length ? true : false;
    },
    render() {
        return (
            <div className="content step wrpParts">
                <h3>{`${gettext('Заказ запчастей для')} ${this.getDeviceName()}`}</h3>
                { this.state.successMasterParts && this.state.successParts ? (
                    this.state.masterParts && this.state.parts ? (
                        <div>
                            <Table parts={this.state.parts} masterParts={this.state.masterParts} />
                            {this.isAnyChanged() && <button className="button button-green selectRepair" onClick={this.toPartOrderStep2}>{gettext('Проверить наличие')}</button>}
                        </div>
                    ) : <div>{gettext('Нет запчастей к данной модели')}</div>
                ) : <Loading />}
            </div>
        );
    }
});
