/* global gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';

import _ from 'underscore';
import actions from '../actions';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage.js';

import Table from '../../common/components/partOrderStep1Table.js';


module.exports = React.createClass({
    device: null,
    model: null,
    mixins: [Reflux.listenTo(store, 'storeTrigger'), Reflux.connect(store), Router.Navigation],
    componentWillMount() {
        actions.cleanStore();
        actions.getMasterOrder(this.props.params.order);
    },
    storeTrigger(data) {
        if (data.order) {
            this.device = _.findWhere(window.DEVICES, {key: data.order.device});
            this.model = _.findWhere(this.device.models, {key: data.order.model});
            actions.getMasterParts(this.model.slug);
            actions.getModelParts(this.model.slug);
        }
    },
    getDeviceName() {
        return this.state.order.device_model_color;
    },
    getNeedsParts() {
        let needs = [];
        _.forEach(this.state.order.parts, (item) => {
            needs.push({
                part: item.part.id,
                qty: item.quantity
            });
        });
        return needs;
    },
    toPartOrderStep2() {
        actions.cleanVendorsPartsCart();
        this.transitionTo('partOrderStep2', {
            device: this.device.slug,
            model: this.model.slug
        });
    },
    isAnyChanged(){
        return this.state.partsCart.length ? true : false;
    },
    render() {
        return (
            <div className="content step wrpParts">
                {this.state.successOrder ? (
                    <div>
                        <h3>{`${gettext('Заказ запчастей для')} ${this.getDeviceName()}`}</h3>
                        {this.state.successMasterParts && this.state.successParts ? (
                            this.state.masterParts && this.state.parts ? (
                                <div>
                                    <Table parts={this.state.parts} masterParts={this.state.masterParts} needs={this.getNeedsParts()} order={this.state.order} onlyNeeds={true}/>
                                    {this.isAnyChanged() && <button className="button button-green selectRepair" onClick={this.toPartOrderStep2}>{gettext('Проверить наличие')}</button>}
                                </div>
                            ) : <div>{gettext('Нет запчастей к данной модели')}</div>
                        ) : <Loading />}
                    </div>
                ) : (this.state.permissionDenied ? <ErrorMessage message={gettext('Нет доступа')}/> : <Loading />)}
            </div>
        );
    }
});
