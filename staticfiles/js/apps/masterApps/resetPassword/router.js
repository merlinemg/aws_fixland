import React from 'react';
import ReactRouter from 'react-router';
import ResetPassword from './components/resetPassword';
import ResetPasswordDone from './components/resetPasswordDone';
import OrderServerProblem from '../../common/components/orderServerProblem';



var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var resetPasswordRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='' path='/' handler={ResetPassword}/>
        <ReactRouter.Route name='done' path='/done' handler={ResetPasswordDone}/>
        <ReactRouter.Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
    </ReactRouter.Route>
);

module.exports = resetPasswordRoutes;
