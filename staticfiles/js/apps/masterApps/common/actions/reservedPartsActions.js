import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getReservedPartsForOrder',
    'cancelOrder',
    'load',
    'cancelOrderItem',
]);

module.exports = actions;
