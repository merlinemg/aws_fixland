import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getMaster',
    'saveAvatar',
    'getCurrentMaster',
    'saveName'
]);

module.exports = actions;
