import React from 'react';
import ReactRouter from 'react-router';

import signupRoutes from './signup/router';
import ordersRoutes from './orders/router';
import Login from './login/components/login';
import coursesRoutes from './courses/router';
import indexRoutes from './index/router';
import profileRoutes from './profile/router';
import resetPasswordRoutes from './resetPassword/router';
import partsRoutes from './parts/router';
import territoryRoutes from './territory/router';
import partnerRoutes from './partner/router';
import partnersRoutes from './partners/router';


window.partnersRoutes = partnersRoutes;
window.territoryRoutes = territoryRoutes;
window.partsRoutes = partsRoutes;
window.profileRoutes = profileRoutes;
window.partnerRoutes = partnerRoutes;
window.resetPasswordRoutes = resetPasswordRoutes;
window.indexRoutes = indexRoutes;
window.coursesRoutes = coursesRoutes;
window.ordersRoutes = ordersRoutes;
window.signupRoutes = signupRoutes;
window.Login = Login;
window.ReactRouter = ReactRouter;
window.React = React;
