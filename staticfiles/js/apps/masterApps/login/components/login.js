/* globals gettext */

import React from 'react';
import actions from '../actions';
import Reflux from 'reflux';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';
import PhoneInput from '../../../common/components/phoneInput';


var Login = React.createClass({
    mixins: [Reflux.connect(store)],

    validateForm() {
        if (this.validatePhone() && this.validatePassword()) {
            return true;
        } else {
            return false;
        }
    },

    validateField(field) {
        if (this.state[field] && !!this.state[field].length) {
            return true;
        } else {
            return false;
        }
    },

    validatePhone() {
        return this.validateField('phone') && (this.state.phone.length === 16);
    },

    validatePassword() {
        return this.validateField('password');
    },

    handlePasswordInputChange(event) {
        actions.setPassword(event.target.value);
    },

    login() {
        this.setState({errors: []});
        if (this.validateForm()) {
            this.setState({
                ajaxRequest: true
            });
            actions.login();
        } else {
            this.setState({
                ajaxRequest: false
            });
            this.setState({showError: true});
        }
    },

    render() {
        return (
            <div className="content step wrpLogin">
                <h3>Вход</h3>
                {this.state.errors && this.state.errors.map(error => {
                    return (
                        <ErrorMessage message={error} />
                    );
                })}
                <div className="wrapperDataInput">
                    <PhoneInput value={this.state.phone} showError={this.state.showError} validate={this.validatePhone} completed={actions.setPhone} />
                    <div className="wrpInptError">
                        <input ref="password" id = "password" type='password' defaultValue={this.state.password} onChange={this.handlePasswordInputChange} onClick={this.handlePasswordInputChange} placeholder='Пароль' className="inputData"/>
                            {this.state.showError && !this.validatePassword() ? <ErrorMessage message={gettext('Введите пароль')} /> : null}
                    </div>
                </div>
                <div className="wrapperDataInput login-buttons">
                    {this.state.ajaxRequest ? <div className="wrapLoading"><Loading /></div> : <div>
                        <div className="one-half">
                            <button className="button button-green selectRepair" onClick={this.login}>{gettext('Войти')}</button>
                        </div>
                        <div className="one-half last-column">
                            <a href="/reset-password" className="button button-blue selectRepair">{gettext('Востановить')}</a>
                        </div>
                    </div>}
                </div>
            </div>
        );
    }
});

module.exports = Login;
