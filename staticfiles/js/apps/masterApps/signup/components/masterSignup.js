/* globals jQuery, gettext */

import React from 'react';
import Router from 'react-router';
import actions from '../actions';
import Reflux from 'reflux';
import addons from 'react-addons';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';
import EmailInput from '../../../common/components/emailInput';
import PhoneInput from '../../../common/components/phoneInput';
import Utils from '../../../common/utils';


var MasterSignup = React.createClass({
    mixins: [Reflux.connect(store), Router.Navigation],

    componentWillMount() {
        actions.removeUserId();
    },

    componentDidMount() {
        var cityNode = jQuery(React.findDOMNode(this.refs.city));
        cityNode.suggestions({
            serviceUrl: 'https://dadata.ru/api/v2',
            token: window.DADATA_TOKEN,
            type: 'ADDRESS',
            triggerSelectOnSpace: false,
            onSelect: (suggestion) => {
                actions.setCity(suggestion.value, suggestion);
            }
        });

        var element = jQuery('.wrpMasSign').closest('#app').get(0);
        if (jQuery(element).hasClass('whiteBg')) {
            jQuery(element).removeClass('whiteBg');
        }
    },

    validateForm() {
        if (this.validateFirstName() && this.validateLastName() && this.validatePhone() && this.validateEmail() && this.validateCity() && this.validatePhone() && this.validatePassword() && this.validateOffer()) {
            return true;
        } else {
            return false;
        }
    },

    validateField(field) {
        if (this.state[field] && !!this.state[field].length) {
            return true;
        } else {
            return false;
        }
    },

    validateFirstName() {
        return this.validateField('firstName');
    },

    validateLastName() {
        return this.validateField('lastName');
    },

    validateEmail() {
        if (!!this.state.email.length && Utils.validateEmail(this.state.email)) {
            return true;
        } else {
            return false;
        }
    },

    validateCity() {
        return this.validateField('city');
    },

    validatePhone() {
        return this.validateField('phone') && (this.state.phone.length === 16);
    },

    validatePassword() {
        return this.validateField('password');
    },

    validateOffer() {
        return this.state.offer;
    },

    handleFirstNameInputChange(event) {
        actions.setFirstName(event.target.value);
    },

    handleLastNameInputChange(event) {
        actions.setLastName(event.target.value);
    },

    handleEmailInputChange(event) {
        actions.setEmail(event.target.value);
    },

    handleCityInputChange(event) {
        this.setState({
            ajaxRequest: true
        });
        actions.setCity(event.target.value);
    },

    handlePhoneInputChange(event) {
        actions.setPhone(event.target.value);
    },

    handlePasswordInputChange(event) {
        actions.setPassword(event.target.value);
    },

    handleCommentInputChange(event) {
        actions.setComment(event.target.value);
    },

    handleOfferclick(event) {
        event.preventDefault();
        if (!this.state.offer) {
            this.transitionTo('offer');
        } else {
            actions.setOffer(false);
        }
    },

    createMaster() {
        if (this.validateForm()) {
            this.setState({
                ajaxRequest: true
            });
            actions.createMaster();
        } else {
            this.setState({
                ajaxRequest: false
            });
            this.setState({showError: true});
        }
    },

    render() {
        var cx = addons.classSet;
        var emailClasses = cx({
            wrEm: true,
            errEm: this.state.showError && !this.validateEmail()
        });
        return (
            <div className="content step wrpMasSign">
                <h3>{window.is_partner
                    ?(gettext('Регистрация кандидата в партнеры'))
                    :(gettext('Регистрация кандидата в мастера'))}
                </h3>
                <div className="wrapperDataInput marBotInpt">
                    <div className="wrpInptError identError">
                        <input ref="firstName" type='text' defaultValue={this.state.firstName} onChange={this.handleFirstNameInputChange} placeholder={gettext('Как вас зовут?')} id = "firstName" className="inputData" />
                        {this.state.showError && !this.validateFirstName() ? <ErrorMessage message={gettext('Введите ваше имя')} /> : null}
                    </div>
                    <div className="wrpInptError identError">
                        <input ref="lastName" type='text' value={this.state.lastName} onChange={this.handleLastNameInputChange} placeholder={gettext('Фамилия')} id = "lastName" className="inputData" />
                        {this.state.showError && !this.validateLastName() ? <ErrorMessage message={gettext('Введите вашу фамилию')} /> : null}
                    </div>
                    <div className="wrpInptError wrpInputCity">
                        <input ref="city" type='text' defaultValue={this.state.city} onBlur={this.handleCityInputChange} placeholder={gettext('Город')} id = "city" className="inputData" />
                        {this.state.showError && !this.validateCity() ? <ErrorMessage message={gettext('Введите город')} /> : null}
                    </div>
                    <PhoneInput update={this.handlePhoneInputChange} value={this.state.phone} showError={this.state.showError} validate={this.validatePhone} completed={actions.setPhone} />
                </div>
                <div className={emailClasses}>
                    {this.state.errors.map(error => {
                        return (
                            <ErrorMessage message={error} />
                        );
                    })}
                    <EmailInput update={this.handleEmailInputChange} value={this.state.email} showError={this.state.showError} validate={this.validateEmail}/>
                </div>
                <div className="wrapperDataInput marTopInpt">
                    <div className="wrpInptError identError">
                        <input ref="password" id = "password" type='password' defaultValue={this.state.password} onChange={this.handlePasswordInputChange} onClick={this.handlePasswordInputChange} placeholder={gettext('Пароль')} className="inputData"/>
                            {this.state.showError && !this.validatePassword() ? <ErrorMessage message={gettext('Введите пароль')} /> : null}
                    </div>
                    <div className="identError">
                        <input ref="comment" id = "comment" type='comment' defaultValue={this.state.comment} onChange={this.handleCommentInputChange} onClick={this.handleCommentInputChange} placeholder={gettext('Коментарий')} className="inputData"/>
                    </div>
                    <div className="identError text-white">
                        <input ref="offer" id="offer" type="checkbox" checked={this.state.offer} onClick={this.handleOfferclick}/>
                        <span>{gettext('Я согласен с условиями оферты')}</span>
                        {this.state.showError && !this.validateOffer() && <ErrorMessage message={gettext('Подтвердите оферту')} />}
                    </div>
                </div>
                <div className="wrapperDataInput">
                    {this.state.ajaxRequest ? <div className="wrapLoading"><Loading /></div> : <button className="button button-green center-button selectRepair slider-btn" onClick={this.createMaster}>{gettext('Отправить')}</button>}
                </div>
            </div>
        );
    }
});

module.exports = MasterSignup;
