import React from 'react';
import ReactRouter from 'react-router';
import MasterSignup from './components/masterSignup';
import ConfirmAccount from './components/confirmAccount';
import Offer from './components/offer';
import Success from './components/success';
import OrderServerProblem from '../../common/components/orderServerProblem';


var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var signupRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='signup' path='/' handler={MasterSignup}/>
        <ReactRouter.Route name='offer' path='/offer' handler={Offer}/>
        <ReactRouter.Route name='confirmAccount' path='/confirm' handler={ConfirmAccount}/>
        <ReactRouter.Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
        <ReactRouter.Route name='Success' path='/success' handler={Success}/>
    </ReactRouter.Route>
);

module.exports = signupRoutes;
