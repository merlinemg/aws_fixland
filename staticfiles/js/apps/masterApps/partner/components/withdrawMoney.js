/* globals gettext */

import React from 'react'

module.exports = React.createClass({
    render() {
        return <h5>{gettext('Эта функция сейчас не доступна. Обратитесь к администратору')}</h5>
    }
});