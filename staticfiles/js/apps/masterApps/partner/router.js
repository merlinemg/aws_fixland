import React from 'react';
import ReactRouter from 'react-router';
import Balance from './components/balance';
import WithdrawMoney from './components/withdrawMoney';
import Transactions from './components/transactions';
import ServerProblem from '../../common/components/orderServerProblem';

let {Route, RouteHandler, Redirect} = ReactRouter;


let App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <div className="content step">
                <RouteHandler />
            </div>
        );
    }
});

let partnerRoutes = (
    <Route name='app' handler={App}>
        <Redirect from="/" to="/balance" />
        <Route name='balance' path='/balance' handler={Balance} />
        <Route name='transactions' path='/balance/transactions' handler={Transactions} />
        <Route name='withdrawMoney' path='/withdraw-money' handler={WithdrawMoney} />
        <Route name='serverProblem' path='/server-problem' handler={ServerProblem} />
    </Route>
);

module.exports = partnerRoutes;
