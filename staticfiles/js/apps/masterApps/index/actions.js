import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getMasterCanWork',
    'setMasterCanWork',
    'getMasterNewOrders',
    'getMasterNewOrder',
    'selectOrder'
]);

module.exports = actions;
