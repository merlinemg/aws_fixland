import Reflux from 'reflux';
import actions from './actions';
import reqwest from 'reqwest';
import Utils from '../../common/utils';

var store = Reflux.createStore({
    listenables: [actions],

    onGetMasterCanWork() {
        reqwest({
            url: '/api/v1/master-can-work/' + window.MASTER_ID + '/',
            method: 'get',
            contentType: 'application/json',
            type: 'json'
        }).then((response) => {
            if (response.can_work === true){
                actions.getMasterNewOrders();
            }
            this.trigger({success: true, canWork: response.can_work});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onSetMasterCanWork(value) {
        Utils.ajaxPut({
            url: '/api/v1/master-can-work/' + window.MASTER_ID + '/',
            data: {'can_work': value}
        }).then((response) => {
            if (value === true){
                actions.getMasterNewOrders();
            }
            this.trigger({success: true, canWork: response.can_work});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetMasterNewOrders() {
        reqwest({
            url: '/api/v1/repair-orders/',
            method: 'get',
            contentType: 'application/json',
            type: 'json',
            data: {
                status: 'new',
                type: 'new'
            }
        }).then((response) => {
            this.trigger({success: true, orders: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetMasterNewOrder(orderId) {
        reqwest({
            url: '/api/v1/repair-orders/' + orderId + '/',
            method: 'get',
            contentType: 'application/json',
            type: 'json',
            data: {
                status: 'new',
                type: 'new'
            }
        }).then((response) => {
            this.trigger({success: true, order: response});
        }).fail(() => {
            this.trigger({permissionDenied: true});
        });
    },

    selectOrder(orderId) {
        reqwest({
            url: '/select-repair-order/' + orderId + '/',
            method: 'get',
            contentType: 'application/json',
            type: 'json'
        }).then(() => {
            location.href = `/master/orders/#/order/${orderId}`;
        }).fail((response) => {
            var data = JSON.parse(response.response);
            if (data.result === 'no_money') {
                location.href = `/master/balance/?amount=${data.amount}`;
            } else if (data.result === 'order_error') {
                location.href = '/master/';
            } else {
                location.hash = '#/server-problem';
            }
        });
    }
});

module.exports = store;
