/* globals jQuery, gettext */

import React from 'react';
import ErrorMessage from '../../common/components/errorMessage';

var EmailInput = React.createClass({
    getDefaultProps() {
        return {
            value: '',
            showError: false,
            validate: () => {
                return true;
            },
            completed: () => {
                return true;
            }
        };
    },

    componentDidMount: function() {
        var that = this;
        jQuery(React.findDOMNode(this.refs.phone)).inputmask({
            mask: '+1(999) 999-9999',
            oncomplete: function(e) {
                that.props.completed(e.target.value);
            },
            onincomplete: function(e) {
                that.props.completed(e.target.value.replace('_', ''));
            }
        });
    },

    render() {
        return (
            <div className='wrpInptError'>
                <input ref='phone' defaultValue={this.props.value} type='text' placeholder={gettext('Телефон')} className='inputData inptTelephone' />
                {this.props.showError && !this.props.validate() ? <ErrorMessage message={gettext('Введите ваш номер телефона')} /> : null}
                { this.props.error && <ErrorMessage message={this.props.error} />}
            </div>
        );
    }
});

module.exports = EmailInput;
