/* globals gettext */

import React from 'react';
import addons from 'react-addons';
import Reflux from 'reflux';
import Router from 'react-router';
import store from '../store';
import _ from 'underscore';
import Loading from './loading';
import Money from './money.js';

let Link = Router.Link;

module.exports = React.createClass({
    componentWillMount() {
        this.selectedRepairIdsOnMount = _.pluck(this.props.selectedRepairs, 'key');
    },
    getInitialState() {
        return {
            isAllDisplayed: false
        }
    },
    getDefaultProps() {
        return {
            showSelectedOnly: false,
            showPrice: true,
            showAllRepairsTitle: gettext('Другие поломки')
        }
    },
    getRepairs(){
        return this.props.model.repairs;
    },
    showAllRepairs() {
        this.setState({
            isAllDisplayed: true,
            repairs: this.getRepairs()
        });
    },
    getParams() {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            color: this.props.params.color
        };
    },
    handleClick(repair) {
        this.props.repairSelected(repair);
    },
    render() {
        let selectedRepairIds = _.pluck(this.props.selectedRepairs, 'key');
        let repairs = this.getRepairs();
        if (!this.state.isAllDisplayed) {
            if (this.props.showSelectedOnly) {
                repairs = _.filter(repairs, repair => this.selectedRepairIdsOnMount.indexOf(repair.key) != -1);
            } else {
                repairs = _.filter(repairs, repair => repair.priority > 50);
            }
        }
        return (
            <div>
                {repairs.map(repair => {
                    let cx = addons.classSet;
                    let classes = cx({
                        deviceRepair: true,
                        selectBtn: true,
                        active: selectedRepairIds.indexOf(repair.key) !== -1
                    });
                    return (
                        <button onClick={this.handleClick.bind(this, repair)} className={classes}>{repair.name}
                            <div className='priceDiv'>
                                {this.props.showPrice && <Money className="wrpSpanPrice" amount={this.getRepairPrice(repair)}/>}
                            </div>
                        </button>
                    );
                })}
                {!this.state.isAllDisplayed ? <button onClick={this.showAllRepairs} className="deviceRepair selectBtn">{this.props.showAllRepairsTitle}</button> : <span className="noneSpan"></span>}
            </div>
        );
    },

    getRepairPrice(repair, region_name) {
        if (!region_name) region_name = window.CLIENT_REGION.dadata_name;
        let price = _.findWhere(repair.prices, {region_name: region_name});
        if (!price) {
            price = _.findWhere(repair.prices, {default: true});
        }
        return price ? price.price : 0;
    }
});
