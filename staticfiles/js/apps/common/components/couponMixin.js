/* globals gettext */

import React from 'react';
import addons from 'react-addons';
import _ from 'underscore';
import Loading from '../../common/components/loading';
import Money from '../../common/components/money.js';
import utils from '../../repair/utils';

module.exports = {
    /*
    Required attributes:
        - state
        - actions
    */
    componentDidMount() {
        this.setState({
            enterCode: this.state.coupon ? true : false,
            code: this.state.coupon ? this.state.coupon.code : ''
        });
    },
    enterCode() {
        this.setState({'enterCode': true});
    },
    handleCodeChange(event) {
        let code = event.target.value;
        if (!code || code.length < 6) {
            this.actions.removeCoupon();
            this.setState({code: null});
            return;
        }
        if (code === this.state.code) {
            return;
        }
        this.setState({ajaxRequest: true});
        this.setState({code: code});
        this.actions.checkCouponCode(code);
    },
    getPrice() {
        let model = _.findWhere(_.findWhere(window.DEVICES, {slug: this.state.userDevice}).models, {slug: this.state.userModel});
        let grouped_repairs = model.grouped_repairs;
        if (!grouped_repairs.length) {
            return _.reduce(this.state.selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0);
        } else {
            let selectedRepairs = this.state.selectedRepairs;
            _.forEach(grouped_repairs, (grouped_repair) => {
                let items_in_group = _.filter(this.state.selectedRepairs, (item) => {
                    return grouped_repair.repair_list.indexOf(item.repair_id) != -1;
                });
                if (items_in_group.length === grouped_repair.repair_list.length) {
                    selectedRepairs = _.without(selectedRepairs, ...items_in_group);
                    selectedRepairs.push(grouped_repair);
                }
            });
            return _.reduce(selectedRepairs, (memo, obj) => {return memo + utils.getRepairPrice(obj); }, 0);
        }
    },
    getCouponDiscount(){
        let price = this.getPrice();
        let couponDiscount = 0;
        if (this.state.coupon) {
            if (this.state.coupon.percent) {
                couponDiscount = price * (this.state.coupon.discount / 100);
            } else {
                couponDiscount = this.state.coupon.discount;
            }
        }
        return couponDiscount;
    },
    getRepeatedOrderCouponDiscount() {
        let price = this.getPrice();
        let repeatedOrderCouponDiscount = 0;
        if (this.state.repeatedOrderCoupon) {
            if (this.state.repeatedOrderCoupon.percent) {
                repeatedOrderCouponDiscount = price * (this.state.repeatedOrderCoupon.discount / 100);
            } else {
                repeatedOrderCouponDiscount = this.state.repeatedOrderCoupon.discount;
            }
        }
        return repeatedOrderCouponDiscount;
    },
    getPriceWithCoupon() {
        let price = this.getPrice();
        let couponDiscount = this.getCouponDiscount();
        let repeatedOrderCouponDiscount = this.getRepeatedOrderCouponDiscount();

        return couponDiscount > repeatedOrderCouponDiscount ? (price - couponDiscount) : price - repeatedOrderCouponDiscount;
    },
    render() {
        var cx = addons.classSet;
        var classes = cx({
            greenText: this.state.coupon || this.state.repeatedOrderCoupon
        });
        return (
            <div className='couponBlock'>
                {!this.state.enterCode && <button onClick={this.enterCode} className='button button-blue center-button selectRepair'>{gettext('Купон на скидку')}</button>}
                {this.state.enterCode && (
                    <div className='wrapperDataInput'>
                        <input ref='couponCode' type='text' className='inputData' maxLength='6' onChange={this.handleCodeChange} defaultValue={this.state.code} disabled={this.state.ajaxRequest ? 'disabled' : false} placeholder={gettext('Введите купон на скидку')}/>
                        {this.state.badCode && <span>{gettext('Неверный код')}</span>}
                    </div>
                )}
                {this.state.coupon && this.state.repeatedOrderCoupon && this.getCouponDiscount() < this.getRepeatedOrderCouponDiscount() && (
                    <span>{gettext('Используемая в заказе скидка выше, чем в введённом купоне')}</span>
                )}
                {this.state.ajaxRequest ? <Loading /> : (
                    (this.state.enterCode || this.state.repeatedOrderCoupon) && <h4>{gettext('Итого со скидкой')}: <span className={classes}><Money amount={this.getPriceWithCoupon()} currency='usd'/></span></h4>
                )}
            </div>
        )
    }
}
