/* globals gettext */

import React from 'react';
import Router from 'react-router';

var Link = Router.Link;

var Button = React.createClass({
    render() {
        return (
            <Link to='orderStep3' className='button button-green center-button selectRepair slider-btn'>{gettext('Готово')}</Link>
        );
    }
});

var OrderStep1Mixin = {
    /*
    Required attributes:
        - state
        - actions
        - TimePicker
    */
    setAsSoonAsPossible () {
        this.actions.setAsSoonAsPossible();
    },
    setShowTimePickers () {
        this.setState({
            showTimePickers: true
        });
    },
    getInitialState() {
        return {
            showTimePickers: false
        };
    },
    render() {
        return (
            <div className='content step'>
                <h3>{gettext('К какому времени?')}</h3>
                <div className='wrapperselectTime'>
                    <button onClick={this.setAsSoonAsPossible} className='coverpage-button sellTimeBtn'>{gettext('Как можно скорее')}</button>
                    <span className='spanSeparator'>{gettext('или')}</span>
                    <button onClick={this.setShowTimePickers} className='coverpage-button sellTimeBtn'>{gettext('Указать удобное время')}</button>
                </div>
                {this.state.showTimePickers || this.state.startDateTime || this.state.endDateTime ? <this.TimePicker /> : <span></span>}
                {this.state.startDateTime && this.state.endDateTime ? <Button {...this.props} /> : <span></span>}
            </div>
        );
    }
};

module.exports = OrderStep1Mixin;
