import Router from 'react-router';

var Link = Router.Link;

var SelectDeviceMixin = {
    /*
    Required attributes:
        - state
        - title
    */
    getInitialState() {
        return {
            devices: window.DEVICES
        };
    },
    render() {
        return (
            <div className='content step wrpDeviceName'>
                <h3 className='devNameh3'>{this.title}</h3>
                {this.state.devices.map(device => {
                    return (
                        <Link to='selectModel' params={{device: device.slug}} className='deviceName selectBtn'>{device.name}</Link>
                    );
                })}
            </div>
        );
    }
};

module.exports = SelectDeviceMixin;
