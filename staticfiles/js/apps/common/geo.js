/* globals google */

import RSVP from 'rsvp';

var Promise = RSVP.Promise;

var Navigator = {
    getUserCoordinate() {
        var promise = new Promise((resolve, reject) => {
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    (data) => {
                        resolve(data);
                    },
                    () => {
                        reject();
                    }
                );
            } else {
                reject();
            }
        });
        return promise;
    },
    getUserAddress() {
        var promise = new Promise((resolve, reject) => {
            this.getUserCoordinate().then((data) => {
                var lat = data.coords.latitude,
                    lon = data.coords.longitude;
                this.getAddressByCoordinates(lat, lon).then((resp) => {
                    resolve(resp.address);
                });
            }, () => {
                reject();
            });
        });
        return promise;
    },
    getAddressByCoordinates(lat, lon) {
        var promise = new Promise((resolve, reject) => {
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lon);
            geocoder.geocode({'latLng': latlng}, (results) => {
                if (results.length) {
                    resolve({address: results[0].formatted_address});
                } else {
                    reject();
                }
            });
        });
        return promise;
    }
};

module.exports = Navigator;
