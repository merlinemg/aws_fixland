''' Core celery tasks '''

import uuid
import tempfile
import urllib
from io import BytesIO
import logging as log
from datetime import datetime
import xml.etree.ElementTree as ET
from decimal import Decimal

from openpyxl import load_workbook
import xlrd
from django.core.mail import mail_admins
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string

from accounts.models import Region
from devices.models import DeviceModelRepair, DeviceModelRepairCommission, Part, VendorPart, Vendor, VendorWarehouse, PartArticle, DeviceModelRepairPrice, PartPrice
from devices.bl import get_vendor_by_email
from fixland.celery import celery_app
from constance import config
from masters.models import MasterStatus
from .utils import get_atachment_data


@celery_app.task
def test_error_task():
    ''' Task just for testing errors logging '''
    raise Exception('Test Error')


@celery_app.task
def task_import_data_from_onedrive():
    response = urllib.request.urlopen(config.ONEDRIVE_DATA_XLS_LINK)
    data = response.read()
    wb = load_workbook(filename=BytesIO(data), read_only=True)
    wb.data_only = True

    bronze = MasterStatus.objects.filter(name_ru='Бронзовый').first()
    silver = MasterStatus.objects.filter(name_ru='Серебряный').first()
    gold = MasterStatus.objects.filter(name_ru='Золотой').first()

    bronze_id = bronze.id if bronze else None
    silver_id = silver.id if silver else None
    gold_id = gold.id if gold else None

    import_report = {
        'created': 0,
        'updated': 0,
        'skipped': 0,
        'error': 0,
    }

    for worksheet in wb.worksheets:
        title = worksheet.title
        if 'Расчёт' not in title:
            continue
        region_dadata_name = title[7:]
        region = Region.objects.filter(dadata_name=region_dadata_name).first()
        if not region:
            log.warning('Region with dadata name: %s, does not exists', region_dadata_name)
            continue
        for index, row in enumerate(worksheet.rows):
            device = row[0].value
            repair = row[1].value
            price = row[2].value
            bronze_price = row[7].value
            silver_price = row[8].value
            gold_price = row[9].value
            if not (device and repair):
                import_report['skipped'] += 1
                log.warning('device and repair are missing at row %s', index + 1)
                continue
            device_model_repair = DeviceModelRepair.objects.filter(device_model__name=device, repair__name_ru=repair).first()
            if device_model_repair:
                prices = {}
                try:
                    prices[bronze_id] = float(bronze_price)
                    prices[silver_id] = float(silver_price)
                    prices[gold_id] = float(gold_price)
                    float(price)
                except ValueError:
                    import_report['error'] += 1
                    log.warning('prise is incorrect at row %s', index + 1)
                    continue
                obj, _ = DeviceModelRepairPrice.objects.update_or_create(
                    device_model_repair=device_model_repair,
                    region=region,
                    defaults=dict(price=price)
                )
                if _:
                    import_report['created'] += 1
                else:
                    import_report['updated'] += 1
                for status_id in prices.keys():
                    if not status_id:
                        continue
                    obj, _ = DeviceModelRepairCommission.objects.update_or_create(
                        model_repair=device_model_repair,
                        status_id=status_id,
                        region=region,
                        defaults=dict(commission=prices[status_id])
                    )
                    if _:
                        import_report['created'] += 1
                    else:
                        import_report['updated'] += 1
            else:
                log.warning('device_model_repair with model_name=%s name_ru= %s does not exist (row %s)', device, repair, index + 1)
                import_report['skipped'] += 1
    send_commission_import_result_email(import_report)


def clean_article(article):
    article = str(article)
    if len(article) == 5:
        article = '0' + article
    return article


@celery_app.task
def task_import_vendor_articles_from_onedrive():
    log.warning('Import vendor articles from onedrive started')
    response = urllib.request.urlopen(config.ONEDRIVE_ARTICLES_XLS_LINK)
    data = response.read()
    wb = load_workbook(filename=BytesIO(data), read_only=True)
    wb.data_only = True
    category = wb.worksheets[0]
    header = []
    for index, row in enumerate(category.rows):
        if not header:
            header = row
            vendors_dict = {i.value: get_vendor_by_email(i.value) for i in header[2:] if i.value}
            continue
        article = row[1].value
        if not article:
            log.warning('Empty article in row %s', index + 1)
            continue
        article = clean_article(article)
        name = row[0].value
        Part.objects.get_or_create(article=article, defaults=dict(name=name))
        for vendor_article in row[2:]:
            index = row.index(vendor_article)
            vendor_email = header[index].value
            if not vendor_email:
                continue
            vendor = vendors_dict[vendor_email]
            if not vendor:
                continue
            if vendor_article.value:
                vendor_article = clean_article(vendor_article.value)
                PartArticle.objects.update_or_create(
                    article=article,
                    vendor_id=vendor.id,
                    defaults=dict(vendor_article=vendor_article)
                )
            else:
                PartArticle.objects.filter(
                    article=article,
                    vendor_id=vendor.id,
                ).delete()
    log.warning('Import vendor articles from onedrive finished')


def get_data(file_type='excel'):
    vendor = Vendor.objects.filter(last_imported_at__isnull=False).order_by('-last_imported_at').first()
    last_imported_at = vendor.last_imported_at if vendor else None
    log.info('Import parts:: Last sync at %s', last_imported_at)
    try:
        data_list = get_atachment_data(file_type, last_imported_at)
    except Exception as exc:
        exc_id = uuid.uuid4()
        log.exception('Get attachemnt exception: %s', exc_id)
        send_import_error_email(vendor, exc, exc_id)
    real_data_list = [x for x in data_list]
    log.info('real_data_list: %s', real_data_list)
    return real_data_list


@celery_app.task
def task_import_parts_from_remote_excel_file():
    """
    Import vendor parts from attachemnt excel files sent via emails
    """
    log.warning('task_import_parts_from_remote_excel_file STARTED')
    real_data_list = get_data('excel')
    if not real_data_list:
        return
    for data in real_data_list:
        import_excel_file(data)


@celery_app.task
def task_import_parts_from_remote_xml_file():
    """
    Import vendor parts from attachemnt xml files sent via emails
    """
    log.info('task_import_parts_from_remote_xml_file STARTED')
    real_data_list = get_data('xml')
    if not real_data_list:
        return
    for data in real_data_list:
        import_xml_file(data)


def import_xml_file(data):
    vendor = Vendor.objects.filter(email=data['email']).first()
    if not vendor:
        log.warning('Vendor with email (%s) deos not exist', data['email'])
        return
    import_date = datetime.now()
    import_report = {
        'new_part': 0,
        'new_vendor_part': 0,
        'skipped': 0,
        'imported': 0,
        'error': 0,
    }
    root = ET.fromstring(data['data'])
    try:
        for item in root.iter('Товар'):
            article = item.attrib.get('Артикул')
            name = item.attrib.get('Наименование')
            warehouse_code = item.attrib.get('КодСклада')
            price = item.attrib.get('Цена').replace('\xa0', '')

            try:
                float(price)  # validate price
            except ValueError:
                continue

            part_article = PartArticle.objects.filter(vendor_article=article, vendor=vendor).first()
            if not part_article:
                log.info('Part article for vendor article (%s) does not exist', article)
                continue
            part = Part.objects.filter(article=part_article.article).first()
            if not part:
                log.info('Part with article (%s) does not exist, vendor article (%s)', part_article.article, article)
                continue
            if warehouse_code == 'НЕТ' or int(price) == 0:
                import_report['skipped'] += 1
                continue
            if not part.price or part.price > Decimal(price):
                part.price = price
                part.save()
            warehouse, created = VendorWarehouse.objects.get_or_create(vendor=vendor, code=warehouse_code)
            if warehouse.region:
                part_price = part.prices.filter(region=warehouse.region).first()
                if not part_price:
                    PartPrice.objects.create(
                        part=part,
                        region=warehouse.region,
                        price=price
                    )
                elif part_price.price > Decimal(price):
                    part_price.price = price
                    part_price.save()
            vendor_part = VendorPart.objects.filter(vendor=vendor, article=article, warehouse=warehouse).first()
            if not vendor_part:
                import_report['new_vendor_part'] += 1
                vendor_part = VendorPart()
            vendor_part.vendor = vendor
            vendor_part.part = part
            vendor_part.article = article
            vendor_part.warehouse = warehouse
            vendor_part.price = price
            vendor_part.name = name
            vendor_part.last_imported_at = import_date
            vendor_part.exists = True
            vendor_part.save()
            import_report['imported'] += 1
    except Exception as exc:
        exc_id = uuid.uuid4()
        log.exception('Parser Exception: %s', exc_id, extra={'data': data})
        send_import_error_email(vendor, exc, exc_id)
    else:
        Vendor.objects.filter(id=vendor.id).update(last_imported_at=import_date)
        # set unupdated files to unexist
        VendorPart.objects.filter(last_imported_at__lt=import_date, vendor=vendor).update(exists=False, last_imported_at=import_date)
        send_import_success_email(vendor, import_report)


def import_excel_file(data):
    vendor = Vendor.objects.filter(email=data['email']).first()
    if not vendor:
        log.warning('Vendor with email (%s) deos not exist', data['email'])
        return
    # TODO: parser values are fixed
    part_name_column_number = 1
    part_article_column_number = 2
    price_column_number = 3
    import_date = datetime.now()
    temp = tempfile.NamedTemporaryFile()
    import_report = {
        'new_part': 0,
        'new_vendor_part': 0,
        'skipped': 0,
        'imported': 0,
        'error': 0,
    }
    try:
        temp.write(data['data'])
        temp.seek(0)

        # parse
        rb = xlrd.open_workbook(temp.name)
        sheet = rb.sheet_by_index(0)
        for rownum in range(sheet.nrows):
            row = sheet.row_values(rownum)
            try:
                name = row[part_name_column_number].strip(' ')
                article = row[part_article_column_number]
                price = row[price_column_number]
                if not article or not price:
                    import_report['skipped'] += 1
                    continue

                try:
                    float(price)  # validate price
                except ValueError:
                    continue

                article = str(article).split('.')[0]

                # save to db
                part_article = PartArticle.objects.filter(vendor_article=article, vendor=vendor).first()
                if not part_article:
                    log.info('Part article for vendor article (%s) does not exist', article)
                    continue
                part = Part.objects.filter(article=part_article.article).first()
                if not part:
                    log.info('Part with article (%s) does not exist, vendor article (%s)', part_article.article, article)
                    continue

                vendor_part = VendorPart.objects.filter(vendor=vendor, article=article).first()
                if not vendor_part:
                    import_report['new_vendor_part'] += 1
                    vendor_part = VendorPart()
                vendor_part.vendor = vendor
                vendor_part.part = part
                vendor_part.article = article
                vendor_part.price = price
                vendor_part.name = name
                vendor_part.last_imported_at = import_date
                vendor_part.exists = True
                vendor_part.save()
                import_report['imported'] += 1
            except IndexError:
                import_report['error'] += 1
                log.warning('User set incorrect column number')
            except ValueError:
                import_report['error'] += 1
                log.warning('Incorrect article number (%s) of type %s' % (row[part_article_column_number], type(row[part_article_column_number])))
    except Exception as exc:
        exc_id = uuid.uuid4()
        log.exception('Parser Exception: %s', exc_id, extra={'data': data})
        send_import_error_email(vendor, exc, exc_id)
    else:
        Vendor.objects.filter(id=vendor.id).update(last_imported_at=import_date)
        # set unupdated files to unexist
        VendorPart.objects.filter(last_imported_at__lt=import_date, vendor=vendor).update(exists=False, last_imported_at=import_date)
        send_import_success_email(vendor, import_report)
    finally:
        temp.close()


def send_import_error_email(vendor, exc, exc_id):
    subject = _('Ошибка при импорте прайса поставщика {}').format(vendor.name)
    context = {
        'vendor': vendor,
        'exc': exc,
        'exc_id': exc_id,
    }
    message = render_to_string('emails/import/import_error.html', context)
    mail_admins(subject, message, fail_silently=True, html_message=message)


def send_import_success_email(vendor, import_report):
    subject = _('Успешный импорт запчастей поставщика {}').format(vendor.name)
    context = {
        'vendor': vendor,
        'import_report': import_report,
    }
    message = render_to_string('emails/import/import_success.html', context)
    mail_admins(subject, message, fail_silently=True, html_message=message)


def send_commission_import_result_email(import_report):
    subject = _('Результаты импорта комиссий')
    context = {
        'import_report': import_report,
    }
    message = render_to_string('emails/import/commission_import_result.html', context)
    mail_admins(subject, message, fail_silently=True, html_message=message)
