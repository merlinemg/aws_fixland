from django import forms
from django.utils.translation import ugettext as _


class ReverseTransactionForm(forms.Form):
    max_amount = None
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    amount = forms.DecimalField(max_digits=20, decimal_places=2, min_value=1, label=_('Сумма'))
    description = forms.CharField(max_length=255, widget=forms.Textarea, label=_('Описание'))

    def clean_amount(self):
        if self.max_amount < self.cleaned_data.get('amount'):
            raise forms.ValidationError(_('Введите меньше или равно {}'.format(self.max_amount)))
        return self.cleaned_data.get('amount')


class ImportMetroStationsForm(forms.Form):
    file = forms.FileField()
