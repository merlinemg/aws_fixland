# coding: utf-8
import io
import re
import csv
import logging
import email
import simplejson
import time
import base64
import urllib
from datetime import datetime

from constance import config
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail import mail_admins
from django.template.loader import render_to_string
from django.utils.timezone import now as utc_now
from django.utils.translation import ugettext_lazy as _
import requests

from django.utils.encoding import smart_str
import simplejson
import urllib
from urllib.request import urlopen
from urllib.parse import quote

import core
from .models import Transaction


log = logging.getLogger(__name__)


def send_html_email(subject, to, template, context):
    html_content = render_to_string(template, context)
    msg = EmailMultiAlternatives(subject, '', settings.DEFAULT_FROM_EMAIL, to)
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def clean_phone(phone):
    return re.sub("[^0-9\+]", "", phone)


def send_sms(to, text):
    if not config.SEND_SMS:
        return send_fake_sms(to, text)
    to = clean_phone(to)
    url = '{url}?id={id}&key={key}'.format(
        url=settings.BYTEHAND_URL_MULTI,
        id=settings.BYTEHAND_ID,
        key=settings.BYTEHAND_KEY
    )
    data = [{
        'to': to,
        'from': settings.BYTEHAND_FROM,
        'text': text,
    }]
    try:
        requests.post(url, json=data)
        log.warning('SUCCESS SEND SMS: to: %s, text: %s', to, text)
    except requests.exceptions.RequestException as e:
        log.exception("Send SMS exception: %s", e)


def send_fake_sms(to, text):
    message = 'Отправка смски на номер: {} с текстом: "{}"'.format(to, text)
    mail_admins('Fixland Fake SMS', message)


class GetMetroException(Exception):
    pass


def get_coordinates(address):
    payload = {
        'geocode': address,
        'format': 'json',
    }
    response = requests.get(settings.YANDEX_MAPS_API_URL, params=payload)
    if not response.ok:
        log.error('bad response: %s', response.content, extra={'response.content': response.content})
        raise GetMetroException('no response for get_coordinates')
    json = response.json()
    feature_member = json.get('response').get('GeoObjectCollection').get('featureMember')
    if not len(feature_member):
        raise GetMetroException('no feature_member')
    return feature_member[0].get('GeoObject').get('Point').get('pos')


def get_metro_by_address(address):
    metro_name=''
    try:
        AUTH_KEY = settings.GOOGLE_TRAIN_STATION_API_KEY
        LOCATION = str(address.lat) + "," + str(address.lon)
        TYPES = 'subway_station'
        MyUrl = ('https://maps.googleapis.com/maps/api/place/nearbysearch/json'
                 '?location=%s'
                 '&rankby=distance'
                 '&types=%s'
                 '&sensor=true&key=%s') % (LOCATION, TYPES, AUTH_KEY)

        response = urlopen(MyUrl).read()
        jsonData = simplejson.loads(response)
        for details in jsonData['results']:
            metro_name=details['name']
            break

        return {
            'name': metro_name,
            'line': '',
            'city': get_city_from_address(metro_name)
        }
    except Exception:
        log.error('Bad response data from yandex', extra={'address': address, 'raw': address.raw})


def fee_repair_order_comission(repair_order, master):
    commission = repair_order.get_master_commission(master)
    if not commission:
        return
    description = _('Списание комиссии за заказ #{}'.format(repair_order.number))
    own_money_commission_amount = 0
    credit_money_commission_amount = 0
    if master.balance > 0:
        if master.balance < commission:
            own_money_commission_amount = master.balance
            credit_money_commission_amount = commission - master.balance
        else:
            own_money_commission_amount = commission
    else:
        credit_money_commission_amount = commission
    transaction = None
    if own_money_commission_amount:
        transaction = Transaction.objects.create(
            sender=master.user,
            amount=own_money_commission_amount,
            currency=master.currency,
            status=Transaction.SUCCESS,
            description=description,
            content_object=repair_order,
            type=Transaction.REPAIR_ORDER_COMMISSION
        )
    if credit_money_commission_amount:
        if not master.take_credit_date:
            master.take_credit_date = utc_now()
            master.save()
        description = _('Списание комиссии за заказ #{}, с кредитных средств'.format(repair_order.number))
        Transaction.objects.create(
            sender=master.user,
            amount=credit_money_commission_amount,
            currency=master.currency,
            status=Transaction.SUCCESS,
            description=description,
            content_object=repair_order,
            type=Transaction.REPAIR_ORDER_COMMISSION,
            is_credit_money=True,
            parent=transaction
        )


def put_in_the_balance(user, wmi_order_id, amount, currency, description):
    Transaction.objects.create(
        recipient=user,
        amount=amount,
        wmi_order_id=wmi_order_id,
        currency=currency,
        status=Transaction.SUCCESS,
        description=description
    )


def get_array_item(array, key, value):
    result = None
    for item in array:
        if item.get(key) == value:
            result = item
            break
    return result


def get_atachment_data(file_type='excel', last_import_at=None):
    """
    :param last_import_at: date of las import
    :type last_import_at: datetime

    :returns: bytes -- atachment file data as bytes
    """
    if not config.MAILGUN_DOMAIN:
        log.exception('Mailgun domain should be set in constance field and')
        return
    res = requests.get("https://api.mailgun.net/v3/domains", auth=("api", config.MAILGUN_API_KEY))
    if res.status_code == 401:
        log.exception('Bad Mailgun API key')
        return
    if not len([i.get('name') for i in res.json().get('items', []) if i.get('name') == config.MAILGUN_DOMAIN]):
        log.exception('Mailgun domain and value in constance field are different')
        return

    if last_import_at:
        nowtuple = last_import_at.timetuple()
        nowtimestamp = time.mktime(nowtuple)
        date = email.utils.formatdate(nowtimestamp, localtime=True)
        res = requests.get("https://api.mailgun.net/v3/{}/events?event=stored&end={}".format(config.MAILGUN_DOMAIN, date), auth=("api", config.MAILGUN_API_KEY))
    else:
        res = requests.get("https://api.mailgun.net/v3/{}/events?event=stored".format(config.MAILGUN_DOMAIN), auth=("api", config.MAILGUN_API_KEY))
    if not res:
        log.error('Can\'t connect to mailgun %s', res)
        return
    res = res.json()
    if not res['items']:
        log.info('There no new email messages with attachment:')

    email_set = set()
    # get events
    for item in res['items']:
        if last_import_at and last_import_at.replace(tzinfo=None) >= datetime.utcfromtimestamp(item['timestamp']):
            continue
        email_url = item['storage']['url']
        # get store email
        res = requests.get(email_url, auth=("api", config.MAILGUN_API_KEY), headers={'Accept': 'message/rfc2822'})
        sender = res.json().get('sender')
        if sender in email_set:
            continue
        else:
            email_set.add(sender)
        message = email.message_from_string(res.json().get('body-mime'))

        # get atachment
        if isinstance(message.get_payload(), list):
            messages = [sub_email_message for sub_email_message in message.get_payload() if sub_email_message.get('Content-Disposition')]
        else:
            messages = [message]
        for msg in messages:
            if msg.get('Content-Disposition'):
                if msg.get('Content-Disposition', ';').split(';')[0] == 'attachment':
                    content_type = msg.get('Content-Type', ';').split(';')[0]
                    if (
                        file_type == 'excel' and content_type.find('application') != -1 and content_type.find('excel') != -1
                    ) or (
                        file_type == 'xml' and content_type.find('text') != -1 and content_type.find('xml') != -1
                    ):
                        log.info('Import file of "%s", (%s) with size %s bytes', sender, msg.get('Content-Type'), len(msg.get_payload()))
                        yield {'email': sender, 'data': base64.b64decode(msg.get_payload())}


def create_reverse_transaction(transaction, description=None, amount=None):
    if not description:
        prefix = 'кредитных ' if transaction.is_credit_money else ''
        description = 'Возврат {}денег по заказу {}'.format(prefix, getattr(transaction.content_object, 'number', 'unknown'))

    Transaction.objects.create(
        content_object=transaction.content_object,
        recipient=transaction.sender,
        description=description,
        amount=amount if amount else transaction.amount,
        status=Transaction.SUCCESS,
        type=Transaction.REVERSE,
        parent=transaction
    )


def csv_file_to_arr(file, delimiter=';', encoding='1251'):
    csv_arr = []
    f = io.StringIO(file.decode(encoding))
    reader = csv.reader(f, delimiter=delimiter)
    for row in reader:
        csv_arr.append(row)
    return csv_arr


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_city_from_address(address):
    url_quote = quote(smart_str(address))
    url = settings.GOOGLE_ADDRESS_API_URL % url_quote
    response = urlopen(url).read()
    results = simplejson.loads(response)
    get_results = results['results']
    city=''
    for result in get_results:
        for component in result["address_components"]:
            if 'locality' in component["types"]:
                city = component["long_name"]
    return city
