''' Admin side for core app '''

from django.contrib import admin
from django import forms
from simple_seo import load_view_names

from .admin_actions import reverse_transaction
from .models import LandingReview, LandingPicture, LandingFeature, MyMetaData, LandingLogo, Transaction, StaticText


class LandingReviewAdmin(admin.ModelAdmin):
    ''' Admin for LandingReview model '''

    list_display = ('id', 'short_review', 'first_name', 'last_name', 'position', 'company', 'visible')
    list_filter = ('visible', )
    list_editable = ('position', 'visible')
    ordering = ('position', )
    search_fields = ('first_name', 'last_name', 'review', 'company')


class LandingLogoAdmin(admin.ModelAdmin):
    ''' Admin for LandingLogo model '''

    list_display = ('id', 'name', 'position', 'visible')
    list_filter = ('visible', )
    list_editable = ('position', 'visible')
    ordering = ('position', )
    search_fields = ('name', )


class LandingPictureAdmin(admin.ModelAdmin):
    ''' Admin for LandingPicture model '''

    list_display = ('title', 'description', 'visible')
    list_filter = ('visible', )
    list_editable = ('visible', )
    search_fields = ('title', 'description')


class LandingFeatureAdmin(admin.ModelAdmin):
    ''' Admin for LandingFeature '''

    list_display = ('title', 'description', 'visible', 'position')
    list_filter = ('visible', )
    list_editable = ('visible', 'position')
    search_fields = ('title', 'description')


class BaseMetadataForm(forms.ModelForm):
    view_name = forms.ChoiceField(choices=load_view_names())


class MyMetaDataAdmin(admin.ModelAdmin):
    list_display = ['view_name', 'title']
    exclude = []
    form = BaseMetadataForm

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(MyMetaDataAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'title':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        if db_field.name == 'description':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        if db_field.name == 'keywords':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        if db_field.name == 'og:title':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        if db_field.name == 'og:description':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        if db_field.name == 'twitter:title':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        if db_field.name == 'twitter:description':
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        return formfield


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('sender', 'recipient', 'amount', 'description', 'type', 'created_at')
    list_display_links = ('sender', 'recipient', )
    raw_id_fields = ('sender', 'recipient',)
    autocomplete_lookup_fields = {
        'fk': ['sender', 'recipient'],
    }
    actions = [reverse_transaction]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ('content_type', 'object_id', 'type', 'wmi_order_id', 'sender', 'recipient', 'amount', 'currency', 'status', 'description', 'created_at', )
        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(LandingReview, LandingReviewAdmin)
admin.site.register(LandingPicture, LandingPictureAdmin)
admin.site.register(LandingFeature, LandingFeatureAdmin)
admin.site.register(MyMetaData, MyMetaDataAdmin)
admin.site.register(LandingLogo, LandingLogoAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(StaticText)
