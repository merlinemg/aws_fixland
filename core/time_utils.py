from datetime import datetime
import pytz
import calendar


def utc_now():
    return datetime.now(pytz.utc)


def datetime_to_unix_time(datetime):
    return calendar.timegm(datetime.timetuple())


def unix_utc_now():
    return datetime_to_unix_time(utc_now())
