import binascii
import urllib
from collections import defaultdict
from hashlib import md5
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.contrib.sites.models import get_current_site
from accounts.models import User
from .models import Transaction

from . import utils


def get_signature(params, secret_key):
    """
    Base64(Byte(MD5(Windows1251(Sort(Params) + SecretKey))))
    params - list of tuples [('WMI_CURRENCY_ID', 643), ('WMI_PAYMENT_AMOUNT', 10)]
    """
    icase_key = lambda s: str(s).lower()

    lists_by_keys = defaultdict(list)
    for key, value in params.items():
        lists_by_keys[key].append(value)

    str_buff = ''
    for key in sorted(lists_by_keys, key=icase_key):
        for value in sorted(lists_by_keys[key], key=icase_key):
            str_buff += str(value)
    str_buff += secret_key
    md5_string = md5(str_buff.encode('1251')).digest()
    return binascii.b2a_base64(md5_string)[:-1]


def get_currency_by_id(currency_id):
    if int(currency_id) == 840:
        return 'usd'
    return 'rub'


def get_walletone_answer(result, description):
    return 'WMI_RESULT={}&WMI_DESCRIPTION={}'.format(result.upper(), urllib.parse.quote_plus(description)).encode('utf8')


def confirm_payment(data):
    if not data.get('WMI_SIGNATURE'):
        return get_walletone_answer('Retry', 'Отсутствует параметр WMI_SIGNATURE')
    if not data.get('WMI_ORDER_STATE'):
        return get_walletone_answer('Retry', 'Отсутствует параметр WMI_ORDER_STATE')
    if not data.get('SENDER_ID'):
        return get_walletone_answer('Retry', 'Отсутствует параметр SENDER_ID')
    params = {}
    for name, value in data.items():
        if name != 'WMI_SIGNATURE':
            params[name] = value
    if data.get('WMI_SIGNATURE') == get_signature(params, settings.W1_SECRET_KEY).decode('utf8'):
        if (data.get('WMI_ORDER_STATE').upper() == 'ACCEPTED'):
            user = User.objects.get(id=data.get('SENDER_ID'))
            currency = get_currency_by_id(data.get('WMI_CURRENCY_ID'))
            description = '{} - {} / {}'.format(_('Пополнение счета'), user.get_full_name(), user.id)
            utils.put_in_the_balance(user, data.get('WMI_ORDER_ID'), data.get('WMI_PAYMENT_AMOUNT'), currency, description)
            return get_walletone_answer('Ok', description)
        else:
            return get_walletone_answer('Retry', 'Неверное состояние {}'.format(data.get('WMI_ORDER_STATE')));
    else:
        return get_walletone_answer('Retry', 'Неверная подпись {}'.format('WMI_SIGNATURE'));


def get_payment_form_context(request, amount):
    user = request.user
    context = {
        'SENDER_ID': user.id,
        'WMI_MERCHANT_ID': settings.WMI_MERCHANT_ID,
        'WMI_PAYMENT_AMOUNT': amount,
        'WMI_CURRENCY_ID': user.master.currency_code,
        'WMI_DESCRIPTION': "Пополнение счета, {}".format(user.get_full_name()),
        'WMI_SUCCESS_URL': 'http://{}{}'.format(get_current_site(request).domain, reverse('payment_success')),
        'WMI_FAIL_URL': 'http://{}{}'.format(get_current_site(request).domain, reverse('payment_fail')),
    }
    context['WMI_SIGNATURE'] = get_signature(context, settings.W1_SECRET_KEY)
    return context
