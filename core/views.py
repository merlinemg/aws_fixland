from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _

from core.w1 import confirm_payment
from .models import StaticText
from .forms import ImportMetroStationsForm
from .bl import update_metro_stations_from_file
from core.tasks import task_import_data_from_onedrive, task_import_parts_from_remote_excel_file, task_import_parts_from_remote_xml_file, task_import_vendor_articles_from_onedrive
from orders.models import RepairOrder
from orders import bl as orders_bl


@csrf_exempt
@require_http_methods(["POST"])
def payment_notify(request):
    response = confirm_payment(request.POST)
    return HttpResponse(response)


def offer(request):
    offer = StaticText.objects.filter(type=StaticText.TYPE_OFFER).first()
    return HttpResponse(offer.text if offer else 'No Offer')


@user_passes_test(lambda u: u.is_superuser)
def import_data_from_onedrive(request):
    task_import_data_from_onedrive()
    return redirect(reverse('admin:index'))


@user_passes_test(lambda u: u.is_superuser)
def import_vendor_articles_from_onedrive(request):
    task_import_vendor_articles_from_onedrive.delay()
    messages.add_message(request, messages.SUCCESS, _('Импорт запущен'))
    return redirect(reverse('admin:index'))


@user_passes_test(lambda u: u.is_superuser)
def import_parts_from_remote_excel_file(request):
    task_import_parts_from_remote_excel_file.delay()
    return redirect(reverse('admin:index'))


@user_passes_test(lambda u: u.is_superuser)
def import_parts_from_remote_xml_file(request):
    task_import_parts_from_remote_xml_file.delay()
    return redirect(reverse('admin:index'))


@user_passes_test(lambda u: u.is_superuser)
def recalculate_masters_rating(request):
    from masters.tasks import update_rating_for_all_masters
    update_rating_for_all_masters.delay()
    return redirect(reverse('admin:index'))


@user_passes_test(lambda u: u.is_superuser)
def fix_masters_rating(request):
    from masters.tasks import update_master_rating_from_rating_logs
    update_master_rating_from_rating_logs.delay()
    return redirect(reverse('admin:index'))


def notify_masters_about_new_order(request, repair_order_id):
    repair_order = get_object_or_404(RepairOrder, id=repair_order_id)
    if repair_order.notified_masters.exists():
        messages.add_message(request, messages.ERROR, _('Рассылка уже была запущена'))
    else:
        orders_bl.notify_masters_about_new_order(repair_order)
        messages.add_message(request, messages.SUCCESS, _('Рассылка запущена повторно'))
    return redirect(reverse('admin:orders_repairorder_change', args=[repair_order_id]))


@user_passes_test(lambda u: u.is_superuser)
def import_metro_stations(request):
    if request.method == 'POST':
        form = ImportMetroStationsForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.cleaned_data['file']
            result = update_metro_stations_from_file(file)
            message = _('Импорт успешно завершен, добавлено город: {created_city}, линия метро: {created_line}, метро: {created_metro}').format(
                created_city=result.get('created_city'),
                created_line=result.get('created_line'),
                created_metro=result.get('created_metro')
            )
            messages.success(request, message)
    else:
        form = ImportMetroStationsForm()
    return render(request, 'admin/import_metro_stations.html', {'form': form})
