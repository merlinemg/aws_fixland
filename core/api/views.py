from rest_framework import viewsets
from django.db.models import Q

from orders.models import RepairOrder

from .serializers import TransactionSerializer
from .permissions import TransactionPermissions
from ..models import Transaction


class TransactionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Transaction.objects
    serializer_class = TransactionSerializer
    permission_classes = (TransactionPermissions,)

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        date_from = self.request.query_params.get('date_from', None)
        date_to = self.request.query_params.get('date_to', None)
        if date_from:
            self.queryset = self.queryset.filter(created_at__gte=date_from)
        if date_to:
            self.queryset = self.queryset.filter(created_at__lt=date_to)
        if user.is_master():
            void_orders = RepairOrder.objects.filter(master=user.master)
        else:
            void_orders = RepairOrder.objects.filter(partner=user.partner)
            self.queryset = self.queryset.filter(type=Transaction.PARTNER_COMMISSION)
        void_orders = void_orders.filter(status=RepairOrder.STATUS_VOID).values_list('id', flat=True)
        return self.queryset.filter(
            Q(sender=user) | Q(recipient=user),
            status=Transaction.SUCCESS
        ).exclude(Q(object_id__isnull=False) & Q(object_id__in=void_orders)).order_by('-created_at')
