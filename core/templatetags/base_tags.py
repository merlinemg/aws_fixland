import re
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils import translation
from django.utils.encoding import force_text

register = template.Library()


@register.simple_tag(takes_context=True)
def active_menu(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname
    path = context['request'].path
    if re.search(pattern, path):
        return 'active-menu'
    return ''


@register.filter()
def is_repair_commission_model(context):
    return context.formset.model.__name__ == 'DeviceModelRepairCommission'


@register.filter()
def is_repair_part_model(context):
    return context.formset.model.__name__ == 'DeviceModelRepairPart'


@register.filter()
def is_language_choosen(request):
    return True if request.session.get(translation.LANGUAGE_SESSION_KEY, None) else False


@register.filter(is_safe=True)
def intcomma(value):
    orig = force_text(value)
    new = re.sub("^(-?\d+)(\d{3})", '\g<1>\'\g<2>', orig)
    if orig == new:
        return new
    else:
        return intcomma(new)


@register.filter()
def places_from_decimal(value):
    return str(value - int(value))[2:] or 0
