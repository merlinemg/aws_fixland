import factory
from django.conf import settings

from .models import Transaction


class TransactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Transaction

    currency = settings.CURRENCY_RUB
    status = Transaction.SUCCESS


W1_SUCCESS_POST_DATA = {
    'WMI_AUTO_ACCEPT': '1',
    'WMI_COMMISSION_AMOUNT': '0.00',
    'WMI_CREATE_DATE': '2015-07-09 13:39:50',
    'WMI_CURRENCY_ID': '643',
    'WMI_DESCRIPTION': 'Test payment',
    'WMI_EXPIRED_DATE': '2015-08-09 13:39:50',
    'WMI_EXTERNAL_ACCOUNT_ID': '380978623406',
    'WMI_FAIL_URL': 'http://localhost/en/master/payment_fail/',
    'WMI_LAST_NOTIFY_DATE': '2015-07-10 11:45:43',
    'WMI_MERCHANT_ID': '123227883520',
    'WMI_NOTIFY_COUNT': '27',
    'WMI_ORDER_STATE': 'Accepted',
    'WMI_ORDER_ID': '344688379096',
    'WMI_PAYMENT_TYPE': 'Privat24UAH',
    'WMI_SUCCESS_URL': 'http://localhost/en/master/payment_success/',
    'WMI_UPDATE_DATE': '2015-07-09 13:42:36',
}
