# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20150730_1753'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='is_credit_money',
            field=models.BooleanField(default=False, help_text='Оплата проводилась кредитными стредствами?', verbose_name='Кредитные?'),
        ),
    ]
