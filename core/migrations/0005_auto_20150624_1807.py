# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simple_seo', '__first__'),
        ('core', '0004_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='MetaData',
            fields=[
                ('allmetadata_ptr', models.OneToOneField(auto_created=True, to='simple_seo.AllMetadata', primary_key=True, serialize=False, parent_link=True)),
            ],
            options={
                'verbose_name': 'Мета информация для страниц',
                'verbose_name_plural': 'Мета информации для страниц',
            },
            bases=('simple_seo.allmetadata',),
        ),
        migrations.AlterField(
            model_name='landingfeature',
            name='visible',
            field=models.BooleanField(verbose_name='Отображать?', default=True),
        ),
        migrations.AlterField(
            model_name='landingpicture',
            name='visible',
            field=models.BooleanField(verbose_name='Отображать?', default=True),
        ),
        migrations.AlterField(
            model_name='landingreview',
            name='visible',
            field=models.BooleanField(verbose_name='Отображать?', default=True),
        ),
    ]
