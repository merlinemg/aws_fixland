# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150702_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='landingfeature',
            name='description_en',
            field=models.TextField(null=True, verbose_name='Текст'),
        ),
        migrations.AddField(
            model_name='landingfeature',
            name='description_ru',
            field=models.TextField(null=True, verbose_name='Текст'),
        ),
        migrations.AddField(
            model_name='landingfeature',
            name='title_en',
            field=models.CharField(max_length=255, null=True, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='landingfeature',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='landingpicture',
            name='description_en',
            field=models.TextField(null=True, verbose_name='Описание'),
        ),
        migrations.AddField(
            model_name='landingpicture',
            name='description_ru',
            field=models.TextField(null=True, verbose_name='Описание'),
        ),
        migrations.AddField(
            model_name='landingpicture',
            name='title_en',
            field=models.CharField(max_length=255, null=True, verbose_name='Слоган'),
        ),
        migrations.AddField(
            model_name='landingpicture',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='Слоган'),
        ),
    ]
