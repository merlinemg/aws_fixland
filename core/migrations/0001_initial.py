# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LandingReview',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=255, verbose_name='Фамилия', blank=True, null=True)),
                ('company', models.CharField(max_length=255, verbose_name='Компания', blank=True, null=True)),
                ('review', models.TextField(verbose_name='Отзыв')),
                ('photo', models.ImageField(upload_to='landing_review')),
                ('visible', models.BooleanField(verbose_name='Отоброжать?', default=True)),
                ('position', models.IntegerField(verbose_name='Позиция', help_text='По этому полю отзывы сортируются')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
