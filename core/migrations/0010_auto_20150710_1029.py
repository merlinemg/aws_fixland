# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0009_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('amount', models.DecimalField(max_digits=20, decimal_places=2, verbose_name='Сумма')),
                ('currency', models.CharField(default='rub', choices=[('rub', 'rub'), ('usd', 'usd')], max_length=255, verbose_name='Валюта')),
                ('status', models.CharField(choices=[('hold', 'Заморожен'), ('success', 'Завершен')], max_length=255, verbose_name='Статус')),
                ('description', models.TextField(verbose_name='Описание')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('recipient', models.ForeignKey(null=True, blank=True, verbose_name='Получатель', to=settings.AUTH_USER_MODEL, related_name='recipient_transactions')),
                ('sender', models.ForeignKey(null=True, blank=True, verbose_name='Отправитель', to=settings.AUTH_USER_MODEL, related_name='sender_transactions')),
            ],
            options={
                'verbose_name_plural': 'Трансакции',
                'verbose_name': 'Трансакция',
            },
        ),
    ]
