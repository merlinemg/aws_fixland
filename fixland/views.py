import simplejson
from django.shortcuts import render, redirect
from django.conf import settings
from django.core.urlresolvers import reverse
from annoying.decorators import ajax_request
from accounts.models import FeedBack
from django import http
from django.utils.translation import check_for_language
from django.utils import translation

from devices.bl import get_devices_for_repair, get_devices_for_sell, get_statuses

from accounts.models import Region
from accounts.tasks import email_feedback_to_admin
from accounts.utils import get_region_by_ip
from core.models import LandingReview, LandingPicture, LandingFeature, LandingLogo
from core.utils import get_client_ip


def index(request):
    if request.user.is_authenticated() and request.user.is_master():
        return redirect(reverse('master_index'))
    context = {
        'reviews': LandingReview.objects.filter(visible=True).order_by('position'),
        'picture': LandingPicture.objects.filter(visible=True).first(),
        'features': LandingFeature.objects.filter(visible=True).order_by('position'),
        'logos': LandingLogo.objects.filter(visible=True).order_by('position')
    }
    return render(request, 'landing.html', context)


def render_repair(request, template):
    devices = get_devices_for_repair()
    client_region = get_region_by_ip(get_client_ip(request)) or Region.get_default()
    context = {
        'devices': simplejson.dumps(devices),
        'GOOGLE_MAPS_API_KEY': settings.GOOGLE_MAPS_API_KEY,
        'client_region': simplejson.dumps({
            'name': client_region.name,
            'dadata_name': client_region.dadata_name
        })
    }
    return render(request, template, context)


def repair(request):
    return render_repair(request, 'repair.html')


def warranty(request):
    return render_repair(request, 'warranty.html')


def sell(request):
    devices = get_devices_for_sell()
    statuses = get_statuses()
    return render(request, 'sell.html', {'devices': simplejson.dumps(devices), 'statuses': simplejson.dumps(statuses), 'GOOGLE_MAPS_API_KEY': settings.GOOGLE_MAPS_API_KEY})


def about(request):
    return render(request, 'about.html')


def feedback(request):
    return render(request, 'feedback.html')


@ajax_request
def create_feedback(request):
    try:
        data = simplejson.loads(request.body.decode('utf-8'))
        name = data.get('name')
        contact = data.get('contact')
        feedback_text = data.get('feedtext')
        feedback = FeedBack(name=name, contact=contact, feedback_text=feedback_text)
        feedback.save()
        email_feedback_to_admin.delay(feedback.id)
        return {'success': True}
    except Exception:
        return {'success': False}


def set_language(request):
    next_url = request.GET.get('next', None)
    if not next_url:
        next_url = request.META.get('HTTP_REFERER', None)
    if not next_url:
        next_url = '/'
    # Правка кривого переключения языка
    if (
            len(next_url) >= 3 and
            next_url[-3:-1] in [i[0] for i in settings.LANGUAGES]
    ):
        next_url = '/'

    response = http.HttpResponseRedirect(next_url)
    if request.method == 'GET':
        lang_code = request.GET.get('language', None)
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session[translation.LANGUAGE_SESSION_KEY] = lang_code
            else:
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
            translation.activate(lang_code)
    return response
