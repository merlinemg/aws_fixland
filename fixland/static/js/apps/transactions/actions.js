import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getTransactions',
    'setFilterItem'
]);

module.exports = actions;
