import React from 'react';
import ReactRouter from 'react-router';
import Transactions from './components/transactionsTable.js';

var {Route, RouteHandler} = ReactRouter;


var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <RouteHandler />
        );
    }
});

var transactionsRoutes = (
    <Route name='app' handler={App}>
        <Route name='transactions' path='/' handler={Transactions} />
    </Route>
);

module.exports = transactionsRoutes;
