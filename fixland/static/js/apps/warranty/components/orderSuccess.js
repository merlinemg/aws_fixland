/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import addons from 'react-addons';
import store from '../store';
import actions from '../actions';
import Utils from '../../common/utils';
import ErrorMessage from '../../common/components/errorMessage'
import Loading from '../../common/components/loading'



module.exports = React.createClass({
    render() {
        return (
            <div className="content step">
                <h3>{gettext('Спасибо за обращение по гарантийному случаю.')}</h3>
                <br/>
                <h4 className="aboutMaster textLighter">{gettext('Наш специалист свяжется с Вами в течение 2-3 часов')}</h4>
            </div>
        );
    }
});
