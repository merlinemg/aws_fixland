import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';

import GetGeolocationMixin from '../../common/components/getGeolocationMixin';

var GetGeolocation = React.createClass({
    actions: actions,
    mixins: [Reflux.connect(store), GetGeolocationMixin]
});

module.exports = GetGeolocation;
