import React from 'react';
import ReactRouter from 'react-router';
import Warranty from './components/warranty';
import Repairs from './components/repairs';
import SelectTime from './components/selectTime';
import SelectAddress from './components/selectAddress';
import OrderSuccess from './components/orderSuccess';
import OrderServerProblem from '../common/components/orderServerProblem';


let App = React.createClass({

    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },

    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

let repairRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='warranty' path='/' handler={Warranty}/>
        <ReactRouter.Route name='repairs' path='/repairs' handler={Repairs}/>
        <ReactRouter.Route name='selectTime' path='/repairs/order' handler={SelectTime}/>
        <ReactRouter.Route name='selectAddress' path='/repairs/order/address' handler={SelectAddress}/>
        <ReactRouter.Route name='orderSucess' path='/repairs/order/address/order/success' handler={OrderSuccess}/>
        <ReactRouter.Route name='orderServerProblem' path='/server-problem' handler={OrderServerProblem}/>
    </ReactRouter.Route>
);

module.exports = repairRoutes;
