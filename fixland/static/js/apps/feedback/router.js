import React from 'react';
import ReactRouter from 'react-router';
import FeedBack from './components/feedBack';
import FeedBackSuccess from './components/success';
import OrderServerProblem from '../common/components/orderServerProblem';



var App = React.createClass({
    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },
    render: function() {
        return (
            <ReactRouter.RouteHandler />
        );
    }
});

var feedbackRoutes = (
    <ReactRouter.Route name='app' handler={App}>
        <ReactRouter.Route name='' path='/' handler={FeedBack}/>
        <ReactRouter.Route name='Success' path='/success' handler={FeedBackSuccess}/>
        <ReactRouter.Route name='serverProblem' path='/server-problem' handler={OrderServerProblem}/>
    </ReactRouter.Route>
);

module.exports = feedbackRoutes;
