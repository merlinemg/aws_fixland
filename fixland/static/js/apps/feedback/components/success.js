/* globals gettext */

import React from 'react';


var FeedBackSuccess = React.createClass({
    goBack() {
        history.go(-2);
    },
    render() {
        return (
            <div className='content step'>
                <h3>{gettext('Спасибо.')}</h3>
                <h4>{gettext('Ваш запрос будет рассмотрен в ближайшее время')}</h4>
                <div>
                    <button className='button button-green center-button selectRepair' onClick={this.goBack} type='button'>{gettext('ОK')}</button>
                </div>
            </div>
        );
    }
});

module.exports = FeedBackSuccess;
