/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';
import ErrorMessage from '../../common/components/errorMessage';
import Loading from '../../common/components/loading.js';


var FeedBack = React.createClass({
    mixins: [Reflux.connect(store)],

    validateForm() {
        if (this.validateName() && this.validateContact() && this.validateFeedtext()) {
            return true;
        } else {
            return false;
        }
    },

    validateField(field) {
        if (this.state[field] && !!this.state[field].length) {
            return true;
        } else {
            return false;
        }
    },

    validateName() {
        return this.validateField('name');
    },

    validateContact() {
        return this.validateField('contact');
    },

    validateFeedtext() {
        return this.validateField('feedtext');
    },

    handleNameInputChange(event) {
        actions.setName(event.target.value);
    },

    handleContactInputChange(event) {
        actions.setContact(event.target.value);
    },

    handleFeedtextInputChange(event) {
        actions.setFeedtext(event.target.value);
    },

    sendFeedBack() {
        if (this.validateForm()) {
            this.setState({
                ajaxRequest: true
            });
            actions.sendFeedBack();
        } else {
            this.setState({
                ajaxRequest: false
            });
            this.setState({showError: true});
        }
    },

    render() {
        return (
            <div className='content step feedBackWrp'>
                <h3>{gettext('У Вас есть, что нам сказать?')}</h3>
                { (window.feedbackTelephone) && (
                    <div className="vcard">
                        <h3>{gettext('Позвоните нам по телефону:')}</h3>
                        <h4 className="telephoneH4"><a className="tel" href="tel:{window.feedbackTelephone}">{window.feedbackTelephone}</a></h4>
                    </div>
                    )
                }
                <h3>{gettext('Мы внимательно слушаем:')}</h3>

                <div className='wrapperDataInput nameInput'>
                        <input ref='name' type='text' defaultValue={this.state.name} onChange={this.handleNameInputChange} placeholder={gettext('Как Вас зовут?')} id = 'fullname' className='inputData' />
                        {this.state.showError && !this.validateName() ? <ErrorMessage message={gettext('Введите ваше имя')} /> : null}

                </div>

                <div className='wrapperDataInput tellEmInput'>
                        <input ref='contact' type='text' defaultValue={this.state.contact} onChange={this.handleContactInputChange} placeholder={gettext('Телефон или e-mail для связи')} id = 'fullname' className='inputData' />
                        {this.state.showError && !this.validateContact() ? <ErrorMessage message={gettext('Введите телефон или e-mail')} /> : null}

                </div>

                <div className='wrapperDataInput commentInput'>
                        <textarea maxLength = '100' ref='feedtext' type='text' defaultValue={this.state.feedtext} onChange={this.handleFeedtextInputChange} placeholder={gettext('Ваше сообщение')} id = 'fullname' className='inputData feedtext' />
                        {this.state.showError && !this.validateFeedtext() ? <ErrorMessage message={gettext('Введите текст сообщения')} /> : null}

                </div>
                <div>
                    {this.state.ajaxRequest ? <div className='wrapLoading'><Loading /></div> : <button className='button button-green center-button selectRepair lhBtn' type='button' onClick={this.sendFeedBack}>{gettext('Отправить')}</button>}
                </div>
            </div>
        );
    }
});

module.exports = FeedBack;
