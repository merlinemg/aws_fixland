import Reflux from 'reflux';

var actions = Reflux.createActions([
    'setName',
    'setContact',
    'setFeedtext',
    'sendFeedBack',
    'cleanFeedBack'
]);

module.exports = actions;
