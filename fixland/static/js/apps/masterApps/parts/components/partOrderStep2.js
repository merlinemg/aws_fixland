/* global gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import addons from 'react-addons';

import _ from 'underscore';
import actions from '../actions';
import store from '../store';
import Loading from '../../../common/components/loading.js';
import Money from '../../../common/components/money.js';
import ErrorMessage from '../../../common/components/errorMessage.js';

var Link = Router.Link;

var CartButton = React.createClass({
    mixins: [Reflux.connect(store)],
    handleButtonClick() {
        actions.selectVendorCartPartToggle(this.props.vendorPart);
    },
    isSelected() {
        let cartItem = _.findWhere(this.state.vendorsPartsCart, {partId: this.props.vendorPart.id});
        return cartItem.selected;
    },
    render() {
        var cx = addons.classSet;
        var classesCatr = cx({
                button: true,
                addToCartBtn: true,
                addedToCart: this.isSelected()
            });
        return (
            <button className={classesCatr} onClick={this.handleButtonClick}><i className="fa fa-cart-plus"></i></button>
        );
    }
});

var VendorPartRow = React.createClass({
    mixins: [Reflux.connect(store)],
    getAmount() {
        return this.getQty() * parseInt(this.props.vendorPart.price);
    },
    getQty() {
        return _.findWhere(this.state.vendorsPartsCart, {partId: this.props.vendorPart.id}).qty;
    },
    handleQtyChange(vendorPart, event) {
        actions.setPartQty(vendorPart, event.target.value || 0);
    },
    render() {
        var selectItem = [];
        for (var i = 0; i <= 20; i++) {
            selectItem.push(<option>{i}</option>);
        }
        return (
            <tr>
                <td className='table-sub-title'><span>{this.props.vendorPart.name}</span></td>
                <td>
                    <select className="inputData qtyInput centered selCount" type='text' defaultValue={this.getQty()} onChange={this.handleQtyChange.bind(this, this.props.vendorPart)}>
                        {selectItem}
                    </select>
                </td>
                <td><span><Money amount={this.props.vendorPart.price} currency="usd"/></span></td>
                <td>
                    <span><Money amount={this.getAmount()} currency="usd"/></span>
                    <span className="wrpAddToCart"><CartButton vendorPart={this.props.vendorPart}/></span>
                </td>
            </tr>
        );
    }
});

var TotalRow = React.createClass({
    render() {
        return (
            <tr>
                <td className='table-sub-title'><span>{gettext('ИТОГО')}</span></td>
                <td><span>{this.props.totalQty}</span></td>
                <td></td>
                <td><Money amount={this.props.totalAmount} currency="usd"/></td>
            </tr>
        );
    }
});

var VendorPartsTable = React.createClass({
    mixins: [Reflux.connect(store)],
    getTotalQty() {
        let totalQty = 0;
        this.state.vendorsPartsCart.forEach( (item) => {
            if (item.part.warehouse.code === this.props.vendorParts[0].warehouse.code) {
                totalQty = totalQty + parseInt(item.qty);
            }
        });
        return totalQty;

    },
    getTotalAmount() {
        let totalAmount = 0;
        this.state.vendorsPartsCart.forEach( (item) => {
            if (item.part.warehouse.code === this.props.vendorParts[0].warehouse.code) {
                let vendorPart = _.findWhere(this.props.vendorParts, {id: item.partId});
                if (vendorPart) {
                    totalAmount = totalAmount + (parseInt(vendorPart.price) * parseInt(item.qty));
                }
            }
        });
        return totalAmount;
    },
    render() {
        return (
            <table cellSpacing='0' className='table parts-table smallInputInTable lastBold'>
                <thead>
                    <tr>
                        <th><span>{gettext('Запчасть')}</span></th>
                        <th><span>{gettext('Заказ')}</span></th>
                        <th><span>{gettext('Цена')}</span></th>
                        <th><span>{gettext('Сумма')}</span></th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.vendorParts.map( (vendorPart) => {
                        return <VendorPartRow vendorPart={vendorPart} />;
                    })}
                    <TotalRow totalQty={this.getTotalQty()} totalAmount={this.getTotalAmount()} />
                </tbody>
            </table>
        );
    }
});

var VendorTable = React.createClass({
    mixins: [addons.LinkedStateMixin],
    SORT_TYPES: {
        default: gettext('нет'),
        availability: gettext('наличию'),
        price: gettext('цене'),
        distance: gettext('удалённости')
    },
    getInitialState() {
        return {
            orderBy: 'default'
        }
    },
    distanceBetweenCoordinates() {
        return 1;
    },
    getSortedVendorParts() {
        if (this.state.orderBy === 'default') {
            return this.props.vendorsParts;
        }
        if (this.state.orderBy === 'availability') {
            return _.sortBy(this.props.vendorsParts, (item) => {
                return item.length;
            }).reverse();
        }
        if (this.state.orderBy === 'price') {
            let sortedByPrice = _.sortBy(this.props.vendorsParts, (item) => {
                return _.reduce(item, (memo, obj) => {return memo + parseFloat(obj.price); }, 0) - (item.length * 1000);
            });
            return sortedByPrice;
        }
        if (this.state.orderBy === 'distance') {
            if (this.props.forOrderCord) {
                return _.sortBy(this.props.vendorsParts, (item) => {
                    return this.distanceBetweenCoordinates(this.props.forOrderCord, item.lat, item.lon);
                });
            }
            return this.props.vendorsParts;
        }
    },
    getWarehouseTitle(vendorParts) {
        const vendorName = vendorParts[0].vendor.name,
              warehouseCode = vendorParts[0].warehouse.code,
              warehouseAddress = vendorParts[0].warehouse.address;
        return `${warehouseCode} ${vendorName} ${warehouseAddress}`;
    },
    handleOrderByChanged(event) {
        return this.setState({orderBy: event.target.value});
    },
    render() {
        return (
            <div>
                <span>{gettext('Сортировка: ')}</span><select ref="orderBy" defaultValue={this.state.orderBy} onChange={this.handleOrderByChanged}>
                    {_.keys(this.SORT_TYPES).map((key) => {
                        return <option value={key}>{this.SORT_TYPES[key]}</option>
                    })}
                </select>
                {this.getSortedVendorParts().map( (vendorParts) => {
                    return (
                        <div>
                            <h5 className="storageName">{this.getWarehouseTitle(vendorParts)}</h5>
                            <VendorPartsTable vendorParts={vendorParts} />
                        </div>
                    );
                })}
            </div>
        );
    }
});

module.exports = React.createClass({
    mixins: [Reflux.connect(store), Router.Navigation, Reflux.listenTo(store, 'storeTrigger')],
    storeTrigger(data) {
        if (data.vendorsParts && data.vendorsParts.length) {
            let notAvailableParts = [];
            _.each(this.state.partsCart, (item) => {
                if (!_.findWhere(this.state.vendorsParts, {part: item.partId})) {
                    notAvailableParts.push(item);
                }
            });
            this.setState({
                notAvailableParts: notAvailableParts
            });
        }
    },
    componentWillMount() {
        if (!this.state.partsCart.length) {
            this.transitionTo('partOrderStep1', this.props.params);
        }
        actions.getVendorsParts(true);
    },
    componentWillUnmount() {
        actions.cleanUpdatePartsAvailabilityId();
    },
    getGroupedVendorsParts() {
        let vendorsParts = _.groupBy(this.state.vendorsParts, function(item) {
            return item.warehouse.id;
        });
        return _.values(vendorsParts);
    },
    isAnySelected() {
        return _.filter(this.state.vendorsPartsCart, (item) => {
            return item.selected === true && item.qty;
        }).length ? true : false;
    },
    getVendorTableProps() {
        return {
            vendorsParts: this.getGroupedVendorsParts(),
            forOrderCord: this.state.partsOrder && !_.isEmpty(this.state.partsOrder) && this.state.partsOrder.orderAddress ? {
                lat: this.state.partsOrder.orderAddress.lat,
                lon: this.state.partsOrder.orderAddress.lon
            } : null
        }
    },
    getPartByArticle(article) {
        return _.findWhere(this.state.vendorsParts, {article: article});
    },
    render() {
        let errorMessageText = null,
            unsuccesfull_parts = this.state.partsOrder.unsuccesfull_parts;
        if (this.state.notAvailableParts && this.state.notAvailableParts.length) {
            let parts = _.pluck(_.pluck(this.state.notAvailableParts, 'part'), 'name').join(', ');
            errorMessageText = gettext('Запчасти: ') + parts + gettext(' - нет в наличии');
        }
        return (
            <div className="content step wrpParts">
                <h3>{gettext('Заказ запчастей')}</h3>
                {errorMessageText && <ErrorMessage message={errorMessageText}/>}
                {this.state.success && unsuccesfull_parts && unsuccesfull_parts.length ? (
                    <div>
                        <ErrorMessage message={'Заказ не создан'}/>
                        {unsuccesfull_parts.map((item) => {
                            let part = this.getPartByArticle(item.article);
                            let message = `${part.name} ${gettext('в наличии')} - ${item.qty}`;
                            return <ErrorMessage message={message}/>
                        })}
                    </div>
                ) : ''}
                { this.state.success ? (
                    this.state.vendorsParts && this.state.vendorsParts.length ? (
                        <div>
                            <VendorTable {...this.getVendorTableProps()}/>
                            {this.isAnySelected() && <Link to="partOrderStep3" className="button button-green selectRepair" params={this.props.params}>{gettext('Подтвердить заказ')}</Link>}
                        </div>
                    ) : <p>{gettext('Нет запчастей к данной модели')}</p>
                ) : <Loading />}
            </div>
        );
    }
});
