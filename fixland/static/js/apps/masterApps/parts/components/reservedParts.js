import React from 'react';
import Router from 'react-router'
import ReservedPartsTable from '../../common/components/reservedPartsTable';

let Link = Router.Link;


module.exports = React.createClass({
    render() {
        return (
            <div className="content step">
                <h1>{gettext('Мои запчасти')}</h1>
                <ReservedPartsTable showth={true} showMasterBalance={true}/>
                <Link to="selectDevice" className="button button-green selectRepair">{gettext('Заказать еще')}</Link>
            </div>
        )
    }
});
