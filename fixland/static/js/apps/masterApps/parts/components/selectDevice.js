/* globals gettext */

import actions from '../actions';
import React from 'react';

import SelectDeviceMixin from '../../../common/components/selectDeviceMixin';

var SelectDevice = React.createClass({
    actions: actions,
    title: gettext('Мои запчасти'),
    mixins: [SelectDeviceMixin]
});

module.exports = SelectDevice;
