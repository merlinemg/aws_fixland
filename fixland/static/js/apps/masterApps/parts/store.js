/* globals  jQuery */

import Reflux from 'reflux';
import _ from 'underscore';
import moment from 'moment';
import actions from './actions';
import Storage from '../../common/storage';
import Utils from '../../common/utils';

var storage = new Storage();

var store = Reflux.createStore({
    listenables: [actions],
    storage: storage,
    partsStore: null,

    cleanedStore: {
        partsCart: [],
        vendorsPartsCart: [],
        partsOrder: {}
    },
    getInitialState() {
        this.partsStore = this.storage.getItem('partsStore');
        if (!this.partsStore) {
            this.partsStore = this.cleanedStore;
        }
        return this.partsStore;
    },

    cleanStore() {
        this.partsStore = this.cleanedStore;
        this.storage.removeItem('partsStore', this.partsStore);
        this.trigger(this.partsStore);
    },

    cleanPartsStoreCart(cart) {
        this.partsStore[cart] = [];
        this.storage.setItem('partsStore', this.partsStore);
        this.trigger(this.partsStore);
    },

    onCleanPartsCart() {
        this.cleanPartsStoreCart('partsCart');
    },

    onCleanVendorsPartsCart() {
        this.cleanPartsStoreCart('vendorsPartsCart');
    },

    setOrderItem(key, value, trigger=true) {
        this.partsStore.partsOrder[key] = value;
        this.storage.setItem('partsStore', this.partsStore);
        if (trigger) this.trigger({partsOrder: this.partsStore.partsOrder});
    },

    onGetMasterParts(model) {
        Utils.ajaxGet({
            url: '/api/v1/master-parts/',
            data: {model: model}
        }).then((response) => {
            this.trigger({successMasterParts: true, masterParts: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetMasterPart(partId) {
        Utils.ajaxGet({
            url: `/api/v1/master-parts/`,
            data: {part: partId}
        }).then((response) => {
            let masterPart = {};
            if (response.length) {
                masterPart = response[0];
            }
            this.trigger({success: true, masterPart: masterPart});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetModelParts(model) {
        Utils.ajaxGet({
            url: '/api/v1/parts/',
            data: {model: model}
        }).then((response) => {
            this.trigger({successParts: true, parts: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    getLastUpdated() {
        if (!this.partsStore.lastUpdated) this.partsStore['lastUpdated'] = {};
        let partIds = _.pluck(this.partsStore.partsCart, 'partId').join(',');
        let hash = Utils.hashCode(partIds);
        return this.partsStore.lastUpdated[hash];
    },

    setLastUpdatedAsNow() {
        let partIds = _.pluck(this.partsStore.partsCart, 'partId').join(',');
        let hash = Utils.hashCode(partIds);
        this.partsStore.lastUpdated[hash] = moment();
        this.setOrderItem('lastUpdated', this.partsStore.lastUpdated, false);
    },

    checkIfUpdatedAndGetVendorParts() {
        if (!this.partsStore.partsOrder.updatePartsAvailabilityId) {
            return
        }
        Utils.ajaxGet({
            url:'/api/v1/check-update-parts-finish',
            data: {
                taskId: this.partsStore.partsOrder.updatePartsAvailabilityId
            },
            decamelize: true
        }).then((response) => {
            if (response.result === 'success') {
                this.setLastUpdatedAsNow();
                this.setOrderItem('updatePartsAvailabilityId', null);
                this.getVendorsParts();
            } else if (response.result === 'failed') {
                setTimeout(this.updatePartsAvailability, 1000);
            } else {
                setTimeout(this.checkIfUpdatedAndGetVendorParts, 1000);
            }
        });
    },

    updatePartsAvailability() {
        if (this.partsStore.partsOrder.updatePartsAvailabilityId) {
            this.checkIfUpdatedAndGetVendorParts();
        } else {
            let lastUpdated = this.getLastUpdated();
            if (lastUpdated && moment.duration(moment().diff(lastUpdated)).minutes() < 10) {
                this.getVendorsParts();
                return
            }
            Utils.ajaxGet({
                url:'/api/v1/update-parts-availability',
                data: {
                    partIds: _.pluck(this.partsStore.partsCart, 'partId').join(','),
                    forOrder: this.partsStore.partsOrder.forOrder,
                },
                decamelize: true
            }).then((response) => {
                if (response.result && response.task_id) {
                    this.setOrderItem('updatePartsAvailabilityId', response.task_id);
                    this.checkIfUpdatedAndGetVendorParts();
                }
            });
        }
    },

    getVendorsParts(updateParts) {
        if (updateParts) {
            this.updatePartsAvailability();
        } else {
            Utils.ajaxGet({
                url: '/api/v1/vendor-parts/',
                data: {
                    partIds: _.pluck(this.partsStore.partsCart, 'partId').join(','),
                    forOrder: this.partsStore.partsOrder.forOrder,
                },
                decamelize: true
            }).then((response) => {
                if (!this.partsStore.vendorsPartsCart.length) {
                    response.forEach( (item) => {
                        var cartPart = _.findWhere(this.partsStore.partsCart, {partId: item.part});
                        this.partsStore.vendorsPartsCart.push({
                            partId: item.id,
                            part: item,
                            qty: cartPart ? cartPart.qty : 0,
                            selected: false
                        });
                    });
                }
                this.storage.setItem('partsStore', this.partsStore);
                this.trigger(this.partsStore);
                this.trigger({success: true, vendorsParts: response});
            }).fail(() => {
                this.trigger({success: false});
            });
        }
    },

    addPartToCart(part, qty, cart){
        var cartPart = _.findWhere(this.partsStore[cart], {partId: part.id});
        if (cartPart) {
            if (!qty) {
                this.partsStore[cart] = _.filter(this.partsStore[cart], (item) => {
                    return item.partId !== part.id;
                });
            } else {
                cartPart.qty = qty;
            }
        } else if (qty) {
            this.partsStore[cart].push({
                partId: part.id,
                part: part,
                qty: qty
            });
        }
        this.storage.setItem('partsStore', this.partsStore);
        this.trigger(this.partsStore);
    },

    onAddPartToPartsCart(part, qty) {
        this.addPartToCart(part, qty, 'partsCart');
    },

    onAddPartToVendorPartsCart(part) {
        this.addPartToCart(part, part.qty, 'vendorsPartsCart');
    },

    onSetPartQty(part, qty) {
        let cartVendorPart = _.findWhere(this.partsStore.vendorsPartsCart, {partId: part.id});
        cartVendorPart.qty = parseInt(qty);
        this.storage.setItem('partsStore', this.partsStore);
        this.trigger(this.partsStore);
    },

    onSelectVendorCartPartToggle(part) {
        let cartVendorPart = _.findWhere(this.partsStore.vendorsPartsCart, {partId: part.id});
        cartVendorPart.selected = !cartVendorPart.selected;
        this.storage.setItem('partsStore', this.partsStore);
        this.trigger(this.partsStore);
    },

    onRemoveVendorPartsCartItem(part) {
        this.partsStore.vendorsPartsCart = this.partsStore.vendorsPartsCart.filter( (item) => {
            return item.partId !== part.id;
        });
        this.storage.setItem('partsStore', this.partsStore);
        this.trigger(this.partsStore);
    },

    onGetMasterOrder(orderId) {
        Utils.ajaxGet({
            url: '/api/v1/repair-orders/' + orderId + '/'
        }).then((response) => {
            this.setOrderItem('forOrder', response.number);
            this.setOrderItem('orderAddress', response.address);
            this.trigger({successOrder: true, order: response});
        }).fail(() => {
            this.trigger({permissionDenied: true});
        });
    },

    onPartChargeOff(model, part, qty, successRedirect) {
        Utils.ajaxPost({url: `/part-charge-off/${part}/${qty}/`}).then( () => {
            this.trigger({ajaxRequest: false});
            location.hash = successRedirect;
        }).fail( (response) => {
            var responseObj = JSON.parse(response.response);
            if (responseObj.errors) {
                this.trigger({errors: responseObj.errors});
            }
            this.trigger({ajaxRequest: false});
        });
    },

    setCreateOrderErrorResult(data) {
        this.setOrderItem('createOrderPartsResult', data)
    },

    onCreateOrder() {
        let partsData = [];
        _.forEach(this.partsStore.vendorsPartsCart, (item) => {
            if (item.selected && item.qty) {
                partsData.push({
                    part: item.partId,
                    qty: item.qty
                });
            }
        });
        if (!partsData) {
            this.trigger({ajaxRequest: false});
        } else {
            let data = {
                parts: partsData,
                forOrder: this.partsStore.partsOrder.forOrder
            };
            Utils.ajaxPost({
                url: '/api/v1/parts-order/',
                data: data,
                decamelize: true
            }).then( (response) => {
                if (response.success) {
                    this.setOrderItem('succesfull_parts', response.succesfull_parts);
                    this.setOrderItem('unsuccesfull_parts', response.unsuccesfull_parts);
                    this.onCleanPartsCart();
                    this.onCleanVendorsPartsCart();
                    this.trigger({orderCreated: true, ajaxRequest: false});
                } else {
                    this.setOrderItem('unsuccesfull_parts', response.unsuccesfull_parts);
                    this.trigger({orderCreated: false, ajaxRequest: false});
                }
            }).fail( () => {
                this.trigger({orderCreated: false, ajaxRequest: false});
            });
        }
    },

    cleanUpdatePartsAvailabilityId() {
        this.setOrderItem('updatePartsAvailabilityId', null);
    },
});

module.exports = store;
