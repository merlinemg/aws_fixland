import actions from './actions';
import Reflux from 'reflux';
import reqwest from 'reqwest';
import Utils from '../../common/utils';

var store = Reflux.createStore({
    listenables: [actions],
    master: null,

    onSaveAvatar(avatar) {
        Utils.ajaxPost({
            url: '/change_avatar/',
            data: avatar
        }).then(() => {
            location.hash = '/';
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetMaster() {
        reqwest({
            url: '/api/v1/masters/me/',
            method: 'get',
            contentType: 'application/json',
            type: 'json'
        }).then((response) => {
            this.master = response;
            this.trigger({success: true, master: response});
        }).fail(() => {
            this.trigger({success: false});
        });
    },

    onGetCurrentMaster() {
        if (this.master) {
            this.trigger({master: this.master});
        } else {
            this.onGetMaster();
        }
    },

    onSaveName(userId, firstName, lastName) {
        Utils.ajaxPatch({
            url: '/api/v1/users/' + userId,
            data: {'first_name': firstName, 'last_name': lastName}
        }).then((response) => {
            this.master.user = response;
            this.trigger({
                success: true,
                ajaxRequest: false,
                editName: false,
                master: this.master
            });
        }).fail(() => {
            this.trigger({success: false, ajaxRequest: false});
        });
    }
});

module.exports = store;
