/* globals jQuery, gettext */

import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import store from '../store';
import actions from '../actions';
import addons from 'react-addons';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';
import Money from '../../../common/components/money';


var Link = Router.Link;

var Certificated = React.createClass({
    render() {
        var cx = addons.classSet;
        var classes = cx({
            'button': true,
            green: this.props.master.is_certificated,
            red: !this.props.master.is_certificated
        });
        return (<div className={classes}>{this.props.master.is_certificated_display}</div>);
    }
});

var Gender = React.createClass({
    render() {
        var text = this.props.gender === 'male' ? gettext('М') : gettext('Ж');
        return (<div className='button genderBlock'>{text}</div>);
    }
});

var Avatar = React.createClass({
    render() {
        var style = {
            borderColor: this.props.borderColor ? '#' + this.props.borderColor : '#C4C4C4'
        };
        return (<img className="avatar-img" src={ this.props.avatarImg } style={style} alt='img' />);
    }
});

var EditName = React.createClass({
    getInitialState() {
        return {
            userId: this.props.user.id,
            firstName: this.props.user.first_name,
            lastName: this.props.user.last_name
        };
    },
    validateForm() {
        if (this.validateFirstName() && this.validateLastName() ) {
            return true;
        } else {
            return false;
        }
    },

    validateField(field) {
        if (this.state[field] && !!this.state[field].length) {
            return true;
        } else {
            return false;
        }
    },

    validateFirstName() {
        return this.validateField('firstName');
    },

    validateLastName() {
        return this.validateField('lastName');
    },

    handleFirstNameInputChange(event) {
        this.setState({firstName: event.target.value});
    },

    handleLastNameInputChange(event) {
        this.setState({lastName: event.target.value});
    },

    saveName() {
        if (this.validateForm()) {
            this.setState({
                ajaxRequest: true
            });
            actions.saveName(this.state.userId, this.state.firstName, this.state.lastName);
        } else {
            this.setState({
                ajaxRequest: false
            });
            this.setState({showError: true});
        }
    },

    render() {
        return (
            <div className="editNameBlock">
                <div className="wrpInptError identError">
                    <input ref="firstName" type='text' defaultValue={this.state.firstName} onChange={this.handleFirstNameInputChange} placeholder='Как вас зовут?' id = "firstName" className="inputData" />
                    {this.state.showError && !this.validateFirstName() ? <ErrorMessage message={gettext('Введите ваше имя')} /> : null}
                </div>
                <div className="wrpInptError identError">
                    <input ref="lastName" type='text' defaultValue={this.state.lastName} onChange={this.handleLastNameInputChange} placeholder={gettext('Фамилия')} id = "lastName" className="inputData" />
                    {this.state.showError && !this.validateLastName() ? <ErrorMessage message={gettext('Введите вашу фамилию')} /> : null}
                </div>
                {this.state.ajaxRequest ? <Loading /> : <button className='button button-blue standartBr' onClick={this.saveName}>{gettext('Сохранить')}</button>}
            </div>
        );
    }
});

var Profile = React.createClass({
    mixins: [Reflux.connect(store)],
    componentWillMount() {
        actions.getMaster();
    },
    getInitialState() {
        return {success: false};
    },
    onClickHandle(course){
        this.subscribeCourse(course);
    },
    editName() {
        this.setState({editName: true});
    },
    updateDimensions() {
        var windowHeight = jQuery(window).height();
        var windowWidth = jQuery(window).width();
        jQuery('.profile').find('.toolTip').each( function() {
            if ((windowHeight < 570) && (windowWidth < 680)) {
                jQuery(this).css('top', '-' + (jQuery(this).height() + 15) + 'px');
            } else if ((windowHeight > 570) && (windowWidth < 680)) {
                jQuery(this).css('top', 20 + 'px');
            } else {
                jQuery(this).css('top', '-' + (jQuery(this).height()/2.0) + 'px');
            }
        });
    },
    componentDidUpdate() {
        this.updateDimensions();
    },
    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    },

    render() {
        return (
            <div className='content step profile'>
                <h3>{gettext('Обо мне')}:</h3>
                <div className='container'>
                    {!this.state.success && <Loading />}
                    {this.state.success && <div>

                        <div className='one-half thumb-left avatarBlock'>
                            <Avatar avatarImg={this.state.master.user.avatar_url} borderColor={this.state.master.status_color_hex}/>
                            <Link className="changeAvatar" to='avatar' ></Link>
                        </div>
                        <div className='two-half last-column profileTextBlock'>
                            {this.state.editName ? <EditName user={this.state.master.user}/> : <p className="profileName">{this.state.master.user.name}</p>}
                            <p>{this.state.master.user.phone}</p>
                            <p className="emailText">{this.state.master.user.email}</p>
                        </div>
                        <div className='decoration'></div>
                        <div>
                            <div className='one-third certificatedBlock'><Certificated master={this.state.master}/></div>
                            <div className='one-third'><Gender gender={this.state.master.user.gender}/></div>
                            <div className='one-third last-column'>
                                {!this.state.editName && <button className='button button-blue full-button changeName standartBr' onClick={this.editName}>{gettext('Изменить')}</button>}
                            </div>
                        </div>
                        <div className="statusBlock">{gettext('Статус:')} <span className="statusSpan">{this.state.master.status_name}</span><span className="spanIco"><i className="info-list"></i><div className="toolTip">{gettext('Ваш статус зависит от Вашего рейтинга. Бывает Золотой, Серебряный и Бронзовый статус. Чем лучше Вы работаете, тем выше Ваш статус, и тем меньше комиссию берёт с Вас Fixland.ru.')}</div></span></div>
                        <div className="statusBlock">{gettext('Рейтинг:')} {this.state.master.not_full_rating} <span className="spanIco"><i className="info-list"></i><div className="toolTip">{gettext('Ваш рейтинг')}</div></span> x {this.state.master.rejection_coef}<span className="spanIco"><i className="info-list"></i><div className="toolTip">{gettext('Коэффициент отказов')}</div></span> = {Math.floor(this.state.master.rating)}<span className="spanIco"><i className="info-list"></i><div className="toolTip">{gettext('Ваш рейтинг рассчитывается, исходя из количества и качества ремонтов, сделанных Вами за последние 90 дней. Чем выше Ваш рейтинг, тем чаще Вас выбирает система, как исполнителя для новых заказов. Рейтинг понижается, если Вы выполняете мало заказов или часто отказываетесь от выбранных Вами заказов.')}</div></span></div>
                        <div className="statusBlock">{gettext('Баланс:')} <Money amount={this.state.master.balance} currency={this.state.master.currency} /><span className="spanIco"><i className="info-list"></i><div className="toolTip">{gettext('В каждом заказе указана сумма комиссии Fixland.ru, которая необходима для того, чтобы иметь возможность выбрать данный заказ. Если Ваш баланс будет ниже этой суммы, то Вы будете видеть заказы, но не сможете их выбирать. Держите свой баланс положительным, чтобы иметь возможность выбирать заказы.')}</div></span></div>
                    </div>}
                </div>
            </div>
        );
    }
});

module.exports = Profile;
