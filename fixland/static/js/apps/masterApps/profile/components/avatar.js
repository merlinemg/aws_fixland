/* globals jQuery, gettext */

import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';
import Loading from '../../../common/components/loading.js';
import ErrorMessage from '../../../common/components/errorMessage';

var Avatar = React.createClass({
    mixins: [Reflux.connect(store)],
    componentDidMount() {
        actions.getCurrentMaster();
    },

    getAvatarElement() {
        return jQuery(React.findDOMNode(this.refs.avatar));
    },

    initAvatarCropper() {
        var avatar = this.getAvatarElement();
        avatar.cropper({
            responsive: true,
            strict: true,
            aspectRatio: 1 / 1,
            autoCropArea: 1,
            autoCrop: true,
            background: false,
            guides: false,
            movable: true,
            highlight: false,
            dragCrop: false,
            cropBoxMovable: false,
            cropBoxResizable: false
        });
    },

    destroyAvatarCropper() {
        var avatar = jQuery(React.findDOMNode(this.refs.avatar));
        avatar.cropper('destroy');
    },

    save() {
        var avatarDataUrl = this.getAvatarElement().cropper('getCroppedCanvas').toDataURL();
        actions.saveAvatar(avatarDataUrl);
    },

    handleFile: function(e) {
        var self = this;
        self.setState({imageTypeError: false});
        var reader = new FileReader();
        var file = e.target.files[0];
        if (!file) {
            return;
        }
        if (file.type.indexOf('image') !== -1){
            reader.onload = function(upload) {
                self.setState({
                    uploadedImg: upload.target.result
                });
                self.destroyAvatarCropper();
                self.initAvatarCropper();
            };
            reader.readAsDataURL(file);
        } else {
            self.setState({imageTypeError: true});
        }
    },

    render() {
        var avatarSrc = '/static/images/pictures/convivence.jpg';
        if (!this.state.uploadedImg && this.state.master && this.state.master.user.avatar) {
            avatarSrc = this.state.master.user.avatar;
        }
        if (this.state.uploadedImg) {
            avatarSrc = this.state.uploadedImg;
        }
        return (
            <div className="content step selectAvatar">
                <h3>{gettext('Моё фото')}</h3>
                { this.state.master ? <div className='container'>
                    <div className="avatar-container">
                        <img ref="avatar" src={avatarSrc} alt="Picture"/>
                    </div>
                    <div className="button-blue wrpInptFile standartBr">
                        <div className="surrogateFileInpt">{gettext('Загрузить новое')}</div>
                        <input type="file" accept="image/*" onChange={this.handleFile} />
                    </div>
                    { this.state.uploadedImg && <button className="button button-green saveAvatar standartBr" onClick={this.save}>{gettext('Сохранить')}</button>}
                    {this.state.imageTypeError && <ErrorMessage message={gettext('Неверный формат')} />}
                </div> : <Loading /> }
            </div>
        );
    }
});

module.exports = Avatar;
