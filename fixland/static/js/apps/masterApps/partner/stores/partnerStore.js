import Reflux from 'reflux';

import actions from '../actions/partnerActions';


var store = Reflux.createStore({
    listenables: [actions],
    partner: null,

    initCompleted(data) {
        this.partner = data;
        this.trigger(data);
    }
});

module.exports = store;
