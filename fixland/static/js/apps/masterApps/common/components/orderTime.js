/* globals gettext */

import React from 'react';
import moment from 'moment';

var OrderTime = React.createClass({
    render() {
        var date;
        var time;
        var statusText;
        if (this.props.asSoonAsPossible) {
            statusText = gettext('Срочно');
            date = moment(this.props.createdAt).format('DD-MM-YYYY');
        } else {
            var starttime = moment(this.props.starttime),
                endtime = moment(this.props.endtime);
            if (starttime.dayOfYear() === endtime.dayOfYear()) {
                date = starttime.format('DD-MM-YYYY');
                time = starttime.format('HH:mm') + '-' + endtime.format('HH:mm');
                statusText = '';
            } else {
                date = starttime.format('DD-MM-YYYY');
                time = starttime.format('HH:mm') + '-' + endtime.format('HH:mm');
                statusText = '';
            }
        }
        return <span><p>{date}</p><p>{time} {statusText}</p></span>;
    }
});

module.exports = OrderTime;
