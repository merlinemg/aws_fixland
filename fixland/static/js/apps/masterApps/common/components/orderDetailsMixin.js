 /* globals gettext */

import React from 'react';
import Router from 'react-router';
import _ from 'underscore';
import Loading from '../../../common/components/loading.js';
import DevicePhoto from '../../../common/components/devicePhoto.js';
import ErrorMessage from '../../../common/components/errorMessage.js';
import Money from '../../../common/components/money.js';
import OrderTime from './orderTime';
import OrderAddress from './orderAddress';
import ReservedPartsTable from './reservedPartsTable';

var Link = Router.Link;

var OrderRepairTableRow = React.createClass({
    render() {
        let price = this.props.is_warranty ? 0 : this.props.repair.price;
        return (
            <tr>
                <td className='table-sub-title'>{this.props.repair.device_model_repair}</td>
                <td>
                    <Money amount={price}
                        currency="usd"/>
                </td>
            </tr>
        );
    }
});

var OrderRepairTable = React.createClass({
    render() {
        var total = 0 - this.props.discount;
        return (
            <table cellSpacing='0' className='table repairs-table'>
                <thead></thead>
                <tbody>
                    {this.props.repairs.map((repair) => {
                        if (!this.props.is_warranty) total = total + parseInt(repair.price);
                        return <OrderRepairTableRow repair={repair} is_warranty={this.props.is_warranty}/>;
                    })}
                    <tr>
                        <td className='table-sub-title'>{gettext('Скидка')}</td>
                        <td>
                            <Money amount={this.props.discount}
                                currency="usd"/>
                        </td>
                    </tr>
                    <tr className='total-row'>
                        <td className='table-sub-title'><span className="greenText">{gettext('ИТОГО')}</span> </td>
                        <td>
                            <Money className="greenText" amount={total}
                                currency="usd"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
});

var OrderPartsTableRow = React.createClass({
    render() {
        const price = parseInt(this.props.part.price) * this.props.part.quantity;
        return (
            <tr>
                <td className='table-sub-title'>{this.props.part.part.name} ({this.props.part.quantity} шт.)</td>
                <td>
                    <Money className="greenText" amount={price}
                        currency="usd"
                        minus={true}/>
                </td>
            </tr>
        );
    }
});

var OrderPartsTable = React.createClass({
    render() {
        var total = 0;
        return (
            <div>
                <h5>{gettext('Необходимые запчасти: *')}</h5>
                <table cellSpacing='0' className='table parts-table'>
                    <thead></thead>
                    <tbody>
                        {this.props.parts.map((part) => {
                            total = total + (parseInt(part.price) * part.quantity);
                            return <OrderPartsTableRow part={part} />;
                        })}
                        <tr className='total-row'>
                            <td className='table-sub-title'><span className="greenText">{gettext('ИТОГО')}</span></td>
                            <td>
                                <Money className="greenText" amount={total}
                                    currency="usd"
                                    minus={true}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
});

var CompleteOrder = React.createClass({
    getInitialState() {
        return {
            confirm: false
        };
    },
    render() {
        if (this.props.order.status === 'new') {
            if (this.state.confirm) {
                return (
                    <div>
                        <h5>{gettext('Вы уверены, что хотите закрыть заказ?')}</h5>
                        <button className='button button-blue center-button selectRepair' onClick={this.props.completeOrder}>{gettext('Да')}</button>
                        <button className='button button-blue center-button selectRepair'onClick={() => { this.setState({confirm: false}); }}>{gettext('Нет')}</button>
                    </div>
                );
            } else {
                return <button className='button button-blue center-button selectRepair' onClick={() => { this.setState({confirm: true}); }}>{gettext('Закрыть заказ')}</button>
            }
        } else if (this.props.order.status === 'done') {
            return (<h5>{gettext('Заказ выполнен')}</h5>);
        } else {
            return null;
        }
    }
});

let RejectedComments = React.createClass({
    render() {
        return (
            <div className="rejectComments">
                <h5>{gettext('Комментарии к отказам')}</h5>
                <div className="decoration hide-if-responsive"></div>
                {this.props.comments.map((comment, key) => {
                    return <p className="text-white ">{`${key+1}. ${comment.comment}`}</p>
                })}
                <div className="decoration hide-if-responsive"></div>
            </div>
        )
    }
});

let OrderUser = React.createClass({
    getTelLink() {
        return 'tel:' + this.props.user.phone;
    },
    render() {
        return (
            <div>
                <h5>{gettext('О клиенте:')}</h5>
                {this.props.user ? (
                    <div className='wrpAboutClient'>
                        <div className='one-half thumb-left'>
                            <img src='/static/images/pictures/convivence.jpg' alt='img' />
                        </div>
                        <div className='two-half last-column aboutClient'>
                            <p>{this.props.user.name}</p>
                            <a className="tellLink" href={this.getTelLink()}>{this.props.user.phone}</a>
                            <div className="decoration"></div>
                        </div>
                    </div>
                ) : <ErrorMessage message={gettext('Контакты клиента будут видны после заказа всех доступных запчастей')}/>}
            </div>
        )
    }
});

var Comment = React.createClass({
    render(){
        return(<div>
            {this.props.comment &&
                    <p className='pAddres mob-lin-h'>
                        <span>{gettext('Удобное время для связи:')}</span>
                        <span className="orderComment">{this.props.comment}</span>
                    </p>
                }
        </div>);
    }
});

var OrderDetailsMixin = {

    getCommission() {
        let repairs = this.state.order.repair_order_items;
        return _.reduce(repairs, (memo, obj) => {
            return memo + parseInt(obj.commission);
        }, 0)
    },
    getCommissionWithDiscount() {
        let comission = this.getCommission();
        if (this.state.order.discount > comission) {
            return 0;
        }
        return comission - this.state.order.discount;
    },
    getTotalIncome() {
        let repairs = this.state.order.repair_order_items;
        let parts = this.state.order.parts;
        let repairsSum = _.reduce(repairs, (memo, obj) => {return memo + parseInt(obj.price); }, 0);
        let partsSum = _.reduce(parts, (memo, obj) => {return memo + (parseInt(obj.price) * parseInt(obj.quantity)); }, 0);
        let amount = Math.floor(repairsSum - this.getCommissionWithDiscount() - this.state.order.discount)
        return this.state.order.is_warranty ? Math.floor(amount +    partsSum) : Math.floor(amount - partsSum);
    },

    getGMapsLink() {
        let address = '';
        let domain='ru';
        if (window.language=='en') {
            domain='com'
        }
        if (this.state.order.address) {
            address = this.state.order.address.raw;
        } else if (this.state.order.metro) {
            address = this.state.order.metro.name;
        }
        return 'https://www.google.'+domain+'/maps/place/' + address;
    },

    getRating(){
        return this.state.order.rating;
    },

    canRejectOrder() {
        return this.state.order.master && this.state.order.status === 'new';
    },
    partsOrder() {
        location.href = `/master/parts/#/orderfor/${this.state.order.number}`;
    },
    render() {
        let reservedParts = this.state.reservedPartsLoaded ? this.state.reservedParts : [];
        return (
            <div className='content step order-details'>
                {this.state.success ? <div className='wrpOrderDetails'>
                    <h3 className='orderDetailH3'>{gettext('Заказ')} #{this.state.order.number}</h3>
                    <h3>{this.state.order.order_name}</h3>
                    <div className='container'>
                    {this.state.order.rejected_data.length > 0 && <RejectedComments comments={this.state.order.rejected_data} />}
                    <div className='one-half thumb-left'>
                        <DevicePhoto photo={this.state.order.device_photo}/>
                    </div>
                    <div className='two-half last-column addrTimeWrp'>
                        <OrderAddress metro={this.state.order.metro} />
                        <OrderTime starttime={this.state.order.starttime} endtime={this.state.order.endtime} asSoonAsPossible={this.state.order.as_soon_as_possible} createdAt={this.state.order.created_at} />
                    </div>
                    <h4>{this.state.order.device_model_color}</h4>
                    </div>
                    <div>
                        <OrderRepairTable repairs={this.state.order.repair_order_items} discount={this.state.order.discount} is_warranty={this.state.order.is_warranty}/>
                    </div>
                    <div>
                        <OrderPartsTable parts={this.state.order.parts}/>
                        <ReservedPartsTable orderNumber={this.props.params.orderId}/>
                        <br/>
                        {this.state.order.master && <button className="button button-green selectRepair" onClick={this.partsOrder}>{gettext('Заказать')}</button>}
                    </div>
                    <div className='total-income'>
                        <span className='pull-left'>{gettext('Комиссия Fixland.ru')}:&nbsp;</span>
                        <span className='pull-right'>
                            <Money amount={this.getCommissionWithDiscount()}currency="usd"
                                minus={true}/>
                        </span>
                        <br/>
                        <span className='pull-left greenText'>{gettext('Итого мой доход')}:&nbsp;</span>
                        <span className='pull-right greenText'>
                            <Money amount={this.getTotalIncome()}currency="usd"/>
                        </span>
                        <br/>
                        <span className='pull-left'>{gettext('Рейтинг за заказ')}:&nbsp;</span>
                        <span className='pull-right'>{this.getRating()}</span>
                    </div>

                    <p className='disclaimer'>* {gettext('В таблице указаны минимальные из доступных цен')}</p>
                    {this.state.order.is_warranty && <p className='disclaimer'>* {gettext('Заказ по гарантийному обращению, комиссия fixland не взымается, стоимость запчастей и доход мастера будет зачислен мастеру на счёт')}</p>}
                    <div className='decoration'></div>
                    {this.state.order.master && (this.state.userLoaded ? <OrderUser user={this.state.user} comment={this.state.order.comment} /> : <Loading />)}
                    {this.state.order.address && <p className='pAddres'>{this.state.order.address.raw}</p>}
                    <Comment comment={this.state.order.comment} />
                    <p>
                        {(!this.state.order.master || (this.state.order.master && this.state.userLoaded)) && (
                            <a className='button button-blue center-button selectRepair ' href={this.getGMapsLink()} target='blank'>{gettext('Показать на карте')}</a>
                        )}
                        {!this.state.order.master && !this.state.order.selected && (
                            <button onClick={this.selectOrder} className='button button-green center-button selectRepair' type='button'>{gettext('Выбрать')}</button>
                        )}
                        {!this.state.order.master && this.state.order.selected && <h5>{gettext('Заказ взят другим мастером')}</h5>}
                        {this.state.order.master && <CompleteOrder order={this.state.order} completeOrder={this.completeOrder}/>}
                        {this.canRejectOrder() && <Link to='rejectOrder' className='button button-red center-button selectRepair' params={{orderId: this.state.order.number}}>{gettext('Отказаться от заказа')}</Link>}
                    </p>
                </div> : (this.state.permissionDenied ? <ErrorMessage message={gettext('Нет доступа')}/> : <Loading />)}
            </div>
        );
    }

};

module.exports = OrderDetailsMixin;
