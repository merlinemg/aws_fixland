/* globals jQuery */

import _ from 'underscore';
import actions from './actions';
import Reflux from 'reflux';
import Storage from '../../common/storage';
import Utils from '../../common/utils';

var storage = new Storage();

var store = Reflux.createStore({
    storage: storage,
    listenables: [actions],
    user: null,

    cleanUser: {
        firstName: '',
        lastName: '',
        city: '',
        phone: '',
        email: '',
        password: '',
        comment: '',
        offer: false
    },

    onCleanUser() {
        this.storage.removeItem('user');
    },

    onCleanUserWhithoutId() {
        this.storage.setItem('user', _.extend(this.user, this.cleanUser));
        this.trigger(this.user);
    },

    getInitialState() {
        this.user = this.storage.getItem('user');
        if (!this.user) {
            this.user = {};
            this.user = this.cleanUser;
        }
        this.user.errors = [];
        this.user.password = '';
        return this.user;
    },

    setUserId(userId) {
        this.storage.setItem('user', {userId: userId});
        this.trigger(this.user);
    },

    setUserItem(key, value) {
        this.user[key] = value;
        this.storage.setItem('user', this.user);
        this.trigger(this.user);
    },

    removeUserKey(key) {
        delete this.user[key];
        this.storage.setItem('user', this.user);
        this.trigger(this.user);
    },

    onRemoveUserId() {
        this.removeUserKey('userId');
    },

    onSetFirstName(value) {
        this.setUserItem('firstName', value);
    },

    onSetLastName(value) {
        this.setUserItem('lastName', value);
    },

    onSetCity(value, suggestion) {
        var city = value;
        if (suggestion) {
            city = suggestion.data.city_with_type || suggestion.data.settlement_with_type || value;
            this.setUserItem('city', city);
        } else if (value) {
            var data = {'query': value, 'count': 5};
            Utils.ajaxDadataPost({
                url: 'https://dadata.ru/api/v2/suggest/address',
                data: data
            }).then((response) => {
                if (response.suggestions.length !== 0) {
                    var firstEl = response.suggestions[0].data;
                    city = firstEl.city_with_type || firstEl.settlement_with_type || value;
                }
                this.setUserItem('city', city);
                this.trigger({ajaxRequest: false, city: city});
            }).fail( () => {
                this.setUserItem('city', city);
                this.trigger({ajaxRequest: false});
            });
        } else {
            this.setUserItem('city', city);
            this.trigger({ajaxRequest: false});
        }
    },

    onSetPhone(value) {
        this.setUserItem('phone', value);
    },

    onSetEmail(value) {
        this.setUserItem('email', value);
    },

    onSetPassword(value) {
        this.setUserItem('password', value);
    },

    onSetComment(value) {
        this.setUserItem('comment', value);
    },

    onSetOffer(value) {
        this.setUserItem('offer', value);
    },

    registerMaster(data) {
        let url = '/register_master/';
        if (window.is_partner==true){
            url='/register_partner/'
        }
        Utils.ajaxPost({
            url:url,
            data: data
        }).then((response) => {
            this.setUserId(response.user_id);
            location.hash = '#/confirm';
        }).fail((resp) => {
            var responseObj = {};
            try {
                responseObj = JSON.parse(resp.response);
            } catch (e) {}
            if (responseObj.errors) {
                var errorsList = _.flatten(_.values(responseObj.errors));
                this.setUserItem('errors', errorsList);
            } else {
                location.hash = '#/server-problem';
            }
            this.setUserItem('ajaxRequest', false);
        });
    },

    onCreateMaster() {
        var data = jQuery.extend(true, {}, this.user);
        data.first_name = data.firstName; // eslint-disable-line camelcase
        data.last_name = data.lastName; // eslint-disable-line camelcase
        data.password1 = data.password;
        delete data.firstName;
        delete data.lastName;
        delete data.password;
        var query = {'query': `${data.first_name} ${data.last_name}`, 'count': 5};
        Utils.ajaxDadataPost({
            url: 'https://dadata.ru/api/v2/suggest/fio',
            data: query
        }).then( (response) => {
            if (response.suggestions.length !== 0) {
                var suggestion = response.suggestions[0];
                data.gender = suggestion.data.gender.toLowerCase();
            }
            this.registerMaster(data);
        }).fail( () => {
            this.registerMaster(data);
        });
    },

    onGetOffer() {
        Utils.ajaxGet({
            url: '/offer'
        }).then((response) => {
            this.trigger({offerHtml: response.response});
        }).fail((response) => {
            this.trigger({offerHtml: response.response});
        });
    }
});

module.exports = store;
