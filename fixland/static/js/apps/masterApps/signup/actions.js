import Reflux from 'reflux';

var actions = Reflux.createActions([
    'getGeoLocation',
    'setFirstName',
    'setLastName',
    'setCity',
    'setPhone',
    'setEmail',
    'setPassword',
    'setComment',
    'createMaster',
    'cleanUser',
    'removeUserId',
    'cleanUserWhithoutId',
    'setOffer',
    'getOffer'
]);

module.exports = actions;
