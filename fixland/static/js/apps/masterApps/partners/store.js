/* global jQuery */

import _ from 'underscore';
import actions from './actions';
import Reflux from 'reflux';
import Storage from '../../common/storage';
import Utils from '../../common/utils';
import moment from 'moment';

var storage = new Storage();

function validateDate(date){
    var date = date.split("/");
    var d = parseInt(date[0], 10);
    var m = parseInt(date[1], 10);
    var y = parseInt(date[2], 10);
    var mom = moment([y,d,m]);
    return mom.isValid();
}

function validatePhone(phone){
    if (!phone || phone.length<16 || phone.indexOf('_')!=-1){
        return false;
    }
    return true;
}

var naturalPersonStore = Reflux.createStore({
    storage: storage,
    listenables: [actions],
    form: {},

    getInitialState() {
        if (!this.form) {
            this.form = {
                lasName: null,
                firstName: null,
                middleName: null,
                email: null,
                phoneNumber: null,
                passportSeries: null,
                passportNumber: null,
                issuedBy: null,
                dateOfIssue: null,
                pensionCertificateNumber: null,
                registrationCountry: null,
                registrationRegion: null,
                registrationCity: null,
                registrationZipCode: null,
                registrationStreet: null,
                registrationHouse: null,
                registrationApartment: null,
                isTheSameAddress:false,
                residenceCountry: null,
                residenceRegion: null,
                residenceCity: null,
                residenceZipCode: null,
                residenceStreet: null,
                residenceHouse: null,
                residenceApartment: null,
            }
        }
        return {form: this.form, errors: []};
    },

    setFormFieldValue(key, value) {
        this.form[key] = value;
        this.storage.setItem('form', this.form);
    },

    cleanForm() {
        this.storage.removeItem('form');
    },

    validate() {
        let errors = {};
        if (!this.form.firstName) {
            errors.firstName = gettext('Введите имя');
        }
        if (!this.form.lastName) {
            errors.lastName = gettext('Введите фамилию');
        }
        if (!this.form.middleName) {
            errors.middleName = gettext('Введите отчество');
        }
        if (!Utils.validateEmail(this.form.email)) {
            errors.email = gettext('Введите email');
        }
        if (!validatePhone(this.form.phoneNumber)) {
            errors.phoneNumber = gettext('Введите телефон');
        }
        if (!this.form.passportSeries) {
            errors.passportSeries = gettext('Введите серию пспорта');
        }
        if (!this.form.passportNumber) {
            errors.passportNumber = gettext('Введите номер паспорта');
        }
        if (!this.form.issuedBy) {
            errors.issuedBy = gettext('Введите кем выдан паспорт');
        }
        if (!this.form.dateOfIssue || !validateDate(this.form.dateOfIssue)) {
            errors.dateOfIssue = gettext('Введите дату выдачи');
        }
        if (!this.form.pensionCertificateNumber) {
            errors.pensionCertificateNumber = gettext('Введите номер пенсионного свидетельства');
        }
        if (!this.form.registrationCountry) {
            errors.registrationCountry = gettext('Введите страну');
        }
        if (!this.form.registrationRegion) {
            errors.registrationRegion = gettext('Введите регион');
        }
        if (!this.form.registrationCity) {
            errors.registrationCity = gettext('Введите город');
        }
        if (!this.form.registrationZipCode) {
            errors.registrationZipCode = gettext('Введите индекс');
        }
        if (!this.form.registrationStreet) {
            errors.registrationStreet = gettext('Введите улицу');
        }
        if (!this.form.registrationHouse) {
            errors.registrationHouse = gettext('Введите номер дома');
        }
        if (!this.form.registrationApartment) {
            errors.registrationApartment = gettext('Введите номер квартиры');
        }
        if (!this.form.residenceCountry) {
            errors.residenceCountry = gettext('Введите страну');
        }
        if (!this.form.residenceRegion) {
            errors.residenceRegion = gettext('Введите регион');
        }
        if (!this.form.residenceCity) {
            errors.residenceCity = gettext('Введите город');
        }
        if (!this.form.residenceZipCode) {
            errors.residenceZipCode = gettext('Введите индекс');
        }
        if (!this.form.residenceStreet) {
            errors.residenceStreet = gettext('Введите улицу');
        }
        if (!this.form.residenceHouse) {
            errors.residenceHouse = gettext('Введите номер дома');
        }
        if (!this.form.residenceApartment) {
            errors.residenceApartment = gettext('Введите номер квартиры');
        }
        return errors;
    },
    submit() {
        let errors = this.validate();
        this.trigger({errors: errors});
        if (!_.isEmpty(errors)) {
            return;
        }
         var data = this.form;
        Utils.ajaxPost({
                url: '/api/',//TODO set api url
                data: data
            }).then((response) => {
                return; //TODO set success actions
            }).fail( () => {
                return; //TODO set fail actions
            });
    }
});

var legalPersonStore = Reflux.createStore({
    storage: storage,
    listenables: [actions],
    form: {},

    getInitialState() {
        if (!this.form) {
            this.form = {
                organizationName: null,
                taxpayerId: null,
                primaryStateRegistrationNumber: null,
                phoneNumber: null,
                email: null,
                directorLastName: null,
                directorFirstName: null,
                directorMiddleName: null,
                position: null,
                charterOrPowerOfAttorney: null,
                legalCountry: null,
                legalRegion: null,
                legalCity: null,
                legalZipCode: null,
                legalStreet: null,
                legalHouse: null,
                legalApartment: null,
                isTheSameAddress:false,
                actualCountry: null,
                actualRegion: null,
                actualCity: null,
                actualZipCode: null,
                actualStreet: null,
                actualHouse: null,
                actualApartment: null,
            }
        }
        return {form: this.form, errors: []};
    },

    setFormFieldValue(key, value) {
        this.form[key] = value;
        this.storage.setItem('form', this.form);
    },

    cleanForm() {
        this.storage.removeItem('form');
    },

    validate() {
        let errors = {};
        if (!this.form.organizationName) {
            errors.organizationName = gettext('Введите наименование организации/ИП');
        }
        if (!this.form.taxpayerId) {
            errors.taxpayerId = gettext('Введите ИНН');
        }
        if (!this.form.primaryStateRegistrationNumber) {
            errors.primaryStateRegistrationNumber = gettext('Введите ОГРН');
        }
        if (!validatePhone(this.form.phoneNumber)) {
            errors.phoneNumber = gettext('Введите телефон');
        }
        if (!Utils.validateEmail(this.form.email)) {
            errors.email = gettext('Введите email');
        }
         if (!this.form.directorFirstName) {
            errors.directorFirstName = gettext('Введите имя');
        }
        if (!this.form.directorLastName) {
            errors.directorLastName = gettext('Введите фамилию');
        }
        if (!this.form.directorMiddleName) {
            errors.directorMiddleName = gettext('Введите отчество');
        }
        if (!this.form.position) {
            errors.position = gettext('Введите должность');
        }
        if (!this.form.charterOrPowerOfAttorney) {
            errors.charterOrPowerOfAttorney = gettext('Введите устав или доверенность');
        }
        if (!this.form.legalCountry) {
            errors.legalCountry = gettext('Введите страну');
        }
        if (!this.form.legalRegion) {
            errors.legalRegion = gettext('Введите регион');
        }
        if (!this.form.legalCity) {
            errors.legalCity = gettext('Введите город');
        }
        if (!this.form.legalZipCode) {
            errors.legalZipCode = gettext('Введите индекс');
        }
        if (!this.form.legalStreet) {
            errors.legalStreet = gettext('Введите улицу');
        }
        if (!this.form.legalHouse) {
            errors.legalHouse = gettext('Введите номер дома');
        }

        if (!this.form.actualCountry) {
            errors.actualCountry = gettext('Введите страну');
        }
        if (!this.form.actualRegion) {
            errors.actualRegion = gettext('Введите регион');
        }
        if (!this.form.actualCity) {
            errors.actualCity = gettext('Введите город');
        }
        if (!this.form.actualZipCode) {
            errors.actualZipCode = gettext('Введите индекс');
        }
        if (!this.form.actualStreet) {
            errors.actualStreet = gettext('Введите улицу');
        }
        if (!this.form.actualHouse) {
            errors.actualHouse = gettext('Введите номер дома');
        }
        return errors;
    },
    submit() {
        let errors = this.validate();
        this.trigger({errors: errors});
        if (!_.isEmpty(errors)) {
            return;
        }
         var data = this.form;
        Utils.ajaxPost({
                url: '/api/',//TODO set api url
                data: data
            }).then((response) => {
                return; //TODO set success actions
            }).fail( () => {
                return; //TODO set fail actions
            });
    }
});

module.exports = {
    naturalPersonStore,
    legalPersonStore
};
