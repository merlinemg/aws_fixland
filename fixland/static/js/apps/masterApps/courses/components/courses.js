/* globals gettext */

import React from 'react';
import Reflux from 'reflux';
import store from '../store';
import actions from '../actions';
import Loading from '../../../common/components/loading.js';
import moment from 'moment';

var CourseButton = React.createClass({
    isSameLessonSelected() {
        return !!this.props.lessonEvents.filter((event) => {
            return (event.lesson.number === this.props.lessonEvent.lesson.number && event.selected && event.id !== this.props.lessonEvent.id);
        }).length;
    },

    selectEventLesson(eventLesson) {
        actions.selectEventLesson(eventLesson);
    },

    cancelEventLesson(eventLesson) {
        actions.cancelEventLesson(eventLesson);
    },

    render(){
        var selected = this.props.lessonEvent.selected;
        if (selected) {
            return (
                <button onClick={this.cancelEventLesson.bind(null, this.props.lessonEvent)} className='coverage-button sellCoursBtn' type='button'>{gettext('Выбрано')}</button>
            );
        } else if (this.isSameLessonSelected()) {
            return (
                <button className='hideBtn' type='button'>{gettext('Выбрать')}</button>
            );
        } else {
            return (
                <button onClick={this.selectEventLesson.bind(null, this.props.lessonEvent)} className='coverage-button notSellCoursBtn' type='button'>{gettext('Выбрать')}</button>
            );
        }
    }
});


var CourseTableRow = React.createClass({
    render() {
        var lessonEvent = this.props.lessonEvent;
        var starttime = moment(lessonEvent.starttime).format('DD-MM-YYYY HH:mm');
        return (
            <tr>
                <td className='table-sub-title'>{starttime}</td>
                <td>{lessonEvent.lesson.name}</td>
                <td>{lessonEvent.address}</td>
                <td>
                    <CourseButton lessonEvent={lessonEvent} lessonEvents={this.props.lessonEvents} />
                </td>
            </tr>
        );
    }
});


var CourseTable = React.createClass({
    render() {
        return (
            <table cellSpacing='0' className='table'>
                <thead></thead>
                <tbody>
                    {this.props.lessonEvents.map((lessonEvent) => {
                        return <CourseTableRow lessonEvent={lessonEvent} lessonEvents={this.props.lessonEvents} key={lessonEvent.id} />;
                    })}
                </tbody>
            </table>
        );
    }
});


var Courses = React.createClass({
    mixins: [Reflux.connect(store)],
    componentWillMount() {
        actions.getCourses();
    },
    getInitialState() {
        return {success: false};
    },
    onClickHandle(course){
        this.subscribeCourse(course);
    },
    render() {
        return (
            <div className='content step wrpCourseList'>
                <h3>{gettext('Программа Вашего обучения')}:</h3>
                <div className='container'>
                    {this.state.success ? <CourseTable lessonEvents={this.state.lessonEvents} /> : <Loading />}
                </div>
            </div>
        );
    }
});

module.exports = Courses;
