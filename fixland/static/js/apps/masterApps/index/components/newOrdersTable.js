/* globals jQuery, gettext */

import React from 'react';
import actions from '../actions';
import store from '../store';
import Reflux from 'reflux';
import Router from 'react-router';
import addons from 'react-addons';
import _ from 'underscore';
import Loading from '../../../common/components/loading.js';
import OrderTime from '../../common/components/orderTime';

var Link = Router.Link;
var cx = addons.classSet;

var CanWorkButton = React.createClass({
    canWork() {
        actions.setMasterCanWork(!this.props.canWork);
    },
    setTransition() {
        jQuery(React.findDOMNode(this)).addClass('transition');
    },
    componentWillMount: function() {
        this.canWork = _.debounce(this.canWork, 600);
    },
    callOtherMethods() {
        this.canWork();
        this.setTransition();
    },
    render() {
        var friendPhoneClasses = cx({
            male: window.GENDER === 'male',
            female: window.GENDER === 'female',
            'can-work-btn': !this.props.canWork,
            'can-not-work-btn': this.props.canWork
        });
        return (
            <button onClick={this.callOtherMethods} className={friendPhoneClasses}></button>
        );
    }
});


var OrderTableRow = React.createClass({
    render() {
        let button_text = this.props.order.selected ? gettext('Выбрано') : gettext('Смотреть');
        let selectBtnCls = cx({
            button: true,
            'view-btn': true,
            'coverage-button': !this.props.order.selected,
            'selectedBtn': this.props.order.selected
        });
        let rowCls = cx({
            selectedOrderRow: this.props.order.selected,
        });
        return (
            <tr className={rowCls}>
                <td className='table-sub-title'><OrderTime starttime={this.props.order.starttime} endtime={this.props.order.endtime} asSoonAsPossible={this.props.order.as_soon_as_possible} createdAt={this.props.order.created_at} /></td>
                <td className='linkTd'>
                    <Link to='newOrderDetails' params={{orderId: this.props.order.number}}>{this.props.order.order_name}
                        {this.props.order.is_rejected && <span> ({gettext('отказной')})</span>}
                        {this.props.order.is_warranty && <span> ({gettext('гарантийный')})</span>}
                    </Link>
                </td>

                <td><Link to='newOrderDetails' params={{orderId: this.props.order.number}} className={selectBtnCls}>{gettext(button_text)}</Link></td>
                <td>{this.props.order.total_amount_with_discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\'')} <span className='rub'> ₽ </span></td>
            </tr>
        );
    }
});

var Table = React.createClass({
    render() {
        return (
            <div>
                {this.props.orders ? <table cellSpacing='0' className='table'>
                    <thead></thead>
                    <tbody>
                        {this.props.orders.map((order) => {
                            if (!order.master){
                                return <OrderTableRow order={order} />;
                            }
                        })}
                    </tbody>
                </table> : <Loading />}
                {this.props.orders && !this.props.orders.length && <h5 className="notifForMasterOrders">{gettext('В Вашем городе нет свободных заказов')}</h5>}
                <CanWorkButton canWork={this.props.canWork}/>
            </div>
        );
    }
});

var OrdersTable = React.createClass({
    mixins: [Reflux.connect(store)],

    componentWillMount() {
        actions.getMasterCanWork();
    },

    render() {
        return (
            <div className='content step wrpNewOrderTables'>
                <h3>{gettext('Личный кабинет мастера')}</h3>
                <div className='container'>
                    {this.state.success ? <div>
                        {this.state.canWork ? <Table orders={this.state.orders} canWork={this.state.canWork}/> : <CanWorkButton />}
                        </div> : <Loading />}
                </div>
            </div>
        );
    }

});

module.exports = OrdersTable;
