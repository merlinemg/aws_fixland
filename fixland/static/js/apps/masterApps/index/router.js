import React from 'react';
import ReactRouter from 'react-router';
import NewOrdersTable from './components/newOrdersTable';
import newOrderDetails from './components/newOrderDetails';
import ServerProblem from '../../common/components/orderServerProblem';

var {Route, RouteHandler} = ReactRouter;


var App = React.createClass({

    componentDidMount(){
        if (window.is_production){
            fbq('track', 'ViewContent');
        }
    },

    render: function() {
        return (
            <RouteHandler />
        );
    }
});

var masterIndexRoutes = (
    <Route name='app' handler={App}>
        <Route name='newOrdersTable' path='/' handler={NewOrdersTable} />
        <Route name='newOrderDetails' path='/order/:orderId' handler={newOrderDetails} />
        <Route name='serverProblem' path='/server-problem' handler={ServerProblem} />
    </Route>
);

module.exports = masterIndexRoutes;
