/* globals gettext */

import React from 'react';
import _ from 'underscore';

var Progress = React.createClass({
    render () {
        var style = {
            width: `${this.props.progress}%`
        };
        return <div className='progress' style={style}></div>;
    }
});

var Steps = React.createClass({
    handleBackClick() {
        history.go(-1);
    },
    moveTo(link) {
        location.hash = link;
    },
    render() {
        return (
            <ul className = "breadcrumbs">
                <li><button className="nav-back" onClick={this.handleBackClick}><i className="fa fa-arrow-left fa-2x backBtn"></i></button></li>
                {this.props.items.map((item) => {
                    return (
                        <li onClick={this.moveTo.bind(this, item.link)}><span>{item.title}</span><br/><span>{item.text}</span>
                            {item.title === 'Сумма' && <span className="rub">₽</span>}
                        </li>
                    );
                })}
            </ul>
        );
    }
});

var BreadcrumbsMixin = {
    getBreadcrumbs() {
        var params = this.props.params,
            sell = this.props.sell,
            breadcrumbs = [],
            progressStep = 0,
            maxStep = sell ? 10 : 8,
            data = {};

        if (!_.isEmpty(params)) {
            if (this.props.route === 'repairCost' && this.state.selectedRepairs.length) {
                breadcrumbs.push(this.getFullDevice());
                progressStep = 4;
            } else if (this.props.route === 'sellCost') {
                breadcrumbs.push(this.getFullDevice());
                progressStep = 6;
            } else {
                _.keys(params).forEach( (key) => {
                    if (key === 'device') {
                        data.device = _.findWhere(window.DEVICES, {slug: params.device});
                        breadcrumbs.push({
                            title: gettext('Устройство'),
                            text: data.device.name,
                            link: '/'
                        });
                    }

                    if (key === 'model') {
                        data.model = _.findWhere(data.device.models, {slug: params.model});
                        breadcrumbs.push({
                            title: gettext('Модель'),
                            text: data.model.name,
                            link: `/${params.device}`
                        });
                    }

                    if (key === 'color') {
                        data.color = _.findWhere(data.model.colors, {slug: params.color});
                        breadcrumbs.push({
                            title: gettext('Цвет'),
                            text: data.color.name,
                            link: `/${params.device}/${params.model}`
                        });
                    }

                    if (key === 'memory') {
                        breadcrumbs.push({
                            title: gettext('Объем памяти'),
                            text: params.memory,
                            link: `/${params.device}/${params.model}`
                        });
                    }

                    if (key === 'version' && params.version !== '-') {
                        var version = _.findWhere(data.model.versions, {slug: params.version});
                        breadcrumbs.push({
                            title: gettext('Версия'),
                            text: version.name,
                            link: `/${params.device}/${params.model}/${params.memory}`
                        });
                    }

                    if (key === 'status') {
                        data.status = _.findWhere(window.STATUSES, {slug: params.status});
                        breadcrumbs.push({
                            title: gettext('Состояние'),
                            text: data.status.name,
                            link: `/${params.device}/${params.model}/${params.memory}/${params.version}`
                        });
                    }

                });
                progressStep = breadcrumbs.length;
            }
        } else if (['orderStep1', 'orderStep2', 'orderStep3', 'regionDiff'].indexOf(this.props.route) !== -1) {
            if (this.state.userDevice || this.state.modelSell) {
                breadcrumbs.push(this.getFullDevice());
                breadcrumbs.push({
                    title: gettext('Сумма'),
                    text: this.getPrice().toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\''),
                    link: sell ? _.last(breadcrumbs).link + '/cost' : _.last(breadcrumbs).link
                });
                progressStep = 5;
                progressStep = sell ? 7 : 5;
                if (this.props.route === 'orderStep2' || this.props.route === 'regionDiff') {
                    progressStep++;
                }
                if (this.props.route === 'orderStep3') {
                    progressStep = progressStep + 2;
                }
            }
        }
        return {items: breadcrumbs, progress: parseInt((100 / maxStep) * progressStep)};
    },
    render() {
        return (
            <div className = "breadcrumbs-nav">
            <Progress progress={this.getBreadcrumbs().progress}/>
            { this.getBreadcrumbs().items.length ? <Steps items={this.getBreadcrumbs().items}/> : <span></span> }
            </div>
        );
    }
};

module.exports = BreadcrumbsMixin;
