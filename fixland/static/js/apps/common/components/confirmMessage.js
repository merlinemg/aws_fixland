import React from 'react';


var ConfirmMessage = React.createClass({
    propTypes: {
        message: React.PropTypes.string.isRequired
    },
    componentDidMount() {
        jQuery('html, body').animate({ scrollTop: 0 }, 'fast');
    },
    render() {
        return (
            <div className="error static-notification-green clearBoth tap-dismiss-notification">
                <p className="center-text uppercase">{this.props.message}</p>
            </div>
        );
    }
});

module.exports = ConfirmMessage;
