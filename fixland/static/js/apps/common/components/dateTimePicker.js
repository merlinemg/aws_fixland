/* globals jQuery, gettext */

import React from 'react';
import moment from 'moment';
import addons from 'react-addons';

let days = [gettext('Воскресенье'), gettext('Понедельник'), gettext('Вторник'), gettext('Среда'), gettext('Четверг'), gettext('Пятница'), gettext('Суббота')];
let months = [gettext('Янв'), gettext('Фев'), gettext('Мар'), gettext('Апр'), gettext('Май'), gettext('Июн'), gettext('Июл'), gettext('Авг'), gettext('Сен'), gettext('Окт'), gettext('Ноя'), gettext('Дек')];

module.exports = React.createClass({
    getInitialState() {
        let date, day, startTime, endTime, showDays;
        if (!this.props.startDateTime || !this.props.endDateTime) {
            date = moment();
            day = date.hour() > 20 ? 1 : 0;
            let startHour = this.getFromHour(day);
            let endHour = startHour + 2;
        } else {
            date = moment();
            let startDateTime = moment(this.props.startDateTime);
            let endDateTime = moment(this.props.endDateTime);
            day = date.date() < startDateTime.date() ? startDateTime.date() - date.date() : (date.hour() > 20 ? 1 : 0);
            let startTime = startDateTime.hour();
            if (day === 0) {
                startTime = startDateTime.hour() < date.hour() ? this.getFromHour(day) : startDateTime.hour();
            }
            if (startTime === startDateTime.hour()) {
                endTime = endDateTime.hour();
            } else {
                endTime = startTime + 2;
            }
        }
        if (day > 1) {
            showDays = true;
        }
        return {
            date: date,
            day: day,
            startTime: startTime,
            endTime: endTime,
            showDays: showDays,
        }
    },
    componentDidMount() {
        let date, day, endTime;
        if (!this.props.startDateTime || !this.props.endDateTime) {
            this.props.setStartDateTime(moment({hour: this.state.startHour}).format('YYYY-MM-DD HH:mm'));
            this.props.setEndDateTime(moment({hour: this.state.endHour}).format('YYYY-MM-DD HH:mm'));
            if (this.state.day > 0) {
                jQuery(React.findDOMNode(this.refs.todayBtn)).prop( 'disabled', true );
            }
        } else {
            if (this.state.day === 0) {
                this.props.setStartDateTime(moment({hour: this.state.startTime}).format('YYYY-MM-DD HH:mm'));
                this.props.setEndDateTime(moment({hour: this.state.endTime}).format('YYYY-MM-DD HH:mm'));
            }
            if (this.state.day > 0 && date.hour() > 20) {
                jQuery(React.findDOMNode(this.refs.todayBtn)).prop( 'disabled', true );
            }
        }
    },

    generateTimeArray(from, to) {
        let arr = [];
        for (let i = from; i <= to; i++) {
            arr.push({
                'value': i,
                'text': i + ':00'
            });
        }
        return arr;
    },

    getFromHour(day) {
        let fromHour = 9;
        let nowHour = moment().hour();
        if (day === 0) {
            fromHour = nowHour < 9 ? 9 : nowHour + 1;
        }
        return fromHour;
    },

    getTimeFrom() {
        return this.generateTimeArray(this.getFromHour(this.state.day), 21);
    },

    getTimeTo() {
        if (this.state.startTime >= 20){
            return this.generateTimeArray(parseInt(this.state.startTime) + 2, parseInt(this.state.startTime) + 2);
        } else {
            return this.generateTimeArray(parseInt(this.state.startTime) + 2, 21);
        }
    },

    getDays() {
        let arr = [];
        for (let i = 2; i <= 9; i++) {
            let date = moment().add(i, 'd');
            arr.push({
                'value': i,
                'text': `${days[date.day()]}, ${date.date()} ${months[date.month()]}`
            });
        }
        return arr;
    },

    updateStartDayTime(hour, day) {
        let dateTime = moment({hour: parseInt(hour)}).add(parseInt(day), 'd');
        this.props.setStartDateTime(dateTime.format('YYYY-MM-DD HH:mm'));
    },

    updateEndDayTime(hour, day) {
        let dateTime = moment({hour: parseInt(hour)}).add(parseInt(day), 'd');
        this.props.setEndDateTime(dateTime.format('YYYY-MM-DD HH:mm'));
    },

    handleTodayClick() {
        this.setState({
            startTime: this.getFromHour(0),
            endTime: this.getFromHour(0) + 2,
            showDays: false,
            day: 0
        });
        this.updateStartDayTime(this.getFromHour(0), 0);
        this.updateEndDayTime(this.getFromHour(0) + 2, 0);
    },
    handleTomorrowClick() {
        this.setState({
            showDays: false,
            day: 1
        });
        this.updateStartDayTime(this.state.startTime, 1);
        this.updateEndDayTime(this.state.endTime, 1);
    },
    handleLaterClick() {
        this.setState({
            showDays: true,
            day: 2
        });
        this.updateStartDayTime(this.state.startTime, 2);
        this.updateEndDayTime(this.state.endTime, 2);
    },

    handleDayChange(event) {
        this.setState({day: event.target.value});
        this.updateStartDayTime(this.state.startTime, event.target.value);
        this.updateEndDayTime(this.state.endTime, event.target.value);
    },

    handleStartTimeChange(event) {
        let hour = event.target.value;
        this.setState({startTime: hour});
        this.updateStartDayTime(hour, this.state.day);
        if (hour > 19 || parseInt(this.state.endTime) > 21 || (parseInt(this.state.endTime) - parseInt(hour)) < 2) {
            this.setState({endTime: parseInt(hour) + 2});
            this.updateEndDayTime(parseInt(hour) + 2, this.state.day);
        }
    },

    handleEndTimeChange(event) {
        this.setState({endTime: event.target.value});
        this.updateEndDayTime(event.target.value, this.state.day);
    },

    render() {
        let cx = addons.classSet;
        let todayButtonClasses = cx({
            'coverpage-button': true,
            active: this.state.day === 0 && !this.state.showDays
        });
        let tomorrowButtonClasses = cx({
            'coverpage-button': true,
            active: this.state.day === 1 && !this.state.showDays
        });
        let laterButtonClasses = cx({
            'coverpage-button': true,
            active: this.state.showDays
        });
        return (
            <div className='wrappTimePicker'>
                <div className='wrapperselectTime wrpSellTime'>
                    <button ref='todayBtn' className={todayButtonClasses} onClick={this.handleTodayClick}>{gettext('Сегодня')}</button>
                    <button className={tomorrowButtonClasses} onClick={this.handleTomorrowClick}>{gettext('Завтра')}</button>
                    <button className={laterButtonClasses} onClick={this.handleLaterClick}>{gettext('Позже')}</button>
                </div>
                <div className='demos'>
                    {this.state.showDays && <select ref='days' className='inputData sellDate' value={this.state.day} onChange={this.handleDayChange}>
                        {this.getDays().map(day => {
                            return (
                                <option value={day.value}>{day.text}</option>
                            );
                        })}
                    </select>}
                    <div className="pseudoSelect pseudoStartTime">
                        <select ref='startDate' className='inputData startTimeSell' value={this.state.startTime} onChange={this.handleStartTimeChange}>
                            {this.getTimeFrom().map(hour => {
                                return (
                                    <option value={hour.value}>{hour.text}</option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="pseudoSelect pseudoEndtTime">
                        <select ref='endDate' className='inputData endTimeSell' value={this.state.endTime} onChange={this.handleEndTimeChange}>
                            {this.getTimeTo().map(hour => {
                                return (<option value={hour.value}>{hour.text}</option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </div>
        );
    }
});
