class FallbackStorage {
    constructor() {
        this._data = {};
    }

    setItem(key, value) {
        this._data[key] = value;
    }

    getItem(key) {
        var value = this._data[key];
        if (value) {
            return value;
        } else {
            return null;
        }
    }

    removeItem(key) {
        delete this._data[key];
    }
}

class Storage {
    constructor() {
        try {
            var test = 'test';
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            this.localStorage = localStorage;
        } catch (e) {
            this.localStorage = new FallbackStorage();
        }
    }

    createLocalStorage() {
        return;
    }

    setItem(key, value) {
        this.localStorage.setItem(key, JSON.stringify(value));
    }

    getItem(key) {
        return JSON.parse(this.localStorage.getItem(key));
    }

    removeItem(key) {
        this.localStorage.removeItem(key);
    }

}

module.exports = Storage;
