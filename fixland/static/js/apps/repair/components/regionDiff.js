/* globals gettext */

import _ from 'underscore';
import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import store from '../store';
import Money from '../../common/components/money.js';
import utils from '../utils';

var Link = Router.Link;

module.exports = React.createClass({
    mixins: [Reflux.connect(store), Router.Navigation],
    getNewPrice(repair) {
        return utils.getRepairPrice(repair, this.state.parsedAddress.region);
    },
    change() {
        const params = {
            'device': this.state.userDevice,
            'model': this.state.userModel,
            'color': this.state.userColor,
        };
        window.CLIENT_REGION.dadata_name = this.state.parsedAddress.region;
        this.transitionTo('selectRepair', params);
    },
    continue() {
        window.CLIENT_REGION.dadata_name = this.state.parsedAddress.region;
        this.transitionTo('orderStep3');
    },
    render() {
        return (
            <div className="content step">
                <h3></h3>
                <h5 className="descrSelRepair regionDiffh5">{gettext('Обратите внимание, что в Вашем регионе действуют другие цены:')}</h5>
                <table className="diffTable">
                    {this.state.selectedRepairs.map(repair => {
                        return (
                            <tr>
                                <td>
                                    <span className="hideDotted">{repair.name}</span>
                                </td>
                                <td className="td-price priceDiff"><Money amount={this.getNewPrice(repair)}/></td>
                            </tr>
                        );
                    })}
                </table>
                <button className="button button-green selectRepair margin-right" onClick={this.continue}>{gettext('Продолжить')}</button>
                <button className="button button-blue selectRepair noneSpan" onClick={this.change}>{gettext('Изменить')}</button>
            </div>
        );
    }
});
