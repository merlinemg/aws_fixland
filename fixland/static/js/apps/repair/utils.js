import _ from 'underscore';

var Utils = {
    validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    },

    getRepairPrice(repair, region_name) {
        if (!region_name) region_name = window.CLIENT_REGION.dadata_name;
        let price = _.findWhere(repair.prices, {region_name: region_name});
        if (!price) {
            price = _.findWhere(repair.prices, {default: true});
        }
        return price ? price.price : 0;
    }
};

module.exports = Utils;
