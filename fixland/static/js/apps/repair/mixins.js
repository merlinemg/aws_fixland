var RepairsRequiredMixin = {
    componentWillMount() {
        if (!this.state.selectedRepairs.length) {
            location.replace('/');
        }
    }
};

module.exports = RepairsRequiredMixin;
