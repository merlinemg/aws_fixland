/* globals gettext*/

import _ from 'underscore';
import React from 'react';
import Router from 'react-router';

var Link = Router.Link;

var SelectMemory = React.createClass({
    getInitialState() {
        var params = this.props.params;
        return {
            status: _.findWhere(window.STATUSES, {slug: params.status})
        };
    },
    getParams() {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            memory: this.props.params.memory,
            version: this.props.params.version,
            status: this.props.params.status
        };
    },
    render() {
        return (
            <div className="content step statusDescrWrp">
                <h3>{gettext('Состояние')} {this.state.status.name} {gettext('означает что всё с ниже перечисленого - правда')}</h3>
                <ul className="ulList">
                    {this.state.status.description_list.map( description => {
                        return (
                            <li>{description}</li>
                        );
                    })}
                </ul>
                <Link to='sellCost' params={this.getParams()} className="button button-green center-button showCostSell">
                    {gettext('ПОСМОТРЕТЬ ЦЕНУ ПРОДАЖИ')}
                </Link>

            </div>
        );
    }
});

module.exports = SelectMemory;
