/* globals jQuery, gettext */

import _ from 'underscore';
import React from 'react';
import Router from 'react-router';
import addons from 'react-addons';
import actions from '../actions';
import Reflux from 'reflux';
import store from '../store';

var Link = Router.Link;

var SelectStatus = React.createClass({
    mixins: [Reflux.connect(store)],
    getStatuses() {
        var params = this.props.params;
        return _.findWhere(_.findWhere(window.DEVICES, {slug: this.props.params.device}).models, {slug: params.model}).statuses;
    },

    componentWillMount() {
        if (!this.props.params.status && this.state.statusCache && this.props.params.model === this.state.modelCache) {
            location.hash = location.hash + `/${this.state.statusCache}`;
        }
        actions.setModelCache(this.props.params.model);
        if (this.props.params.status) {
            actions.setStatusCache(this.props.params.status);
        }
    },

    componentDidMount() {
        var legendHeight = jQuery('#breadcrumbs').height();
        if (legendHeight > 120) {
            jQuery('.step').addClass('moreMargin');
        }
    },

    componentDidUpdate() {
        var legendHeight = jQuery('#breadcrumbs').height();
        if (legendHeight > 120) {
            jQuery('.step').addClass('moreMargin');
        }
    },

    getParams(status) {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            memory: this.props.params.memory,
            version: this.props.params.version,
            status: status.slug
        };
    },

    getTitle() {
        var slag = _.findWhere(window.STATUSES, {slug: this.props.params.status});
        if (!slag) {
            return '';
        }
        var allText = slag.title;
        var objTitle = {
            hasCharacter: function() {
                return allText.indexOf('Ё') > -1;
            },
            allText: allText,
            firstHalf: allText.split('Ё')[0],
            secondHalf: allText.split('Ё')[1],
        }
        return objTitle;
    },

    getStatus() {
        return _.findWhere(window.STATUSES, {slug: this.props.params.status});
    },

    handleStatusChange(status) {
        actions.setStatusCache(status);
    },

    render() {
        var title = this.getTitle();
        return (
            <div className="content step">
                <h3>{gettext('В каком состоянии Ваш телефон?')}</h3>
                <div className="wrpSellStatus">
                    {this.getStatuses().map(status => {
                        var cx = addons.classSet;
                        var classes = cx({
                            deviceModel: true,
                            deviceStatus: true,
                            selectBtn: true,
                            active: this.props.params.status && this.props.params.status === status.slug
                        });
                        return (
                            <Link to='statusDescription' onClick={this.handleStatusChange.bind(this, status.slug)} params={this.getParams(status)} className={classes}>
                                <span className="statusSpan">{status.name}</span>
                            </Link>
                        );
                    })}
                </div>
                { this.props.params.status ? <div>
                    { title.hasCharacter() ? <h4 className="statusH4">{ title.firstHalf }<span className="rub">Ё</span>{ title.secondHalf }</h4> :
                        <h4 className="statusH4">{ title.allText }</h4>
                    }
                    <ul className="ulList icon-list">
                        {this.getStatus().description_list.map( description => {
                            return (
                                <li className="tick-list">{description}</li>
                            );
                        })}
                    </ul>
                    <Link to='sellCost' params={this.props.params} className="button button-green center-button selectRepair sellBtn">
                        {gettext('ПОСМОТРЕТЬ ЦЕНУ ПРОДАЖИ')}
                    </Link>
                </div> : '' }
            </div>
        );
    }
});

module.exports = SelectStatus;
