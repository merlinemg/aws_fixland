/* globals gettext */

import _ from 'underscore';
import React from 'react';
import Router from 'react-router';

var Link = Router.Link;

var SelectMemory = React.createClass({
    getInitialState() {
        var params = this.props.params;
        return {
            'memory_list': _.findWhere(_.findWhere(window.DEVICES, {slug: this.props.params.device}).models, {slug: params.model}).memory_list
        };
    },
    getModelMemoryVersions(memory) {
        var params = this.props.params;
        var modelsSell = _.findWhere(_.findWhere(window.DEVICES, {slug: this.props.params.device}).models, {slug: params.model}).sell;
        return _.pluck(_.filter(modelsSell, modelSell => {
            return !!modelSell.version && modelSell.memory === memory;
        }), 'version');
    },
    getParams(memory) {
        return {
            device: this.props.params.device,
            model: this.props.params.model,
            memory: memory.slug
        };
    },
    render() {
        return (
            <div className="content step">
                <h3 className="devModelh3">{gettext('Укажите объем памяти')}</h3>
                <div>
                    {this.state.memory_list.map(memory => {
                        if (this.getModelMemoryVersions(memory.slug).length) {
                            return (
                                <Link to='selectVersion' params={this.getParams(memory)} className="deviceModel selectBtn">{memory.name}</Link>
                            );
                        } else {
                            let params = _.extend(this.getParams(memory), {version: '-'});
                            return (
                                <Link to='selectStatus' params={params} className="deviceModel selectBtn">{memory.name}</Link>
                            );
                        }
                    })}
                </div>
            </div>
        );
    }
});

module.exports = SelectMemory;
