import actions from '../actions';
import React from 'react';
import Router from 'react-router';
import Reflux from 'reflux';
import store from '../store';
import GetGeolocation from './getGeolocation';

import OrderStep2Mixin from '../../common/components/orderStep2Mixin';

module.exports = React.createClass({
    GetGeolocation: GetGeolocation,
    actions: actions,
    mixins: [
        Reflux.connect(store),
        Reflux.listenTo(store, 'storeTrigger'),
        Router.Navigation,
        OrderStep2Mixin
    ],
    storeTrigger(data) {
        if (data.addressUpdated) {
            this.transitionTo('orderStep3');
        }
    },
    componentWillMount() {
        if (!(this.state.isSoonAsPossible === true || this.state.startDateTime || this.state.endDateTime)) {
            location.replace('/');
        }
    }
});
