import _ from 'underscore';
import actions from './actions';
import Reflux from 'reflux';

import baseStore from '../common/store';

var store = Reflux.createStore({
    placeOrderUrl: '/orders/sell/',
    listenables: [actions],
    orderKey: 'sellOrder',
    mixins: [baseStore],

    onCleanOrder() {
        this.storage.removeItem(this.orderKey);
        this.storage.setItem(this.orderKey, _.extend(this.order, {
            startDateTime: null,
            endDateTime: null,
            isSoonAsPossible: null,
            userDevice: null,
            userModel: null,
            userColor: null,
            modelCache: null,
            statusCache: null,
            modelSell: null
        }));
        this.trigger(this.order);
    },
    onSetModelSell(modelSell) {
        this.setOrderItem('modelSell', modelSell.key);
    },
    onSetStatusCache(value) {
        this.setOrderItem('statusCache', value);
    }
});

module.exports = store;
