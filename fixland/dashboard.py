"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'fixland.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        # append a group for "Administration" & "Applications"
        self.children.append(modules.Group(
            _('Администрирование'),
            column=1,
            collapsible=True,
            children=[
                modules.AppList(
                    _('База данных'),
                    column=1,
                    collapsible=False,
                    models=('*',),
                ),
            ]
        ))
        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))

        self.children.append(modules.LinkList(
            title=_('Ссылки'),
            column=2,
            children=(
                [_('Перевод сайта'), reverse('rosetta-home')],
                [_('Отчеты v1'), '/report_builder/'],
                [_('Импортировать цены и комиссии'), '/import_data_from_onedrive'],
                [_('Импортировать детали поставщиков'), '/import_parts_from_remote_excel_file'],
                [_('Импортировать детали поставщиков v2'), '/import_parts_from_remote_xml_file/'],
                [_('Пересчитать рейтинг мастеров'), '/recalculate_masters_rating/'],
                [_('Пересчитать рейтинг мастеров на основе логов рейтинга'), '/fix_masters_rating/'],
                [_('Импортировать станции метро'), '/import_metro_stations/'],
                [_('Импортировать артикулы поставщиков'), '/import_vendor_articles_from_onedrive'],
            )
        ))
