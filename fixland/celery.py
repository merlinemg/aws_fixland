from __future__ import absolute_import
import os

from celery import Celery
from celery.signals import setup_logging
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fixland.settings')

celery_app = Celery('fixland')

celery_app.config_from_object('django.conf:settings')
celery_app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

if not settings.DEBUG:

    if settings.LOGGING:
        @setup_logging.connect
        def project_setup_logging(loglevel, logfile, format, colorize, **kwargs):
            import logging.config
            logging.config.dictConfigClass(settings.LOGGING).configure()

    from raven.contrib.django.models import client
    from raven.contrib.celery import register_signal

    register_signal(client)
