DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fixland_db_2',
        'USER': "postgres",
        'PASSWORD': "qwerty",
        'PORT': '5432',
        'HOST': 'localhost',
        'STORAGE_ENGINE': 'MYISAM',
    }
}

DEBUG = True
