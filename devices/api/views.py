from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from celery.result import AsyncResult

from masters.api.permissions import IsMaster
from orders.models import RepairOrder
from .serializers import PartSerializer, VendorPartSerializer
from ..models import Part, VendorPart
from ..bl import get_warehouses_by_master_regions, get_warehouses_by_order_number
from ..bl import get_vendors_by_warehouses
from ..vendors.tasks import update_availability_for_vendors


class PartViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Part.objects.all()
    serializer_class = PartSerializer
    permission_classes = (IsMaster,)
    renderer_classes = (JSONRenderer,)

    def get_queryset(self):
        ids = self.request.GET.get('ids')
        model = self.request.query_params.get('model', None)
        if ids:
            ids_list = ids.split(',')
            self.queryset = self.queryset.filter(id__in=ids_list)
        if model:
            self.queryset = self.queryset.filter(part_repairs__model_repair__device_model__slug=model).distinct()
        return self.queryset


class VendorPartViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = VendorPart.objects.all()
    serializer_class = VendorPartSerializer
    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    def get_queryset(self):
        queryset = self.queryset
        part_ids = self.request.GET.get('part_ids')
        if part_ids:
            queryset = queryset.select_related('warehouse', 'warehouse__city', 'warehouse__region').filter(
                exists=True,
                vendor__integration_soap1s_url__isnull=False,
                vendor__integration_soap1s_username__isnull=False,
                vendor__integration_soap1s_password__isnull=False,
                vendor__integration_soap1s_api_key__isnull=False,
            )
            part_list = part_ids.split(',')
            queryset = queryset.filter(part_id__in=part_list)
            for_order = self.request.GET.get('for_order')
            if for_order:
                warehouses = get_warehouses_by_order_number(for_order)
                queryset = queryset.filter(warehouse__in=warehouses)
            else:
                master = self.request.user.master
                warehouses = get_warehouses_by_master_regions(master)
                queryset = queryset.filter(warehouse__in=warehouses)
        return queryset


class UpdatePartsAvailability(RetrieveAPIView):
    serializer_class = None
    permission_classes = (IsMaster,)
    renderer_classes = (JSONRenderer,)

    def get(self, request, *args, **kwargs):
        part_ids = self.request.GET.get('part_ids')
        for_order = self.request.GET.get('for_order')
        part_list = None
        if part_ids:
            part_list = part_ids.split(',')
        elif for_order:
            repair_order = RepairOrder.objects.filter(number=for_order).first()
            if repair_order:
                order_parts = repair_order.get_order_parts()
                part_list = [i.part_id for i in order_parts]
        if not part_list:
            return Response({'result': False})
        if for_order:
            warehouses = get_warehouses_by_order_number(for_order)
        else:
            master = request.user.master
            warehouses = get_warehouses_by_master_regions(master)
        vendors = get_vendors_by_warehouses(warehouses)
        result = update_availability_for_vendors.delay(vendors.values_list('id', flat=True), part_list)
        return Response({'result': True, 'task_id': result.id})


class UpdatePartsStatus(RetrieveAPIView):
    serializer_class = None
    permission_classes = (IsMaster,)
    renderer_classes = (JSONRenderer,)

    def get(self, request, *args, **kwargs):
        task_id = self.request.GET.get('task_id')
        if not task_id:
            return Response({'result': False})
        result = AsyncResult(task_id)
        return Response({'result': result.status.lower()})
