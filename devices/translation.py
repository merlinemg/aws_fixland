''' Translations for devices models '''

from modeltranslation.translator import translator, TranslationOptions

from .models import DeviceColor, Repair, Status, Part


class DeviceColorTranslationOptions(TranslationOptions):
    fields = ('name', )


class RepairTranslationOptions(TranslationOptions):
    fields = ('name', )


class StatusTranslationOptions(TranslationOptions):
    fields = ('name', 'title', 'description')


class PartTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(DeviceColor, DeviceColorTranslationOptions)
translator.register(Repair, RepairTranslationOptions)
translator.register(Status, StatusTranslationOptions)
translator.register(Part, PartTranslationOptions)
