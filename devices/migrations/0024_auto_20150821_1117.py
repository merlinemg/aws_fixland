# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0023_auto_20150817_1458'),
    ]

    operations = [
        migrations.AddField(
            model_name='devicemodel',
            name='visible',
            field=models.BooleanField(default=True, verbose_name='Отображать?'),
        ),
        migrations.AddField(
            model_name='repair',
            name='visible',
            field=models.BooleanField(default=True, verbose_name='Отображать?'),
        ),
        migrations.AddField(
            model_name='devicemodelrepair',
            name='visible',
            field=models.BooleanField(default=True, verbose_name='Отображать?'),
        ),
    ]
