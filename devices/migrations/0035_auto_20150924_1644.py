# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0034_part_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='color',
            field=models.ForeignKey(blank=True, default=None, null=True, to='devices.DeviceColor', verbose_name='Цвет запчасти', editable=False),
        ),
    ]
