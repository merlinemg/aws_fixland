# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0005_auto_20150501_1021'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='device',
            options={'verbose_name': 'Устройство', 'verbose_name_plural': 'Устройства', 'ordering': ('-priority',)},
        ),
        migrations.AlterModelOptions(
            name='devicecolor',
            options={'verbose_name': 'Цвет устройства', 'verbose_name_plural': 'Цвета устройств'},
        ),
        migrations.AlterModelOptions(
            name='devicemodel',
            options={'verbose_name': 'Модель', 'verbose_name_plural': 'Модели', 'ordering': ('-priority',)},
        ),
        migrations.AddField(
            model_name='devicecolor',
            name='text_color',
            field=models.CharField(max_length=6, choices=[('000000', 'Черный'), ('ffffff', 'Белый')], default='000000'),
        ),
    ]
