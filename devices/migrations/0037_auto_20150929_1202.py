# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def copy_price_to_device_model_repair_price(apps, schema_editor):
    DeviceModelRepair = apps.get_model("devices", "DeviceModelRepair")
    DeviceModelRepairPrice = apps.get_model("devices", "DeviceModelRepairPrice")
    Region = apps.get_model("accounts", "Region")
    regions = Region.objects.filter(dadata_name__in=['Москва', 'Московская'])
    if not regions:
        return
    for item in DeviceModelRepair.objects.all():
        for region in regions:
            DeviceModelRepairPrice.objects.create(
                device_model_repair=item,
                price=item.price,
                region=region
            )


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0030_auto_20150922_1805'),
        ('devices', '0036_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelRepairPrice',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('price', models.DecimalField(decimal_places=2, verbose_name='Цена', max_digits=14)),
            ],
            options={
                'verbose_name': 'Цена поломки модели',
                'verbose_name_plural': 'Цены поломок моделей',
            },
        ),
        migrations.AlterModelOptions(
            name='part',
            options={'verbose_name': 'Наша запчасть', 'verbose_name_plural': 'Наши запчасти'},
        ),
        migrations.AlterField(
            model_name='devicemodelrepair',
            name='price',
            field=models.DecimalField(editable=False, decimal_places=2, max_digits=14),
        ),
        migrations.AlterField(
            model_name='partarticle',
            name='vendor_article',
            field=models.CharField(max_length=255, blank=True, verbose_name='Артикул поставщика', null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='vendorpart',
            name='article',
            field=models.CharField(max_length=255, blank=True, verbose_name='Артикул поставщика', null=True, db_index=True),
        ),
        migrations.AddField(
            model_name='devicemodelrepairprice',
            name='device_model_repair',
            field=models.ForeignKey(related_name='prices', verbose_name='Поломка модели', to='devices.DeviceModelRepair'),
        ),
        migrations.AddField(
            model_name='devicemodelrepairprice',
            name='region',
            field=models.ForeignKey(verbose_name='Регион', to='accounts.Region'),
        ),
        migrations.RunPython(copy_price_to_device_model_repair_price)
    ]
