# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0021_auto_20150811_1703'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendor',
            name='email',
            field=models.CharField(db_index=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='vendor',
            name='last_imported_at',
            field=models.DateTimeField(null=True),
        ),
    ]
