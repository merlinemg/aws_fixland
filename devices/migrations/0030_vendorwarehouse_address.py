# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0029_auto_20150910_1224'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendorwarehouse',
            name='address',
            field=models.CharField(verbose_name='Адрес', max_length=255, blank=True, null=True),
        ),
    ]
