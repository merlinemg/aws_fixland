# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0043_partprice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='devicemodelsell',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=14),
        ),
    ]
