# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0044_auto_20160730_0549'),
    ]

    operations = [
        migrations.AlterField(
            model_name='devicemodelsell',
            name='price',
            field=models.DecimalField(decimal_places=2, editable=False, max_digits=14),
        ),
    ]
