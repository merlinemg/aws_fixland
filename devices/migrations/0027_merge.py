# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0026_vendorregion'),
        ('devices', '0026_vendor_is_import_new'),
    ]

    operations = [
    ]
