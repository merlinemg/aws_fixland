# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0014_masterstatus_color_hex'),
        ('devices', '0014_auto_20150706_1600'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceModelRepairCommission',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('commission', models.IntegerField(verbose_name='Комиссия')),
                ('percentage', models.BooleanField(verbose_name='Процент', default=False)),
                ('model_repair', models.ForeignKey(related_name='commissions', to='devices.DeviceModelRepair')),
                ('status', models.ForeignKey(verbose_name='Статус мастера', to='masters.MasterStatus')),
            ],
        ),
    ]
