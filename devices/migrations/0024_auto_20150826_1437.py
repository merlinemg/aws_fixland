# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0023_auto_20150817_1458'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='article',
            field=models.CharField(verbose_name='Артикул', null=True, blank=True, db_index=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='vendorpart',
            name='article',
            field=models.CharField(verbose_name='Артикул', null=True, blank=True, db_index=True, max_length=255),
        ),
    ]
