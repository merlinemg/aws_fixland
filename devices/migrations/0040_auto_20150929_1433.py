# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0039_auto_20150929_1359'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='devicemodelrepaircommission',
            options={'ordering': ('region',), 'verbose_name': 'Комиссия', 'verbose_name_plural': 'Комиссии'},
        ),
        migrations.AlterField(
            model_name='devicemodelrepaircommission',
            name='region',
            field=models.ForeignKey(verbose_name='Регион', to='accounts.Region'),
        ),
    ]
