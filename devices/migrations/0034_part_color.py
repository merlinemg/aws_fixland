# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0033_remove_part_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='color',
            field=models.ForeignKey(blank=True, to='devices.DeviceColor', verbose_name='Цвет запчасти', null=True, default=None),
        ),
    ]
