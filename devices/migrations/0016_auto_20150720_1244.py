# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0015_devicemodelrepaircommission'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='devicemodelrepaircommission',
            options={'verbose_name': 'Комиссия', 'verbose_name_plural': 'Комиссии'},
        ),
        migrations.AlterModelOptions(
            name='devicemodelrepairpart',
            options={'verbose_name': 'Запчасть', 'verbose_name_plural': 'Запчасти'},
        ),
        migrations.AlterField(
            model_name='devicemodelrepairpart',
            name='model_repair',
            field=models.ForeignKey(related_name='parts_quantity', to='devices.DeviceModelRepair'),
        ),
    ]
