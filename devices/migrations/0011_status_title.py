# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0010_auto_20150531_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='title',
            field=models.TextField(verbose_name='Заголовок', default=''),
            preserve_default=False,
        ),
    ]
