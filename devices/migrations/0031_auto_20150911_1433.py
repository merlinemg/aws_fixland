# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0030_vendorwarehouse_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendorwarehouse',
            name='lat',
            field=models.FloatField(blank=True, verbose_name='Долгота', null=True),
        ),
        migrations.AddField(
            model_name='vendorwarehouse',
            name='lon',
            field=models.FloatField(blank=True, verbose_name='Широта', null=True),
        ),
    ]
