# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0025_auto_20150826_1017'),
        ('devices', '0027_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='VendorWarehouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('code', models.CharField(verbose_name='Код склада', max_length=255)),
                ('name', models.CharField(verbose_name='Название', null=True, blank=True, max_length=255)),
                ('city', models.ForeignKey(verbose_name='Город', blank=True, to='accounts.City', null=True)),
                ('region', models.ForeignKey(verbose_name='Регион', blank=True, to='accounts.Region', null=True)),
                ('vendor', models.ForeignKey(verbose_name='Поставщик', blank=True, to='devices.Vendor', null=True, related_name='warehouses')),
            ],
            options={
                'verbose_name': 'Склад поставщика',
                'verbose_name_plural': 'Склады поставщиков',
            },
        ),
        migrations.AddField(
            model_name='vendorpart',
            name='warehouse',
            field=models.ForeignKey(verbose_name='Склад', blank=True, to='devices.VendorWarehouse', null=True),
        ),
    ]
