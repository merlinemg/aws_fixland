# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0020_vendor_vendorpart'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendorpart',
            name='exists',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='vendorpart',
            name='last_imported_at',
            field=models.DateTimeField(null=True),
        ),
    ]
