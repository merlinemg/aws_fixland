# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_colors(apps, schema_editor):
    Part = apps.get_model("devices", "Part")
    for part in Part.objects.all():
        if part.color:
            part.colors.add(part.color)


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0031_auto_20150911_1433'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='colors',
            field=models.ManyToManyField(blank=True, related_name='parts', to='devices.DeviceColor', verbose_name='Цвета запчасти'),
        ),
        migrations.RunPython(set_colors),
    ]
