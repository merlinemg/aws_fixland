# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0004_auto_20150501_1014'),
    ]

    operations = [
        migrations.AddField(
            model_name='repair',
            name='slug',
            field=models.SlugField(default='slug', help_text='То что будет отоброжаться в URL. Должно быть уникально.', unique=True, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='devicemodelrepair',
            name='device_model',
            field=models.ForeignKey(to='devices.DeviceModel', related_name='model_repairs'),
        ),
    ]
