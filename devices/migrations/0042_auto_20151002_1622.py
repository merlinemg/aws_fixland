# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0041_auto_20150929_2030'),
    ]

    operations = [
        migrations.AddField(
            model_name='devicemodelrepair',
            name='repairs',
            field=models.ManyToManyField(verbose_name='Поломки', related_name='model_repair_group', blank=True, to='devices.Repair'),
        ),
        migrations.AlterField(
            model_name='devicemodelrepair',
            name='repair',
            field=models.ForeignKey(to='devices.Repair', blank=True, verbose_name='Поломка', null=True),
        ),
        migrations.AlterField(
            model_name='devicemodelrepair',
            name='price',
            field=models.DecimalField(max_digits=14, editable=False, decimal_places=2, null=True),
        ),
    ]
