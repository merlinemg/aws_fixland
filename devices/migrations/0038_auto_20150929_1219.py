# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0030_auto_20150922_1805'),
        ('devices', '0037_auto_20150929_1202'),
    ]

    operations = [
        migrations.AddField(
            model_name='devicemodelrepaircommission',
            name='region',
            field=models.ForeignKey(verbose_name='Регион', null=True, to='accounts.Region'),
        ),
    ]
