# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0028_auto_20150908_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendor',
            name='integration_soap1s_api_key',
            field=models.CharField(null=True, blank=True, verbose_name='SOAP API KEY 1С интеграции', max_length=255),
        ),
        migrations.AddField(
            model_name='vendor',
            name='integration_soap1s_password',
            field=models.CharField(null=True, blank=True, verbose_name='SOAP password 1С интеграции', max_length=255),
        ),
        migrations.AddField(
            model_name='vendor',
            name='integration_soap1s_url',
            field=models.URLField(null=True, blank=True, verbose_name='SOAP URL 1С интеграции', max_length=255),
        ),
        migrations.AddField(
            model_name='vendor',
            name='integration_soap1s_username',
            field=models.CharField(null=True, blank=True, verbose_name='SOAP username 1С интеграции', max_length=255),
        ),
    ]
