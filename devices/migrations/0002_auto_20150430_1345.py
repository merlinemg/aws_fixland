# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='slug',
            field=models.SlugField(unique=True, help_text='То что будет отоброжаться в URL. Должно быть уникально.', max_length=255, default='slug'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='devicecolor',
            name='slug',
            field=models.SlugField(unique=True, help_text='То что будет отоброжаться в URL. Должно быть уникально.', max_length=255, default='slug'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='devicemodel',
            name='slug',
            field=models.SlugField(unique=True, help_text='То что будет отоброжаться в URL. Должно быть уникально.', max_length=255, default='slug'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='devicemodel',
            name='device',
            field=models.ForeignKey(to='devices.Device', related_name='models'),
        ),
    ]
