from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from partners.models import Partner


class PartnerSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False, read_only=True)
    balance = serializers.SerializerMethodField()

    class Meta:
        model = Partner
        fields = ('user', 'partner_code', 'balance')

    def get_balance(self, obj):
        return obj.get_balance()
