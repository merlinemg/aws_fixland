from rest_framework import viewsets
from rest_framework.response import Response

from partners.models import Partner
from .serializers import PartnerSerializer
from .permissions import PartnerPermission


class PartnerViewSet(viewsets.ModelViewSet):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer
    permission_classes = (PartnerPermission,)

    def retrieve(self, request, pk=None):
        """
        If provided 'pk' is "me" then return the current user.
        """
        if pk == 'me':
            return Response(PartnerSerializer(request.user.partner, context={
                'request': request
            }).data)
        return super(PartnerViewSet, self).retrieve(request, pk)
