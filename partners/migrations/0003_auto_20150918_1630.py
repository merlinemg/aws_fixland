# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import partners.utils


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0002_auto_20150918_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='partner_code',
            field=models.CharField(max_length=255, default=partners.utils.generate_partner_code),
        ),
    ]
