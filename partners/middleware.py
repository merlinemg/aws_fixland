from datetime import datetime

class PartnerMiddleware(object):
    def process_request(self, request):
        partner_code = request.GET.get('partner', None)
        if partner_code:
            request.session['partner_code'] = partner_code
            request.session['entry_by_partner_code_datetime'] = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")