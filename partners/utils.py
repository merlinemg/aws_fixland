import uuid
from datetime import datetime, timedelta
from constance import config


def generate_partner_code():
    from .models import Partner
    code = str(uuid.uuid4())[:8]
    if Partner.objects.filter(partner_code=code).exists():
        return generate_partner_code()
    return code


def return_partner_if_link_is_ok(request):
    from .models import Partner
    partner_code = request.session.get('partner_code', None)
    if partner_code:
        if request.session.get('entry_by_partner_code_datetime', None):
            entry_by_partner_code_datetime = datetime.strptime(request.session['entry_by_partner_code_datetime'], "%Y-%m-%d %H:%M:%S")
            expiration_datetime = entry_by_partner_code_datetime + timedelta(hours=config.LIFE_PERIOD_OF_PARTNER_LINK)
            if expiration_datetime >= datetime.utcnow():
                partner = Partner.objects.filter(partner_code=partner_code).first()
                if partner:
                    return partner
    return None

def clear_partner_session_data(request):
    request.session['partner_code'] = None
    request.session['entry_by_partner_code_datetime'] = None
