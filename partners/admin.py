from django.contrib import admin
from partners.models import Partner


class PartnerAdmin(admin.ModelAdmin):
    list_display = ('user', 'registration_comment',)
    model = Partner
    raw_id_fields = ('user',)
    autocomplete_lookup_fields = {
        'fk': ['user'],
    }


admin.site.register(Partner, PartnerAdmin)