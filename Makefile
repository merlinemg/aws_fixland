TESTS=

run:
	honcho start

celery:
	celery -A fixland -B -n fixland worker -l info

check_dependencies:
	pip list --outdated

lint_python:
	prospector

clean:
	find . -name "*.pyc" -exec rm -rf {} \;
	find . -name "__pycache__" -exec rm -rf {} \;

test:
	python manage.py test $(TESTS) --failfast --settings=fixland.test_settings --nomigrations

coverage:
	coverage run --source='.' manage.py test $(TESTS) --failfast --settings=fixland.test_settings --nomigrations
	coverage report

ansible_staging:
	ansible-playbook -i server/ansible/staging server/ansible/site.yml -u root --ask-vault

ansible_production:
	ansible-playbook -i server/ansible/production server/ansible/site.yml -u root --ask-vault

ansible_release:
	ansible-playbook -i server/ansible/release server/ansible/site.yml -u root --ask-vault

ansible_manicure:
	ansible-playbook -i server/ansible/manicure server/ansible/site.yml -u root --ask-vault

ansible_production_webapp:
	ansible-playbook -i server/ansible/production server/ansible/webapp.yml -u root --ask-vault

ansible_release_webapp:
	ansible-playbook -i server/ansible/release server/ansible/webapp.yml -u root --ask-vault

ansible_staging_webapp:
	ansible-playbook -i server/ansible/staging server/ansible/webapp.yml -u root --ask-vault

deploy_staging:
	npm install
	gulp build_all
	fab -R staging deploy
	python deploy_notify.py staging

deploy_production:
	# fab -R production backup_database
	npm install
	gulp build_all
	fab -R production deploy
	python deploy_notify.py production

deploy_release:
	npm install
	gulp build_all
	fab -R release deploy
	python deploy_notify.py release

makemessages:
	python manage.py makemessages --ignore=node_modules --ignore=*.js
	django-admin.py makemessages -d djangojs --ignore=node_modules --ignore=staticfiles

compilemessages:
	python manage.py compilemessages
	python manage.py compilejsi18n

sync_translations:
	fab -R staging download_translations
	fab -R production upload_translations
	rm locale.tar.gz
