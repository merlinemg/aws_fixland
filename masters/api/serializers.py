from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from devices.api.serializers import VendorWarehouseSerializer, PartSerializer
from ..models import CourseLessonEvent, CourseLesson, MasterCourses, Master, MasterPlace, PartOrderItem


class MasterSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    is_certificated_display = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    not_full_rating = serializers.SerializerMethodField()

    class Meta:
        model = Master
        fields = ('id', 'user', 'registration_comment', 'is_certificated', 'is_certificated_display', 'rating', 'status_name', 'status_color_hex', 'can_work', 'balance', 'currency', 'rejection_coef', 'not_full_rating')

    def get_is_certificated_display(self, obj):
        return obj.is_certificated_display

    def get_rating(self, obj):
        return obj.total_rating

    def get_not_full_rating(self, obj):
        return obj.rating


class LessonSerializer(serializers.ModelSerializer):

    class Meta:
        model = CourseLesson
        fields = ('number', 'name')


class CourseLessonEventSerializer(serializers.ModelSerializer):
    lesson = LessonSerializer(many=False, read_only=True)
    selected = serializers.SerializerMethodField()
    master_course = serializers.SerializerMethodField()

    class Meta:
        model = CourseLessonEvent
        fields = ('id', 'lesson', 'starttime', 'address', 'selected', 'master_course')

    def get_selected(self, obj):
        request = self.context.get('request')
        return request and obj.id in request.user.master.get_lesson_event_ids()

    def get_master_course(self, obj):
        request = self.context.get('request')
        master_course = request.user.master.get_master_course(obj.id)
        return master_course.id if master_course else None


class MasterCoursesSerializer(serializers.ModelSerializer):

    class Meta:
        model = MasterCourses
        fields = ('id', 'lesson_event', 'master')
        read_only_fields = ('master', )


class MasterCanWorkSerializer(serializers.ModelSerializer):

    class Meta:
        model = Master
        fields = ('id', 'can_work')


class MasterPartSerializer(serializers.Serializer):
    # id = serializers.IntegerField()
    part = serializers.IntegerField()
    part__name = serializers.CharField()
    quantity = serializers.IntegerField()


class MasterPlaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = MasterPlace
        fields = ('id', 'master', 'region', 'city', 'metro_stations', 'all_metro')
        read_only_fields = ('id', 'master', )


class PartOrderItemSerializer(serializers.ModelSerializer):
    warehouse = VendorWarehouseSerializer()
    part = serializers.SerializerMethodField()
    for_order_number = serializers.SerializerMethodField()

    class Meta:
        model = PartOrderItem
        fields = ('id', 'order_number', 'quantity', 'price', 'vendor_part', 'warehouse', 'part', 'created_at', 'for_order_number')

    def get_part(self, obj):
        part = obj.vendor_part.part
        return PartSerializer(part, many=False).data

    def get_for_order_number(self, obj):
        return obj.for_order.number if obj.for_order else None
