from django.utils import timezone
from django.db.models import Count
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.response import Response

from devices.models import Part
from ..models import CourseLessonEvent, MasterCourses, Master, MasterPart, MasterPlace, PartOrderItem
from .serializers import CourseLessonEventSerializer, MasterCoursesSerializer, MasterSerializer, MasterCanWorkSerializer, MasterPartSerializer, MasterPlaceSerializer, PartOrderItemSerializer
from .permissions import IsMaster, MasterPermission, ObjMasterPermissions


class MasterViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Master.objects
    serializer_class = MasterSerializer
    permission_classes = (MasterPermission,)

    def get_queryset(self, *args, **kwargs):
        return self.queryset.filter(user=self.request.user)

    def retrieve(self, request, pk=None):
        """
        If provided 'pk' is "me" then return the current user.
        """
        if pk == 'me':
            master = self.queryset.get(user=self.request.user)
            return Response(MasterSerializer(master, context={
                'request': request
            }).data)
        return super(MasterViewSet, self).retrieve(request, pk)


class CourseLessonEventView(viewsets.ReadOnlyModelViewSet):
    serializer_class = CourseLessonEventSerializer
    queryset = CourseLessonEvent.objects.all()
    permission_classes = (IsMaster,)

    def get_queryset(self, *args, **kwargs):
        master_completed_courses = self.request.user.master.get_completed_lesson_ids()
        return (
            self.queryset
            .select_related('lesson')
            .filter(
                starttime__gt=timezone.now(),
            )
            .exclude(
                lesson__id__in=master_completed_courses,
            )
        )


class MasterCoursesView(mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = MasterCoursesSerializer
    queryset = MasterCourses.objects.all()
    permission_classes = (ObjMasterPermissions,)

    def perform_create(self, serializer):
        return serializer.save(master=self.request.user.master)


class MasterCanWorkViewSet(mixins.UpdateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Master.objects.all()
    serializer_class = MasterCanWorkSerializer
    permission_classes = (MasterPermission,)


class MasterPartViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MasterPart.objects.all()
    serializer_class = MasterPartSerializer
    permission_classes = (IsMaster,)
    filter_fields = ('part', )

    def get_queryset(self):
        model = self.request.query_params.get('model', None)
        self.queryset = self.queryset.filter(master=self.request.user.master, status=MasterPart.STATUS_INSTOCK)
        if model:
            model_parts_list = Part.objects.filter(part_repairs__model_repair__device_model__slug=model).values_list('id', flat=True)
            self.queryset = self.queryset.filter(part__in=model_parts_list)
        return self.queryset.values('part', 'part__name').annotate(quantity=Count('part'))


class MasterPlaceViewSet(viewsets.ModelViewSet):
    queryset = MasterPlace.objects
    serializer_class = MasterPlaceSerializer
    permission_classes = (ObjMasterPermissions,)

    def get_queryset(self):
        return self.queryset.filter(master_id=self.request.user.master.id)

    def perform_create(self, serializer):
        serializer.save(master=self.request.user.master)


class PartOrderItemViewSet(viewsets.ModelViewSet):
    queryset = PartOrderItem.objects
    serializer_class = PartOrderItemSerializer
    permission_classes = (ObjMasterPermissions,)

    def get_queryset(self):
        for_order = self.request.GET.get('for_order')
        self.queryset = self.queryset.filter(warehouse__isnull=False, status=PartOrderItem.STATUS_CREATED, master=self.request.user.master)
        if for_order:
            return self.queryset.filter(for_order__number=for_order)
        return self.queryset
