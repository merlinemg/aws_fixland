''' Translations for devices models '''

from modeltranslation.translator import translator, TranslationOptions

from .models import Course, CourseLesson, CourseLessonEvent, MasterStatus


class CourseTranslationOptions(TranslationOptions):
    fields = ('name', )


class CourseLessonTranslationOptions(TranslationOptions):
    fields = ('name', )


class CourseLessonEventTranslationOptions(TranslationOptions):
    fields = ('address', )


class MasterStatusTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(Course, CourseTranslationOptions)
translator.register(CourseLesson, CourseLessonTranslationOptions)
translator.register(CourseLessonEvent, CourseLessonEventTranslationOptions)
translator.register(MasterStatus, MasterStatusTranslationOptions)
