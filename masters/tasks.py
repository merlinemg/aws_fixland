import logging

from fixland.celery import celery_app

from .models import Master, MasterRatingLog
from .rating import calculate_rating_for_master, get_masters_for_update_rating

log = logging.getLogger(__name__)


@celery_app.task
def update_rating_for_all_masters():
    for master in Master.objects.all():
        log.info('update_rating_for_all_masters, master: %s', master)
        calculate_rating_for_master(master)


@celery_app.task
def update_rating_for_master_who_requires_update():
    for master_id in get_masters_for_update_rating():
        log.info('update_rating_for_master_who_requires_update, master_id: %s', master_id)
        calculate_rating_for_master(Master.objects.get(id=master_id))


@celery_app.task
def update_master_rating_from_rating_logs():
    for master in Master.objects.all():
        total_rating = sum(MasterRatingLog.objects.filter(master=master, active=True).values_list('amount', flat=True))
        master.rating = total_rating
        master.save()
