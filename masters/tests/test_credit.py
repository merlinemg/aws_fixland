from decimal import Decimal
from django.conf import settings
from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.utils.timezone import now
from django.db.models.signals import post_save

from accounts.models import User
from devices import factories as devices_factories
from core import factories as core_factories
from core.models import Transaction
from core.w1 import get_walletone_answer, get_signature
from orders import factories as order_factories
from orders.models import RepairOrder
from ..factories import MasterFactory, create_master_statuses
from ..models import Master
from orders.models import register_master_to_update_rating_if_needed, order_post_save_signal_handler


def disconnect_signals():
    post_save.disconnect(register_master_to_update_rating_if_needed, sender=RepairOrder)
    post_save.disconnect(order_post_save_signal_handler, sender=RepairOrder)


class CreditTestCase(TestCase):
    def setUp(self):
        disconnect_signals()
        settings.CELERY_ALWAYS_EAGER = True
        master = MasterFactory()
        self.master = Master.objects.get(id=master.id)
        self.master.rating = 600
        self.master.save()
        self.user = self.master.user
        self.password = User.objects.make_random_password()
        self.user.set_password(self.password)
        self.user.save()
        self.order = order_factories.RepairOrderFactory()

        create_master_statuses(self.master)

        dmr1 = devices_factories.DeviceModelRepairFactory(
            device_model=self.order.device_model_color.device_model,
            price=Decimal(1001)
        )
        dmr2 = devices_factories.DeviceModelRepairFactory(
            device_model=self.order.device_model_color.device_model,
            price=Decimal(2002)
        )

        devices_factories.DeviceModelRepairCommissionFactory(
            model_repair=dmr1,
            status=self.master.get_status(),
            commission=100
        )
        devices_factories.DeviceModelRepairCommissionFactory(
            model_repair=dmr2,
            status=self.master.get_status(),
            commission=100
        )

        order_factories.RepairOrderItemFactory(repair_order=self.order, device_model_repair=dmr1)
        order_factories.RepairOrderItemFactory(repair_order=self.order, device_model_repair=dmr2)

        self.success_post_data = core_factories.W1_SUCCESS_POST_DATA.copy()
        self.success_post_data['SENDER_ID'] = str(self.user.id)
        self.success_post_data['WMI_PAYMENT_AMOUNT'] = '500'

        self.success_post_data['WMI_SIGNATURE'] = get_signature(self.success_post_data, settings.W1_SECRET_KEY).decode('utf8')
        self.success_response = get_walletone_answer('Ok', '{} - {} / {}'.format('Top ups', self.user.get_full_name(), self.user.id))

    def test_select_order_no_money_shound_take_credit(self):
        master_start_balance = 50
        core_factories.TransactionFactory(
            recipient=self.master.user,
            amount=master_start_balance,
            currency=self.master.currency,
            type=Transaction.REPAIR_ORDER_COMMISSION
        )
        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.get(reverse('select_repair_order', kwargs={'order_id': self.order.number}), follow=True)
        self.assertEqual(response.status_code, 200)
        master = Master.objects.get(id=self.master.id)
        self.assertEqual(master.used_credit_amount, Decimal(abs(master_start_balance - self.order.get_master_commission(master))))
        self.assertIsNotNone(master.take_credit_date)

    def test_select_order_no_money_no_credit_shound_return_403(self):
        core_factories.TransactionFactory(
            sender=self.master.user,
            amount=400,
            currency=self.master.currency,
            type=Transaction.REPAIR_ORDER_COMMISSION
        )
        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.get(reverse('select_repair_order', kwargs={'order_id': self.order.number}), follow=True)
        self.assertEqual(response.status_code, 403)

    def test_cancel_credit(self):
        core_factories.TransactionFactory(
            sender=self.master.user,
            amount=400,
            currency=self.master.currency,
            type=Transaction.REPAIR_ORDER_COMMISSION
        )
        self.master.take_credit_date = now()
        self.assertEqual(self.master.balance, Decimal('-400'))

        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.post(reverse('payment_notify'), self.success_post_data)
        self.assertEqual(response.content, self.success_response)

        master = Master.objects.get(id=self.master.id)
        self.assertEqual(master.used_credit_amount, Decimal(0))
        self.assertEqual(master.balance, Decimal(100))
        self.assertIsNone(master.take_credit_date)

    def tearDown(self):
        post_save.connect(register_master_to_update_rating_if_needed, sender=RepairOrder)
        post_save.connect(order_post_save_signal_handler, sender=RepairOrder)
