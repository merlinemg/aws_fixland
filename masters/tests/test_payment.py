from decimal import Decimal
from django.conf import settings
from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from accounts.models import User
from core.models import Transaction
from core.w1 import get_walletone_answer, get_signature
from core import factories as core_factories
from ..models import Master


class MasterPaymentTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username='username',
            first_name='first_name',
            last_name='last_name',
        )
        self.password = User.objects.make_random_password()
        self.user.set_password(self.password)
        self.master = Master.objects.create(user=self.user)
        self.success_post_data = core_factories.W1_SUCCESS_POST_DATA.copy()
        self.success_post_data['SENDER_ID'] = str(self.user.id)
        self.success_post_data['WMI_PAYMENT_AMOUNT'] = '0.01'

        self.bad_signature = 'badbad'
        self.success_post_data_bad_signature = self.success_post_data.copy()
        self.success_post_data_bad_signature['WMI_SIGNATURE'] = self.bad_signature
        self.success_post_data_bad_signature['WMI_PAYMENT_AMOUNT'] = '0.01'

        self.bad_order_state = 'bad_order_state'
        self.post_data_bad_signature_order_state = self.success_post_data.copy()
        self.post_data_bad_signature_order_state['WMI_ORDER_STATE'] = self.bad_order_state
        self.post_data_bad_signature_order_state['WMI_SIGNATURE'] = get_signature(self.post_data_bad_signature_order_state, settings.W1_SECRET_KEY).decode('utf8')

        self.success_post_data['WMI_SIGNATURE'] = get_signature(self.success_post_data, settings.W1_SECRET_KEY).decode('utf8')

        self.bad_signature_response = get_walletone_answer('Retry', 'Неверная подпись WMI_SIGNATURE')
        self.bad_state_response = get_walletone_answer('Retry', 'Неверное состояние {}'.format(self.bad_order_state))
        self.success_response = get_walletone_answer('Ok', '{} - {} / {}'.format('Top ups', self.user.get_full_name(), self.user.id))

    def test_payment_bad_signature(self):
        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.post(reverse('payment_notify'), self.success_post_data_bad_signature)
        self.assertEqual(response.content, self.bad_signature_response)

    def test_payment_bad_order_state(self):
        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.post(reverse('payment_notify'), self.post_data_bad_signature_order_state)
        self.assertEqual(response.content, self.bad_state_response)

    def test_payment_success_post_data(self):
        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.post(reverse('payment_notify'), self.success_post_data)
        self.assertEqual(response.content, self.success_response)
        transaction = Transaction.objects.filter(wmi_order_id='344688379096').first()
        self.assertNotEqual(transaction, None)
        self.assertEqual(transaction.amount, Decimal('0.01'))
        master = Master.objects.get(id=self.master.id)
        self.assertEqual(master.balance, Decimal('0.01'))
