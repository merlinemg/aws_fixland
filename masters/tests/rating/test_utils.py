from datetime import timedelta
from decimal import Decimal

from django.test import TestCase
from django.utils import timezone

from masters.rating import calculate_rating_for_master, calculate_rejection_coef_for_master
from masters.factories import MasterFactory, MasterCoursesFactory, MasterReviewFactory, MasterRatingLogFactory
from masters.models import MasterRatingLog, MasterReview
from orders.factories import RepairOrderFactory, RepairOrderItemFactory, RejectedOrderFactory
from orders.models import RepairOrder


class CalculateRatingForMasterTestCase(TestCase):
    def setUp(self):
        self.master = MasterFactory()

    def test_should_return_0_if_no_actions(self):
        self.assertEqual(calculate_rating_for_master(self.master), 0)

    def test_should_calculate_finished_courses(self):
        course = MasterCoursesFactory(master=self.master, score=90, completed=True)
        self.assertEqual(calculate_rating_for_master(self.master), 90)
        log = MasterRatingLog.objects.filter(master=self.master).first()
        self.assertEqual(log.rating_type, MasterRatingLog.RATING_COURSES)
        self.assertEqual(log.amount, 90)
        self.assertEqual(log.master_course_id, course.id)

        # should calculate only one time
        self.assertEqual(calculate_rating_for_master(self.master), 0)

    def test_should_calculate_rating_for_repairs(self):
        order = RepairOrderFactory(master=self.master, status=RepairOrder.STATUS_DONE)
        repair_order = RepairOrderItemFactory(repair_order=order, device_model_repair__device_model__slug='iphone4s')
        # repair_order.device_model_repair.device_model.save()
        self.assertEqual(calculate_rating_for_master(self.master), 120)
        log = MasterRatingLog.objects.filter(master=self.master).first()
        self.assertEqual(log.rating_type, MasterRatingLog.RATING_REPAIRS)
        self.assertEqual(log.amount, 120)
        self.assertEqual(log.repair_order_item_id, repair_order.id)

        # should calculate only one time
        self.assertEqual(calculate_rating_for_master(self.master), 0)

    def test_should_calculate_rating_for_reviews(self):
        review = MasterReviewFactory(master=self.master, score=MasterReview.FIVE_STAR)
        self.assertEqual(calculate_rating_for_master(self.master), 50)
        log = MasterRatingLog.objects.filter(master=self.master).first()
        self.assertEqual(log.rating_type, MasterRatingLog.RATING_REVIEWS)
        self.assertEqual(log.amount, 50)
        self.assertEqual(log.master_review_id, review.id)

        # should calculate only one time
        self.assertEqual(calculate_rating_for_master(self.master), 0)

    def test_should_reduce_outdated_logs(self):
        log = MasterRatingLogFactory(master=self.master, amount=50, rating_type=MasterRatingLog.RATING_REPAIRS, active=True)
        log.created_at = timezone.now() - timedelta(days=100)
        log.save()
        log = MasterRatingLogFactory(master=self.master, amount=60, rating_type=MasterRatingLog.RATING_REVIEWS, created_at=timezone.now() - timedelta(days=100), active=True)
        log.created_at = timezone.now() - timedelta(days=100)
        log.save()

        self.assertEqual(calculate_rating_for_master(self.master), -110)
        self.assertEqual(calculate_rating_for_master(self.master), 0)


class CalculateRejectionCoefForMaster(TestCase):
    def setUp(self):
        self.master = MasterFactory()

    def test_should_return_one_if_no_activity(self):
        self.assertEqual(calculate_rejection_coef_for_master(self.master), Decimal(1))

    def test_should_returf_coef(self):
        for _ in range(10):
            RepairOrderFactory(master=self.master, status=RepairOrder.STATUS_DONE)
        RejectedOrderFactory(master=self.master)

        self.assertEqual(calculate_rejection_coef_for_master(self.master), Decimal('0.95'))
