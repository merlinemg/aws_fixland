# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0017_auto_20150806_1150'),
        ('masters', '0017_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterPlace',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('city', models.ForeignKey(null=True, to='accounts.City')),
                ('master', models.ForeignKey(to='masters.Master')),
                ('metro_stations', models.ManyToManyField(to='accounts.MetroStation', blank=True)),
                ('region', models.ForeignKey(null=True, to='accounts.Region')),
            ],
        ),
    ]
