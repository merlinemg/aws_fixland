# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0016_master_certified_models'),
        ('masters', '0016_auto_20150717_1640'),
    ]

    operations = [
    ]
