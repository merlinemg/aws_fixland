# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0020_auto_20150807_1145'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='masterreview',
            options={'verbose_name': 'Оценка мастера', 'verbose_name_plural': 'Оценки мастера'},
        ),
        migrations.AlterField(
            model_name='master',
            name='rejection_coef',
            field=models.DecimalField(verbose_name='Коэф отказов', max_digits=3, default=Decimal('1'), decimal_places=2),
        ),
    ]
