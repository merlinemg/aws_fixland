# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0032_auto_20150909_0911'),
        ('masters', '0026_partorderitem_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='partorderitem',
            name='cancel_comment',
            field=models.CharField(null=True, max_length=255, verbose_name='Комментарий отказа', blank=True),
        ),
        migrations.AddField(
            model_name='partorderitem',
            name='for_order',
            field=models.ForeignKey(to='orders.RepairOrder', verbose_name='Для заказа', related_name='parts_orders', null=True, blank=True),
        ),
    ]
