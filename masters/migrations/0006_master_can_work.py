# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0005_auto_20150611_1358'),
    ]

    operations = [
        migrations.AddField(
            model_name='master',
            name='can_work',
            field=models.BooleanField(default=False, verbose_name='Может пренимать заказы'),
        ),
    ]
