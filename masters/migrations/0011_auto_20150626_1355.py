# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0010_mastercourses_score'),
    ]

    operations = [
        migrations.AlterField(
            model_name='master',
            name='registration_comment',
            field=models.TextField(help_text='Комментарий который был введен при регистрации', verbose_name='Комментарий', null=True, blank=True),
        ),
    ]
