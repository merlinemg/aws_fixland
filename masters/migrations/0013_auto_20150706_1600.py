# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0014_auto_20150706_1600'),
        ('masters', '0012_auto_20150702_1414'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(verbose_name='Количество', default=0)),
                ('master', models.ForeignKey(to='masters.Master')),
                ('part', models.ForeignKey(to='devices.Part', verbose_name='Запчасть')),
            ],
            options={
                'verbose_name': 'Запчасть мастера',
                'verbose_name_plural': 'Запчасти мастера',
            },
        ),
        migrations.AddField(
            model_name='master',
            name='parts',
            field=models.ManyToManyField(verbose_name='Запчасти', to='devices.Part', through='masters.MasterPart'),
        ),
    ]
