# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0002_add_courses'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourseLesson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('number', models.IntegerField(verbose_name='Последовательный номер')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Урок',
                'verbose_name_plural': 'Уроки',
            },
        ),
        migrations.CreateModel(
            name='CourseLessonEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('starttime', models.DateTimeField()),
                ('address', models.CharField(max_length=255)),
                ('lesson', models.ForeignKey(related_name='events', to='masters.CourseLesson', verbose_name='Урок')),
            ],
            options={
                'verbose_name': 'Занятия',
            },
        ),
        migrations.AlterModelOptions(
            name='mastercourses',
            options={'verbose_name': 'Занятие мастера', 'verbose_name_plural': 'Занятия мастера'},
        ),
        migrations.RemoveField(
            model_name='course',
            name='address',
        ),
        migrations.RemoveField(
            model_name='course',
            name='closed',
        ),
        migrations.RemoveField(
            model_name='course',
            name='date',
        ),
        migrations.RemoveField(
            model_name='master',
            name='courses',
        ),
        migrations.RemoveField(
            model_name='mastercourses',
            name='course',
        ),
        migrations.AddField(
            model_name='courselesson',
            name='course',
            field=models.ForeignKey(related_name='exercises', to='masters.Course', verbose_name='Курс'),
        ),
        migrations.AddField(
            model_name='master',
            name='lessons',
            field=models.ManyToManyField(to='masters.CourseLesson', through='masters.MasterCourses'),
        ),
        migrations.AddField(
            model_name='mastercourses',
            name='lesson',
            field=models.ForeignKey(related_name='masters_courses', to='masters.CourseLesson', default=1),
            preserve_default=False,
        ),
    ]
