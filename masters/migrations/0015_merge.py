# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0014_master_new_repair_order_notification'),
        ('masters', '0014_masterstatus_color_hex'),
    ]

    operations = [
    ]
