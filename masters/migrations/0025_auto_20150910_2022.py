# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0029_auto_20150910_1224'),
        ('masters', '0024_auto_20150901_1639'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partorder',
            name='master',
        ),
        migrations.RemoveField(
            model_name='partorder',
            name='parts',
        ),
        migrations.RemoveField(
            model_name='partorderitem',
            name='part_order',
        ),
        migrations.AddField(
            model_name='partorderitem',
            name='created_at',
            field=models.DateTimeField(verbose_name='Cоздан', default=datetime.datetime(2015, 9, 10, 17, 22, 36, 813152, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='partorderitem',
            name='master',
            field=models.ForeignKey(null=True, verbose_name='Мастер', to='masters.Master'),
        ),
        migrations.AddField(
            model_name='partorderitem',
            name='order_number',
            field=models.CharField(null=True, max_length=255, verbose_name='Код заказа'),
        ),
        migrations.AddField(
            model_name='partorderitem',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Обновлен', default=datetime.datetime(2015, 9, 10, 17, 22, 42, 724529, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='partorderitem',
            name='warehouse',
            field=models.ForeignKey(null=True, verbose_name='Склад', to='devices.VendorWarehouse'),
        ),
        migrations.DeleteModel(
            name='PartOrder',
        ),
    ]
