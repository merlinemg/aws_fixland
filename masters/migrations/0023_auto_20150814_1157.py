# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0022_auto_20150812_1511'),
    ]

    operations = [
        migrations.AddField(
            model_name='master',
            name='take_credit_date',
            field=models.DateTimeField(null=True, verbose_name='Дата взятия кредита', blank=True),
        ),
        migrations.AddField(
            model_name='masterstatus',
            name='credit_amount',
            field=models.IntegerField(verbose_name='Кредит доверия', default=0),
        ),
        migrations.AlterField(
            model_name='masterreview',
            name='phone_number',
            field=models.CharField(max_length=255, blank=True, help_text='Для отправки бонуса', verbose_name='Номер телефона', null=True),
        ),
    ]
