# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0011_auto_20150626_1355'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='name_en',
            field=models.CharField(null=True, help_text='Название курса', max_length=255, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='course',
            name='name_ru',
            field=models.CharField(null=True, help_text='Название курса', max_length=255, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='courselesson',
            name='name_en',
            field=models.CharField(null=True, max_length=255, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='courselesson',
            name='name_ru',
            field=models.CharField(null=True, max_length=255, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='courselessonevent',
            name='address_en',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='courselessonevent',
            name='address_ru',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='masterstatus',
            name='name_en',
            field=models.CharField(null=True, max_length=255, verbose_name='Наименование'),
        ),
        migrations.AddField(
            model_name='masterstatus',
            name='name_ru',
            field=models.CharField(null=True, max_length=255, verbose_name='Наименование'),
        ),
    ]
