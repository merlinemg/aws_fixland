# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from datetime import timedelta


def remove_master_rating_log_dublicates(apps, schema_editor):
    MasterRatingLog = apps.get_model("masters", "MasterRatingLog")
    logs = MasterRatingLog.objects.all()
    for log in logs:
        if MasterRatingLog.objects.filter(
            master=log.master,
            rating_type=log.rating_type,
            amount=log.amount,
            repair_order_item=log.repair_order_item,
            master_review=log.master_review,
            created_at__lte=log.created_at+timedelta(seconds=30),
            created_at__gte=log.created_at-timedelta(seconds=30)
        ).count() > 1:
            log.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0031_auto_20151013_1024'),
    ]

    operations = [
        migrations.RunPython(remove_master_rating_log_dublicates)
    ]
