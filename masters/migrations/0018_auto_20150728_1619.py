# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0020_vendor_vendorpart'),
        ('masters', '0017_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartOrder',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Cоздан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Обновлен')),
                ('master', models.ForeignKey(to='masters.Master', related_name='part_orders', verbose_name='Мастер')),
            ],
            options={
                'verbose_name_plural': 'Заказы запчастей',
                'verbose_name': 'Заказ запчастей',
            },
        ),
        migrations.CreateModel(
            name='PartOrderItem',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('quantity', models.IntegerField(verbose_name='Количество')),
                ('price', models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена')),
                ('part_order', models.ForeignKey(to='masters.PartOrder', related_name='items')),
                ('vendor_part', models.ForeignKey(to='devices.VendorPart', verbose_name='Запчасть')),
            ],
            options={
                'verbose_name_plural': 'Элементы заказа',
                'verbose_name': 'Элемент заказа',
            },
        ),
        migrations.AddField(
            model_name='partorder',
            name='parts',
            field=models.ManyToManyField(to='devices.VendorPart', through='masters.PartOrderItem'),
        ),
    ]
