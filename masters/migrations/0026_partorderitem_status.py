# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0025_auto_20150910_2022'),
    ]

    operations = [
        migrations.AddField(
            model_name='partorderitem',
            name='status',
            field=models.CharField(max_length=255, default='created', choices=[('created', 'created'), ('cancelled', 'cancelled'), ('received', 'received')]),
        ),
    ]
