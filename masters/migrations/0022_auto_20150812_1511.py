# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('masters', '0021_auto_20150807_1341'),
    ]

    operations = [
        migrations.AddField(
            model_name='masterreview',
            name='as_in_order',
            field=models.BooleanField(default=True, help_text='Для отправки бонуса', verbose_name='Тот что в заказе'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='masterreview',
            name='comment',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Комментарий'),
        ),
        migrations.AddField(
            model_name='masterreview',
            name='email',
            field=models.EmailField(blank=True, help_text='Для отправки бонуса', max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='masterreview',
            name='phone_number',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Номер телефона'),
        ),
        migrations.AddField(
            model_name='masterreview',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='заказчик', related_name='reviews', default='', null=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='masterratinglog',
            name='master_course',
            field=models.ForeignKey(null=True, blank=True, to='masters.MasterCourses', verbose_name='Курс Мастера'),
        ),
        migrations.AlterField(
            model_name='masterratinglog',
            name='master_review',
            field=models.ForeignKey(null=True, blank=True, to='masters.MasterReview', verbose_name='Оценка'),
        ),
        migrations.AlterField(
            model_name='masterratinglog',
            name='rating_type',
            field=models.CharField(max_length=255, choices=[('courses', 'Рейтинг за курсы'), ('repairs', 'Рейтинг за отремонтированные поломки'), ('reviews', 'Рейтинг за оценки'), ('bonus', 'Бонус Рейтинг')], verbose_name='Тип рейтинга'),
        ),
        migrations.AlterField(
            model_name='masterratinglog',
            name='repair_order_item',
            field=models.ForeignKey(null=True, blank=True, to='orders.RepairOrderItem', verbose_name='Отремонтированная поломка'),
        ),
    ]
