from functools import wraps

from django.core.exceptions import PermissionDenied


def master_required(func):
    @wraps(func)
    def _wrapped_view(request, *args, **kwargs):
        if not request.user.is_master():
            raise PermissionDenied()
        return func(request, *args, **kwargs)
    return _wrapped_view
