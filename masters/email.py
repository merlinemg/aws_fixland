from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.conf import settings
from django.core.urlresolvers import reverse

from masters.models import MasterStatus
import logging

log = logging.getLogger(__name__)


def get_context_for_status_changed_notification(master, old_status):
    new_status = master.get_status()
    rating=0
    if old_status:
        rating = old_status.rating
    else:
        rating = new_status.rating
    status = new_status.name
    credit_limit = new_status.credit_amount
    context = {
        'name': '{} {}'.format(master.user.first_name, master.user.last_name),
        'rating': '{},'.format(rating),
        'status': status,
        'credit_limit': credit_limit,
        'your_crew_link': 'http://{}'.format(
            settings.HOST_URL),
        'cabinet_link': 'http://{}{}'.format(
            settings.HOST_URL,
            reverse('master_index', ))
    }
    return context


def send_improvement_status_email(master):
    context = get_context_for_status_changed_notification(master=master, old_status=None)
    email = master.user.email
    subject = _('Fixland.ru - Ваш рейтинг увеличился')
    message = render_to_string('emails/masters/improvement_status_email.html', context)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], html_message=message)


def send_deterioration_status_email(master, old_status):
    context = get_context_for_status_changed_notification(master=master, old_status=old_status)
    email = master.user.email
    subject = _('Fixland.ru - Ваш рейтинг уменьшился')
    message = render_to_string('emails/masters/deterioration_status_email.html', context)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], html_message=message)
