import logging
from easy_pdf.rendering import render_to_pdf_response
from decimal import Decimal
from constance import config
from datetime import timedelta
from django.conf import settings
from django.utils.timezone import now as utc_now
from django.utils.dates import MONTHS
from django.utils.translation import ugettext as _

from integrations.soap1s import bl as soap1s
from orders.models import Coupon, RepairOrder
from devices.models import VendorPart, VendorWarehouse, Vendor
from devices.vendors.bl import order_parts, save_parts
from .models import Course, MasterCourses, Master, PartOrderItem, MasterPart, MasterReview
from orders import tasks as order_tasks
from .utils import get_and_increment_agent_report_number
from orders.email import new_master_review_admin

log = logging.getLogger(__name__)


def get_courses(user):
    master = Master.objects.filter(user=user)
    master_courses = MasterCourses.objects.values_list('course', flat=True).filter(master=master)
    courses = list(Course.objects.all())
    res_courses = []
    for course in courses:
        try:
            completed = MasterCourses.objects.get(course=course, master=master).completed
        except Exception:
            completed = False
        res_courses.append({
            'id': course.id,
            'date': course.date.strftime("%d-%m-%Y"),
            'time': course.date.strftime("%H:%M"),
            'name': course.name,
            'address': course.address,
            'subscribed': 'true' if (course.id in master_courses) else 'false',
            'closed': 'true' if (course.closed) else 'false',
            'completed': 'true' if (completed) else 'false'
        })
    return res_courses


def add_master_parts(data, master):
    for_order = RepairOrder.objects.filter(number=data.get('for_order')).first() if data.get('for_order') else None
    parts_qty = {part['part']: part['qty'] for part in data.get('parts')}
    vendor_parts = (
        VendorPart.objects
        .select_related('warehouse', 'vendor')
        .filter(id__in=parts_qty.keys())
        .all()
    )
    if not vendor_parts:
        return None

    order = [
        {
            'part': part,
            'qty': parts_qty[part.id]
        }
        for part in vendor_parts
    ]
    result_list = order_parts(order)
    if result_list is None:
        return {
            'errors': _('Не удалось заказать детали'),
            'success': False,
        }

    parts_by_article = {
        part.article: part for part in vendor_parts
    }
    success = False
    succesfull_parts = []
    unsuccesfull_parts = []
    for result in result_list:
        for order in result['orders']:
            success = True
            warehouse = VendorWarehouse.objects.get(code=order['warehouse'])
            order_item = PartOrderItem.objects.create(
                master=master,
                vendor_part=parts_by_article[order['article']],
                order_number=order['order_number'],
                warehouse=warehouse,
                quantity=int(order['qty']),
                price=safe_price_to_decimal(order['price']),
                for_order=for_order
            )
            succesfull_parts.append({
                'order_item_id': order_item.id,
                'article': order['article'],
                'qty': order['qty'],
                'warehouse': order['warehouse'],
            })
        if result['parts']:
            vendor = Vendor.objects.get(id=int(result['vendor_id']))
            save_parts(result, vendor, vendor_parts)
            articles = []
            for part in result['parts']:
                if part['article'] not in articles:
                    articles.append(part['article'])
                    unsuccesfull_parts.append({
                        'warehouse': part['warehouse'],
                        'article': part['article'],
                        'qty': part['qty'],
                    })

    return {
        'success': success,
        'succesfull_parts': succesfull_parts,
        'unsuccesfull_parts': unsuccesfull_parts,
    }


def safe_price_to_decimal(price):
    price = price.replace(',', '.')
    price = price.replace(' ', '')
    price = price.replace('\xa0', '')
    return Decimal(price)


def create_master_review(data):
    review_invite = data.get('review_invite')
    master_review = MasterReview.objects.create(
        master=review_invite.repair_order.master,
        score=data.get('score'),
        repair_order=review_invite.repair_order,
        user=review_invite.user,
        comment=data.get('comment'),
        as_in_order=data.get('as_in_order'),
        email=data.get('email'),
        phone_number=data.get('phone')
    )
    if master_review.score < 5:
        new_master_review_admin(review=master_review)
    review_invite.accept()
    if config.ORDER_REVIEW_BONUS_TYPE == settings.ORDER_REVIEW_BONUS_COUPON_KEY:
        coupon = Coupon.objects.create(
            discount=config.COUPON_DISCOUNT,
            expiration_date=utc_now() + timedelta(days=365)
        )
        email = review_invite.repair_order.user.email if data.get('as_in_order') else data.get('email')
        order_tasks.send_coupon.delay(review_invite.user.id, email, coupon.id, review_invite.repair_order.id)


def get_agent_report_pdf_response(request, master, year, month):
    orders = RepairOrder.objects.filter(
        master=master,
        status=RepairOrder.STATUS_DONE,
        created_at__year=year,
        created_at__month=month,
    )
    agent_amount = Decimal(0)
    orders_amount = Decimal(0)
    for order in orders:
        agent_amount += order.get_commission()
        orders_amount += order.total_amount

    number = get_and_increment_agent_report_number()
    context = {
        'pagesize': "A4",
        'title': "Pdf agent report",
        'report_number': '{}{}{}'.format(month, year[2:], int(number)),
        'master': master,
        'year': year,
        'month': month,
        'month_name': MONTHS.get(int(month)),
        'orders': orders,
        'agent_amount': agent_amount,
        'orders_amount': orders_amount,
        'signature': settings.STATIC_FULL_PATH + '/images/signature.png',
    }
    return render_to_pdf_response(request, 'masters/agent_report_pdf.html', context)


def group_by_vendors(parts_orders):
    by_vendors = {}
    for part_order in parts_orders:
        vendor_part = part_order.vendor_part
        if vendor_part.vendor_id not in by_vendors:
            by_vendors[vendor_part.vendor_id] = []
        by_vendors[vendor_part.vendor_id].append(part_order)
    return by_vendors


def cancel_parts_orders(parts_orders, master=None, numbers=False):
    if master and numbers:
        parts_orders = PartOrderItem.objects.filter(
            order_number__in=parts_orders,
            master=master,
            status=PartOrderItem.STATUS_CREATED
        )
    by_orders_number = {i.order_number: i for i in parts_orders}
    for orders in group_by_vendors(by_orders_number.values()).values():
        vendor = orders[0].vendor_part.vendor
        if not vendor.is_soap_integrated():
            log.error('Soap not integrated for vendor: %s', vendor)
        order_numbers = [i.order_number for i in orders]
        result = soap1s.cancel_order(
            vendor.integration_soap1s_url,
            vendor.integration_soap1s_username,
            vendor.integration_soap1s_password,
            vendor.integration_soap1s_api_key,
            order_numbers,
        )
        if result['success']:
            comment = result['orders'][0]['comment']
            PartOrderItem.objects.filter(
                order_number__in=order_numbers
            ).update(
                status=PartOrderItem.STATUS_CANCELLED,
                cancel_comment=comment
            )


def cancel_parts_orders_items(order_ids, master):
    parts_orders = PartOrderItem.objects.filter(id__in=order_ids, master=master)
    by_orders_number = {i.order_number: i for i in parts_orders}
    for orders in group_by_vendors(by_orders_number.values()).values():
        vendor = orders[0].vendor_part.vendor
        if not vendor.is_soap_integrated():
            log.error('Soap not integrated for vendor: %s', vendor)
        orders_list = []
        for item in orders:
            orders_list.append({
                'order_number': item.order_number,
                'article': item.vendor_part.article
            })
        result = soap1s.cancel_order_item(
            vendor.integration_soap1s_url,
            vendor.integration_soap1s_username,
            vendor.integration_soap1s_password,
            vendor.integration_soap1s_api_key,
            orders_list,
        )
        if result['success']:
            comment = result['orders'][0]['comment']
            success_orders = result['orders']
            for order in success_orders:
                PartOrderItem.objects.filter(
                    order_number=order.get('order_number'),
                    vendor_part__article=order.get('article')
                ).update(
                    status=PartOrderItem.STATUS_CANCELLED,
                    cancel_comment=comment
                )


def is_master_have_pars_for_order(master, repair_order, return_need_update=False):
    if not config.SHOW_USER_IF_MASTER_HAVE_PARTS_FOR_ORDER:
        return True if not return_need_update else False
    result = False
    order_parts = repair_order.get_order_parts()
    master_parts_list = master.get_instock_parts().values_list('part_id', flat=True)
    master_reserved_parts_list = (
        master
        .get_reserved_parts()
        .values_list('vendor_part__part_id', flat=True)
    )
    to_check_vendor_availability = []
    for order_part in order_parts:
        if order_part.part_id not in master_parts_list and order_part.part_id not in master_reserved_parts_list:
            to_check_vendor_availability.append(order_part.part_id)
    if return_need_update:
        return True if to_check_vendor_availability else False
    if not to_check_vendor_availability:
        result = True
    else:
        vendor_parts = VendorPart.objects.filter(part_id__in=to_check_vendor_availability, exists=True).count()
        result = False if vendor_parts else True
    return result


def charge_off_master_parts_for_order(master, repair_order):
    order_parts = repair_order.get_order_parts()
    order_parts_list = [i.part_id for i in order_parts]
    master_parts_for_order = MasterPart.objects.filter(
        part__in=order_parts_list,
        master=master,
        status=MasterPart.STATUS_INSTOCK
    ).distinct('part_id')
    MasterPart.objects.filter(id__in=[i.id for i in master_parts_for_order]).update(status=MasterPart.STATUS_CHARGE_OFF)
    return [i.part_id for i in master_parts_for_order]
