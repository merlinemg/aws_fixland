from django_redis import get_redis_connection

KEY = 'masters-to-update-rating'


def register_master_to_update_rating(master_id):
    con = get_redis_connection('default')
    con.sadd(KEY, master_id)


def get_masters_for_update_rating():
    con = get_redis_connection('default')
    while True:
        master_id = con.spop(KEY)
        if master_id:
            yield int(master_id)
        else:
            return
