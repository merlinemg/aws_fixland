from datetime import timedelta
from functools import reduce
from itertools import chain
import logging

from django.utils import timezone
from django.db.models import Q

from constance import config
from masters.email import send_deterioration_status_email, send_improvement_status_email
from orders.models import RepairOrder, RepairOrderItem, RejectedOrder
from masters.models import MasterRatingLog, MasterCourses, MasterReview, Master

log = logging.getLogger(__name__)


def calculate_rating_for_master(master):
    rating = master.rating
    old_status = master.get_status()
    for func in rating_functions:
        rating += func(master)
    Master.objects.filter(id=master.id).update(rating=rating)
    calculate_rejection_coef_for_master(master)
    updated_master = Master.objects.filter(id=master.id).first()
    new_status = updated_master.get_status()
    if new_status != old_status:
        log.warning('Status of a master is changed, old: %s, new: %s', old_status, new_status, extra={'master': updated_master})
        if new_status.rating > old_status.rating:
            send_improvement_status_email(updated_master)
        elif new_status.rating < old_status.rating:
            send_deterioration_status_email(updated_master, old_status=old_status)
    return rating


def calculate_rejection_coef_for_master(master):
    days_ago = timezone.now() - timedelta(days=config.REJECTED_COEF_DURATION)
    done_orders_count = (
        RepairOrder.objects.filter(
            master=master,
            created_at__gte=days_ago,
            status=RepairOrder.STATUS_DONE,
        )
        .count()
    )
    rejected_orders_count = (
        RejectedOrder.objects.filter(
            master=master,
            created_at__gte=days_ago,
        )
        .count()
    )
    if done_orders_count == 0 and rejected_orders_count != 0:
        rejection_percent = 100
    elif done_orders_count == 0 and rejected_orders_count == 0:
        rejection_percent = 0
    else:
        rejection_percent = round((rejected_orders_count / done_orders_count) * 100)
    if rejection_percent <= config.REJECTION_MAX_PERCENTAGE_LIMITS_1:
        coef = config.REJECTION_COEF_LIMITS_1
    elif config.REJECTION_MAX_PERCENTAGE_LIMITS_1 < rejection_percent <= config.REJECTION_MAX_PERCENTAGE_LIMITS_2:
        coef = config.REJECTION_COEF_LIMITS_2
    elif config.REJECTION_MAX_PERCENTAGE_LIMITS_2 < rejection_percent <= config.REJECTION_MAX_PERCENTAGE_LIMITS_3:
        coef = config.REJECTION_COEF_LIMITS_3
    elif config.REJECTION_MAX_PERCENTAGE_LIMITS_3 < rejection_percent <= config.REJECTION_MAX_PERCENTAGE_LIMITS_4:
        coef = config.REJECTION_COEF_LIMITS_4
    else:
        coef = config.REJECTION_COEF_LIMITS_5
    Master.objects.filter(id=master.id).update(rejection_coef=coef)
    return coef


def add_rating_for_reviews(master):
    saved_review_ids = (
        MasterRatingLog.query_reviews().filter(master=master).values_list('master_review_id', flat=True).all()
    )
    new_reviews = (
        MasterReview.objects.filter(
            master=master,
        )
        .exclude(
            Q(id__in=saved_review_ids) | Q(repair_order__status=RepairOrder.STATUS_VOID),
        )
        .values('id', 'score', 'master_id')
    )
    return reduce(save_review_rating, new_reviews, 0)


def save_review_rating(acc, review):
    score = get_score_for_stars(review['score'])
    _, created = MasterRatingLog.objects.get_or_create(
        master_id=review['master_id'],
        rating_type=MasterRatingLog.RATING_REVIEWS,
        master_review_id=review['id'],
        defaults=dict(amount=score)
    )
    return acc + (score if created else 0)


def get_score_for_stars(stars):
    score = {
        MasterReview.FIVE_STAR: config.REVIEW_RATING_FIVE_STAR,
        MasterReview.FOUR_STAR: config.REVIEW_RATING_FOUR_STAR,
        MasterReview.THREE_STAR: config.REVIEW_RATING_THREE_STAR,
        MasterReview.TWO_STAR: config.REVIEW_RATING_TWO_STAR,
        MasterReview.ONE_STAR: config.REVIEW_RATING_ONE_STAR,
    }[stars]
    if not score:
        score = 0
        log.error('Unknown stars: %s', stars)
    return score


def add_rating_for_repairs(master):
    saved_repair_ids = (
        MasterRatingLog.query_repairs().filter(master=master).values_list('repair_order_item_id', flat=True).all()
    )
    new_repairs = (
        RepairOrderItem.objects.filter(
            repair_order__master=master,
            repair_order__status=RepairOrder.STATUS_DONE,
        )
        .exclude(
            id__in=saved_repair_ids,
        )
        .values('id', 'repair_order__master_id', 'device_model_repair__device_model__slug')
    )
    return reduce(save_repair_rating, new_repairs, 0)


def save_repair_rating(acc, repair):
    score = get_score_for_slug(repair['device_model_repair__device_model__slug'])
    _, created = MasterRatingLog.objects.get_or_create(
        master_id=repair['repair_order__master_id'],
        rating_type=MasterRatingLog.RATING_REPAIRS,
        amount=score,
        repair_order_item_id=repair['id'],
    )
    return acc + (score if created else 0)


def get_score_for_slug(slug):
    if slug.startswith('iphone3'):
        return config.REPAIR_RATING_IPHONE_3
    elif slug.startswith('iphone4'):
        return config.REPAIR_RATING_IPHONE_4
    elif slug.startswith('iphone5'):
        return config.REPAIR_RATING_IPHONE_5
    elif slug.startswith('iphone6'):
        return config.REPAIR_RATING_IPHONE_6
    elif slug.startswith('ipad'):
        return config.REPAIR_RATING_IPAD
    else:
        return config.REPAIR_RATING_OTHER_DEVICES


def add_rating_for_courses(master):
    saved_course_ids = (
        MasterRatingLog.query_courses().filter(master=master).values_list('master_course_id', flat=True).all()
    )
    new_finished_courses = (
        MasterCourses.objects.filter(
            master=master,
            completed=True,
            lesson_event__lesson__is_exam=True,
            lesson_event__lesson__course__course_type__isnull=False,
        )
        .exclude(
            id__in=saved_course_ids,
        )
        .values('master_id', 'score', 'id')
    )
    return reduce(save_course_rating, new_finished_courses, 0)


def save_course_rating(acc, finished_course):
    _, created = MasterRatingLog.objects.get_or_create(
        master_id=finished_course['master_id'],
        rating_type=MasterRatingLog.RATING_COURSES,
        amount=finished_course['score'],
        master_course_id=finished_course['id'],
    )
    return acc + (finished_course['score'] if created else 0)


def remove_outdated_rating(master):
    outdated_repair_rating = (
        MasterRatingLog.query_repairs().filter(
            master=master,
            created_at__lt=timezone.now() - timedelta(days=config.REPAIR_RATING_DURATION),
            active=True,
        )
        .values('amount', 'id')
    )
    outdated_review_rating = (
        MasterRatingLog.query_reviews().filter(
            master=master,
            created_at__lt=timezone.now() - timedelta(days=config.REVIEW_RATING_DURATION),
            active=True,
        )
        .values('amount', 'id')
    )
    return reduce(outdate_master_log, chain(outdated_repair_rating, outdated_review_rating), 0)


def outdate_master_log(acc, log):
    MasterRatingLog.objects.filter(id=log['id']).update(active=False)
    return acc - log['amount']


rating_functions = [
    add_rating_for_courses,
    add_rating_for_repairs,
    add_rating_for_reviews,
    remove_outdated_rating,
]
