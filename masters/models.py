# coding: utf-8
from decimal import Decimal
from constance import config

from django.conf import settings
from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import localtime
from django.utils.timezone import now as utc_now
from django.utils.translation import ugettext_lazy as _
from django.utils import formats
from django.db.models import F
from django.core.urlresolvers import reverse
from django.template.defaultfilters import escape

from accounts.models import Region, City, MetroStation


class Master(models.Model):
    user = models.OneToOneField('accounts.User', related_name='master')
    registration_comment = models.TextField(_('Комментарий'), help_text=_('Комментарий который был введен при регистрации'), blank=True, null=True)
    is_certificated = models.BooleanField(default=False, verbose_name='Сертифицированный')
    lessons = models.ManyToManyField('masters.CourseLessonEvent', through='MasterCourses', through_fields=('master', 'lesson_event',))
    can_work = models.BooleanField(verbose_name=_('Может принимать заказы'), default=False)
    rating = models.IntegerField(_('Рейтинг'), default=0)
    rejection_coef = models.DecimalField(max_digits=3, decimal_places=2, default=Decimal(1), verbose_name=_('Коэф отказов'))
    parts = models.ManyToManyField('devices.Part', verbose_name=_('Запчасти'), through='MasterPart', through_fields=('master', 'part'))
    certified_models = models.ManyToManyField('devices.DeviceModel', verbose_name=_('Модели устройств которые мастер может брать в работу'), blank=True)

    send_sms_repair_order_notification = models.BooleanField(_('Сообщать о новом заказе по sms?'), default=False)
    send_email_repair_order_notification = models.BooleanField(_('Сообщать о новом заказе по email?'), default=False)
    take_credit_date = models.DateTimeField(_('Дата взятия кредита'), blank=True, null=True)
    parts_control = models.BooleanField(_('Контроль закупки запчастей'), default=True)

    class Meta:
        verbose_name = _('Мастер')
        verbose_name_plural = _('Мастера')

    def __str__(self):
        return self.user.get_full_name()

    def get_lesson_evets(self):
        if not getattr(self, '_lesson_events', None):
            self._lesson_events = self.courses.all()
        return self._lesson_events

    def get_lesson_event_ids(self):
        if not getattr(self, '_lesson_event_ids', None):
            self._lesson_event_ids = [event.lesson_event.id for event in self.get_lesson_evets()]
        return self._lesson_event_ids

    def get_completed_lesson_ids(self):
        if not getattr(self, '_completed_lesson_ids', None):
            self._completed_lesson_ids = self.courses.filter(completed=True).values_list('lesson_event__lesson', flat=True)
        return self._completed_lesson_ids

    @property
    def is_certificated_display(self):
        if self.user.gender == 'male':
            return _('Аттестован') if self.is_certificated else _('Не аттестован')
        else:
            return _('Аттестована') if self.is_certificated else _('Не аттестована')

    @property
    def city(self):
        addres = self.user.get_address()
        return addres.city if addres else None

    @property
    def total_rating(self):
        return self.rating * self.rejection_coef

    def get_status(self):
        if getattr(self, '_master_status_cache', None) is None:
            rating = self.total_rating if self.total_rating >= 0 else 0
            self._master_status_cache = MasterStatus.objects.filter(rating__lte=rating).order_by('-rating').first()
        return self._master_status_cache

    @property
    def status_name(self):
        return self.get_status().name if self.get_status() else None

    @property
    def status_color_hex(self):
        return self.get_status().color_hex if self.get_status() else None

    def get_master_course(self, course_id):
        return self.courses.filter(lesson_event_id=course_id).first()

    @property
    def scores(self):
        return self.courses.aggregate(score=Sum('score')).get('score')

    @property
    def currency_code(self):
        return 643

    @property
    def currency(self):
        return settings.CURRENCY_RUB

    @property
    def sent_amount(self):
        if getattr(self, '_sent_amount', None) is None:
            self._sent_amount = self.user.sender_transactions.aggregate(amount=Sum('amount')).get('amount')
        return self._sent_amount or Decimal('0')

    @property
    def received_amount(self):
        if getattr(self, '_received_amount', None) is None:
            self._received_amount = self.user.recipient_transactions.aggregate(amount=Sum('amount')).get('amount')
        return self._received_amount or Decimal('0')

    @property
    def balance(self):
        return self.received_amount - self.sent_amount

    @property
    def credit_amount(self):
        return self.get_status().credit_amount if self.get_status() else 0

    @property
    def balance_with_credit(self):
        return self.balance + self.credit_amount

    @property
    def remain_credit_amount(self):
        if self.take_credit_date:
            return self.credit_amount - abs(self.balance)
        return 0

    @property
    def used_credit_amount(self):
        if self.take_credit_date:
            return abs(self.balance)
        return 0

    @property
    def credit_days(self):
        return config.MASTER_CREDIT_DAYS

    def remain_credit_days(self):
        if self.take_credit_date is None:
            return config.MASTER_CREDIT_DAYS
        days = (utc_now() - self.take_credit_date).days
        return self.credit_days - days if days < self.credit_days - days else 0

    def remain_credit_hours(self):
        if self.take_credit_date is None:
            return config.MASTER_CREDIT_DAYS * 24
        hours = (utc_now() - self.take_credit_date).total_seconds() // 3600
        return self.credit_days * 24 - hours if hours < self.credit_days * 24 - hours else 0

    def can_use_credit(self):
        if self.take_credit_date is None:
            return True
        return self.remain_credit_days() > 0

    def get_instock_parts(self):
        return self.parts_status.select_related('part').filter(status=MasterPart.STATUS_INSTOCK)

    def get_reserved_parts(self):
        return self.reserved_parts.select_related('vendor_part', 'vendor_part__part').filter(status=PartOrderItem.STATUS_CREATED)


class MasterPart(models.Model):
    STATUS_INSTOCK = 'instock'
    STATUS_CHARGE_OFF = 'charge_off'
    STATUS_CHOICES = (
        (STATUS_INSTOCK, _('В наличии')),
        (STATUS_CHARGE_OFF, _('Списанная'))
    )
    master = models.ForeignKey(Master, related_name='parts_status')
    part = models.ForeignKey('devices.Part', verbose_name=_('Запчасть'))
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, default=STATUS_INSTOCK)

    class Meta:
        verbose_name = _('Запчасть мастера')
        verbose_name_plural = _('Запчасти мастера')


class Course(models.Model):
    IPHONE_4 = 'iphone4'
    IPHONE_5 = 'iphone5'
    IPHONE_6 = 'iphone6'
    IPAD = 'ipad'

    COURSE_TYPES = (
        (IPHONE_4, _('Iphone 4-ки')),
        (IPHONE_5, _('Iphone 5-ки')),
        (IPHONE_6, _('Iphone 6-ки')),
        (IPAD, _('Ipad\'ы')),
    )
    name = models.CharField(_('Название'), max_length=255, help_text=_('Название курса'))
    course_type = models.CharField(_('Тип курса'), max_length=255, choices=COURSE_TYPES, null=True)

    class Meta:
        verbose_name = _('Курс')
        verbose_name_plural = _('Курсы')

    def __str__(self):
        return self.name


class CourseLesson(models.Model):
    course = models.ForeignKey(Course, related_name='exercises', verbose_name=_('Курс'))
    number = models.IntegerField(_('Последовательный номер'))
    name = models.CharField(_('Название'), max_length=255)
    is_exam = models.BooleanField(_('Экзамен?'), default=False)

    class Meta:
        verbose_name = _('Урок')
        verbose_name_plural = _('Уроки')
        ordering = ('number', )

    def __str__(self):
        return self.name


class CourseLessonEvent(models.Model):
    lesson = models.ForeignKey(CourseLesson, related_name='events', verbose_name=_('Урок'))
    starttime = models.DateTimeField()
    address = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Занятие')
        verbose_name_plural = _('Занятия')

    def __str__(self):
        return '{} {} {}'.format(
            self.lesson.name,
            formats.date_format(localtime(self.starttime), "SHORT_DATETIME_FORMAT"),
            self.address
        )


class MasterCourses(models.Model):
    lesson_event = models.ForeignKey(CourseLessonEvent, related_name='masters', verbose_name=_('Занятие'))
    master = models.ForeignKey(Master, related_name='courses', verbose_name=_('Мастер'))
    completed = models.BooleanField(default=False, verbose_name=_('Завершен'))
    score = models.IntegerField(_('Оценка'), blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return _('Курс мастера {} (score={})').format(self.id, self.score)

    class Meta:
        verbose_name = _('Занятие мастера')
        verbose_name_plural = _('Занятия мастера')


@receiver(post_save, sender=MasterCourses)
def register_master_to_update_rating_if_needed(sender, instance, created, **kwargs):
    from masters.rating import register_master_to_update_rating
    register_master_to_update_rating(instance.master_id)


class MasterStatus(models.Model):
    name = models.CharField(_('Наименование'), max_length=255)
    rating = models.IntegerField(_('Порог рейтинга'))
    color_hex = models.CharField(max_length=6, blank=True, null=True)
    credit_amount = models.IntegerField(_('Кредит доверия'), default=0)

    class Meta:
        verbose_name = _('Статус мастера')
        verbose_name_plural = _('Статусы мастеров')

    def __str__(self):
        return self.name

    @classmethod
    def get_max(cls):
        return cls.objects.order_by('-rating').first()


class PartOrderItem(models.Model):
    STATUS_CREATED = 'created'
    STATUS_CANCELLED = 'cancelled'
    STATUS_RECEIVED = 'received'

    STATUS_CHOICES = (
        (STATUS_CREATED, STATUS_CREATED),
        (STATUS_CANCELLED, STATUS_CANCELLED),
        (STATUS_RECEIVED, STATUS_RECEIVED),
    )

    master = models.ForeignKey(Master, verbose_name=_('Мастер'), null=True, related_name='reserved_parts')
    vendor_part = models.ForeignKey('devices.VendorPart', verbose_name=_('Запчасть'))
    order_number = models.CharField(_('Код заказа'), max_length=255, null=True)
    warehouse = models.ForeignKey('devices.VendorWarehouse', verbose_name=_('Склад'), null=True)
    quantity = models.IntegerField(_('Количество'))
    price = models.DecimalField(max_digits=14, decimal_places=2, verbose_name=_('Цена'))
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, default=STATUS_CREATED)
    for_order = models.ForeignKey('orders.RepairOrder', related_name='parts_orders', verbose_name=_('Для заказа'), blank=True, null=True)
    cancel_comment = models.CharField(_('Комментарий отказа'), max_length=255, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Cоздан'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Обновлен'))

    class Meta:
        verbose_name = _('Заказ запчасти')
        verbose_name_plural = _('Заказы запчастей')


class MasterRatingLog(models.Model):
    RATING_COURSES = 'courses'
    RATING_REPAIRS = 'repairs'
    RATING_REVIEWS = 'reviews'
    RATING_BONUS = 'bonus'
    RATING_PENALTY_PART = 'penalty_part'

    RATING_TYPES = (
        (RATING_COURSES, _('Рейтинг за курсы')),
        (RATING_REPAIRS, _('Рейтинг за отремонтированные поломки')),
        (RATING_REVIEWS, _('Рейтинг за оценки')),
        (RATING_BONUS, _('Бонус Рейтинг')),
        (RATING_PENALTY_PART, _('Штраф за покупку запчасти у других поставщиков')),
    )
    master = models.ForeignKey('masters.Master', related_name='rating_log', verbose_name=_('Мастер'))
    rating_type = models.CharField(_('Тип рейтинга'), max_length=255, choices=RATING_TYPES)
    amount = models.IntegerField()
    master_course = models.ForeignKey(MasterCourses, verbose_name=_('Курс Мастера'), null=True, on_delete=models.CASCADE, blank=True)
    repair_order_item = models.ForeignKey('orders.RepairOrderItem', verbose_name=_('Отремонтированная поломка'), null=True, on_delete=models.CASCADE, blank=True)
    master_review = models.ForeignKey('masters.MasterReview', verbose_name=_('Оценка'), null=True, on_delete=models.CASCADE, blank=True)
    penalty_order_part = models.ForeignKey('orders.RepairOrderItemPart', verbose_name=_('Штрафная запчасть'), null=True, on_delete=models.CASCADE, blank=True)
    active = models.BooleanField(_('Не просроченный'), default=True)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Cоздан'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Обновлен'))

    class Meta:
        verbose_name = _('Лог рейтинга')
        verbose_name_plural = _('Логи рейтинга')

    @classmethod
    def query_courses(cls):
        return cls.objects.filter(rating_type=cls.RATING_COURSES)

    @classmethod
    def query_repairs(cls):
        return cls.objects.filter(rating_type=cls.RATING_REPAIRS)

    @classmethod
    def query_reviews(cls):
        return cls.objects.filter(rating_type=cls.RATING_REVIEWS)


@receiver(post_save, sender=MasterRatingLog)
def update_master_rating_if_bonus(sender, instance, created, **kwargs):
    if created and instance.rating_type == MasterRatingLog.RATING_BONUS:
        Master.objects.filter(id=instance.master_id).update(rating=F('rating') + instance.amount)


class MasterReview(models.Model):
    FIVE_STAR = 5
    FOUR_STAR = 4
    THREE_STAR = 3
    TWO_STAR = 2
    ONE_STAR = 1

    REVIEW_SCORES = (
        (FIVE_STAR, _('5 звезд')),
        (FOUR_STAR, _('4 звезды')),
        (THREE_STAR, _('3 звезды')),
        (TWO_STAR, _('2 звезды')),
        (ONE_STAR, _('1 звезда')),
    )
    master = models.ForeignKey('masters.Master', related_name='reviews', verbose_name=_('Мастер'))
    score = models.IntegerField(choices=REVIEW_SCORES, verbose_name=_('Оценка'))
    repair_order = models.ForeignKey('orders.RepairOrder', verbose_name=_('Заказ'))

    user = models.ForeignKey('accounts.User', related_name='reviews', verbose_name=_('заказчик'), null=True)
    comment = models.CharField(_('Комментарий'), max_length=255, blank=True, null=True)

    as_in_order = models.BooleanField(_('Тот что в заказе'), help_text=_('Для отправки бонуса'))
    email = models.EmailField(blank=True, null=True, help_text=_('Для отправки бонуса'))
    phone_number = models.CharField(_('Номер телефона'), max_length=255, blank=True, null=True, help_text=_('Для отправки бонуса'))

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Cоздан'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Обновлен'))

    def repair_order_link(self):
        repair_order = self.repair_order
        return '<a href="%s">%s</a>' % (reverse("admin:orders_repairorder_change", args=(repair_order.id,)), escape(repair_order))
    repair_order_link.short_description = _('Заказ')
    repair_order_link.allow_tags = True
    repair_order_link.admin_order_field = 'repair_order'

    class Meta:
        verbose_name = _('Оценка мастера')
        verbose_name_plural = _('Оценки мастера')


@receiver(post_save, sender=MasterReview)
def register_master_to_update_rating_if_needed_for_master_review(sender, instance, created, **kwargs):
    from masters.rating import register_master_to_update_rating
    if instance.master_id:
        register_master_to_update_rating(instance.master_id)


class MasterPlace(models.Model):
    master = models.ForeignKey(Master, related_name='places')
    region = models.ForeignKey(Region, null=True)
    city = models.ForeignKey(City, null=True)
    metro_stations = models.ManyToManyField(MetroStation, blank=True)
    all_metro = models.BooleanField(_('Все метро'), default=False)

    def __str__(self):
        return '{} {} {}'.format(self.master, self.region, self.city)
