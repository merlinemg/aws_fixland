from django.utils import timezone
import factory

from .models import Master, MasterStatus, MasterCourses, CourseLessonEvent
from .models import CourseLesson, Course, MasterReview, MasterRatingLog, MasterPlace


class MasterFactory(factory.DjangoModelFactory):
    class Meta:
        model = Master

    user = factory.SubFactory('accounts.factories.UserFactory')
    send_sms_repair_order_notification = True
    send_email_repair_order_notification = True

class CourseFactory(factory.DjangoModelFactory):
    class Meta:
        model = Course

    name = 'test'
    course_type = Course.IPHONE_4


class CourseLessonFactory(factory.DjangoModelFactory):
    class Meta:
        model = CourseLesson

    course = factory.SubFactory(CourseFactory)
    number = 1
    name = 'Test'
    is_exam = True


class CourseLessonEventFactory(factory.DjangoModelFactory):
    class Meta:
        model = CourseLessonEvent

    lesson = factory.SubFactory(CourseLessonFactory)
    starttime = timezone.now()
    address = 'Test address'


class MasterCoursesFactory(factory.DjangoModelFactory):
    class Meta:
        model = MasterCourses

    master = factory.SubFactory(MasterFactory)
    lesson_event = factory.SubFactory(CourseLessonEventFactory)


class MasterReviewFactory(factory.DjangoModelFactory):
    class Meta:
        model = MasterReview

    master = factory.SubFactory(MasterFactory)
    repair_order = factory.SubFactory('orders.factories.RepairOrderFactory')
    as_in_order = True


class MasterRatingLogFactory(factory.DjangoModelFactory):
    class Meta:
        model = MasterRatingLog

    master = factory.SubFactory(MasterFactory)


class MasterPlaceFactory(factory.DjangoModelFactory):
    class Meta:
        model = MasterPlace

    master = factory.SubFactory(MasterFactory)


def create_master_statuses(master):
    MasterStatus.objects.create(
        name='status 1',
        rating=500,
        credit_amount=300
    )

    MasterStatus.objects.create(
        name='status 2',
        rating=1000,
        credit_amount=500
    )

    MasterStatus.objects.create(
        name='status 3',
        rating=2000,
        credit_amount=1000
    )
