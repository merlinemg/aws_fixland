from django import forms
from django.conf import settings
from django.utils.dates import MONTHS
from django.utils import six
from core.time_utils import utc_now


class PaymentForm(forms.Form):
    payment_amount = forms.DecimalField(min_value=settings.MIN_PAYMENT_AMOUNT, max_value=settings.MAX_PAYMENT_AMOUNT, decimal_places=2)


class AgentReportForm(forms.Form):
    YEAR_CHOICES = {}
    year = forms.ChoiceField(choices=YEAR_CHOICES)
    month = forms.ChoiceField(choices=list(six.iteritems(MONTHS)))

    def __init__(self, master=None, *args, **kwargs):
        super(AgentReportForm, self).__init__(*args, **kwargs)
        if master:
            year_from = master.user.date_joined.year
            self.fields['year'].choices = [(i, i) for i in range(year_from, utc_now().year + 1)]
