# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_master'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='email_confirmed',
            field=models.BooleanField(verbose_name='Email подтвержден?', default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='phone_confirmed',
            field=models.BooleanField(verbose_name='Телефон подтвержден?', default=False),
        ),
    ]
