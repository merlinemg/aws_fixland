# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0020_auto_20150819_2033'),
    ]

    operations = [
        migrations.AddField(
            model_name='metroline',
            name='city',
            field=models.ForeignKey(verbose_name='Город', to='accounts.City', null=True),
        ),
    ]
