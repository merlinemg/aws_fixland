# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_metro_line_cities(apps, schema_editor):
    City = apps.get_model("accounts", "City")
    MetroLine = apps.get_model("accounts", "MetroLine")
    metro_lines = MetroLine.objects.all()
    for line in metro_lines:
        city, created = City.objects.get_or_create(name=line.city_text)
        line.city = city
        line.save()


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0022_auto_20150819_2223'),
    ]

    operations = [
        migrations.RunPython(update_metro_line_cities),
    ]
