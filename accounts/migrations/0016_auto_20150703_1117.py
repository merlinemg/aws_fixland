# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0015_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metroline',
            name='color_hex',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
    ]
