# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_add_feedback'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='master',
            name='user',
        ),
        migrations.DeleteModel(
            name='Master',
        ),
    ]
