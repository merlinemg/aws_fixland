# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20150430_1345'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('raw', models.CharField(max_length=255)),
                ('country', models.CharField(max_length=255, blank=True, null=True)),
                ('region', models.CharField(max_length=255, blank=True, null=True)),
                ('city', models.CharField(max_length=255, blank=True, null=True)),
                ('zip_code', models.CharField(max_length=255, blank=True, null=True)),
                ('street', models.CharField(max_length=255, blank=True, null=True)),
                ('house', models.CharField(max_length=255, blank=True, null=True)),
                ('appartment', models.CharField(max_length=255, blank=True, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=255)),
                ('phone_type', models.CharField(default='OWN', max_length=255, choices=[('OWN', 'Мой'), ('FRIEND', 'Друга')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
