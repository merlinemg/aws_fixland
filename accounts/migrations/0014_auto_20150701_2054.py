# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0013_user_avatar'),
    ]

    operations = [
        migrations.CreateModel(
            name='MetroLine',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(verbose_name='Название', max_length=255)),
                ('city', models.CharField(verbose_name='Город', max_length=255)),
                ('color_hex', models.CharField(max_length=6)),
            ],
            options={
                'verbose_name_plural': 'Ветки метро',
                'verbose_name': 'Ветка метро',
            },
        ),
        migrations.CreateModel(
            name='MetroStation',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(verbose_name='Название', max_length=255)),
                ('line', models.ForeignKey(verbose_name='Ветка метро', to='accounts.MetroLine', related_name='station')),
            ],
            options={
                'verbose_name_plural': 'Станции метро',
                'verbose_name': 'Станция метро',
            },
        ),
        migrations.AlterField(
            model_name='user',
            name='date_joined',
            field=models.DateTimeField(verbose_name='Дата регистрации', default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(help_text='Активный? Может войти на сатй?', verbose_name='active', default=False),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_staff',
            field=models.BooleanField(help_text='Может заходить в админку?', verbose_name='staff status', default=False),
        ),
        migrations.AddField(
            model_name='address',
            name='metro',
            field=models.ForeignKey(null=True, blank=True, verbose_name='Станция метро', to='accounts.MetroStation'),
        ),
    ]
