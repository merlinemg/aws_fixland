# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0021_metroline_city'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='region',
            field=models.ForeignKey(null=True, to='accounts.Region', related_name='zone_cities', verbose_name='Регион'),
        ),
        migrations.AlterField(
            model_name='metroline',
            name='city',
            field=models.ForeignKey(null=True, blank=True, to='accounts.City', verbose_name='Город'),
        ),
    ]
