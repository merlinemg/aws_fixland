# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0019_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='metroline',
            old_name='city',
            new_name='city_text',
        ),
    ]
