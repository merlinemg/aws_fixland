import httpretty
from django.conf import settings
from django.test import TestCase

from accounts.models import User
from ..models import MetroLine, MetroStation, Address


# Taras: These tests because of patching socket library - broke another tests
# class UpdateMetroTestCase(TestCase):
#     def setUp(self):
#         settings.CELERY_ALWAYS_EAGER = True
#         self.user = User.objects.create(username='username', first_name='first_name', last_name='last_name')
#         self.metro_line = MetroLine.objects.create(
#             name='Сокольническая линия',
#             city='Москва',
#             color_hex='000000'
#         )
#         self.metro_station = MetroStation.objects.create(name='метро Университет', line=self.metro_line)
#         self.response_coordinates_first = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"МоскваСтроителей3","suggest":"Москва Строителей 3","found":"3","results":"1"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Москва, улица Строителей, 3","precision":"exact","AddressDetails":{"Country":{"AddressLine":"Москва, улица Строителей, 3","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Центральный федеральный округ","SubAdministrativeArea":{"SubAdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"улица Строителей","Premise":{"PremiseNumber":"3"}}}}}}}}},"description":"Москва, Россия","name":"улица Строителей, 3","boundedBy":{"Envelope":{"lowerCorner":"37.529079 55.680473","upperCorner":"37.545536 55.689771"}},"Point":{"pos":"37.537308 55.685122"}}}]}}}'
#         self.response_coordinates_second = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"Москва кравченко 7","found":"1","results":"1"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Россия, Москва, улица Кравченко, 7","precision":"exact","AddressDetails":{"Country":{"AddressLine":"Москва, улица Кравченко, 7","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Центральный федеральный округ","SubAdministrativeArea":{"SubAdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"улица Кравченко","Premise":{"PremiseNumber":"7"}}}}}}}}},"description":"Москва, Россия","name":"улица Кравченко, 7","boundedBy":{"Envelope":{"lowerCorner":"37.507663 55.671991","upperCorner":"37.524121 55.68129"}},"Point":{"pos":"37.515892 55.676641"}}}]}}}'
#         self.response_coordinates_bad_address = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"citystreet33333333333","found":"0","results":"1"}},"featureMember":[]}}}'
#         self.response_address_first = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"37.537308 55.685122","found":"196","results":"1","boundedBy":{"Envelope":{"lowerCorner":"37.287307 55.434324","upperCorner":"37.787309 55.934316"}},"Point":{"pos":"37.537308 55.685122"},"kind":"metro"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"metro","text":"Россия, Москва, Сокольническая линия, метро Университет","precision":"other","AddressDetails":{"Country":{"AddressLine":"Москва, Сокольническая линия, метро Университет","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Центральный федеральный округ","SubAdministrativeArea":{"SubAdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"Сокольническая линия","Premise":{"PremiseName":"метро Университет"}}}}}}}}},"description":"Сокольническая линия, Москва, Россия","name":"метро Университет","boundedBy":{"Envelope":{"lowerCorner":"37.526304 55.687792","upperCorner":"37.542761 55.697087"}},"Point":{"pos":"37.534532 55.692440"}}}]}}}'
#         self.response_address_second = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"37.515892 55.676641","found":"193","results":"1","boundedBy":{"Envelope":{"lowerCorner":"37.265891 55.425839","upperCorner":"37.765893 55.925838"}},"Point":{"pos":"37.515892 55.676641"},"kind":"metro"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"metro","text":"Россия, Москва, Сокольническая линия, метро Проспект Вернадского","precision":"other","AddressDetails":{"Country":{"AddressLine":"Москва, Сокольническая линия, метро Проспект Вернадского","CountryNameCode":"RU","CountryName":"Россия","AdministrativeArea":{"AdministrativeAreaName":"Центральный федеральный округ","SubAdministrativeArea":{"SubAdministrativeAreaName":"Москва","Locality":{"LocalityName":"Москва","Thoroughfare":{"ThoroughfareName":"Сокольническая линия","Premise":{"PremiseName":"метро Проспект Вернадского"}}}}}}}}},"description":"Сокольническая линия, Москва, Россия","name":"метро Проспект Вернадского","boundedBy":{"Envelope":{"lowerCorner":"37.497602 55.67226","upperCorner":"37.514059 55.681559"}},"Point":{"pos":"37.505831 55.676910"}}}]}}}'
#         self.response_kyiv_coordinates = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"вулиця Хрещатик, 20-22, Київ, Украина","found":"1","results":"10"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"house","text":"Украина, Киев, улица Крещатик, 20\/22","precision":"exact","AddressDetails":{"Country":{"AddressLine":"Киев, улица Крещатик, 20\/22","CountryNameCode":"UA","CountryName":"Украина","AdministrativeArea":{"AdministrativeAreaName":"Киев","Locality":{"LocalityName":"Киев","Thoroughfare":{"ThoroughfareName":"улица Крещатик","Premise":{"PremiseNumber":"20\/22"}}}}}}}},"description":"Киев, Украина","name":"улица Крещатик, 20\/22","boundedBy":{"Envelope":{"lowerCorner":"30.514827 50.444522","upperCorner":"30.531284 50.455029"}},"Point":{"pos":"30.523056 50.449776"}}}]}}}'
#         self.response_kyiv_metro = '{"response":{"GeoObjectCollection":{"metaDataProperty":{"GeocoderResponseMetaData":{"request":"30.523056 50.449776","found":"54","results":"1","boundedBy":{"Envelope":{"lowerCorner":"30.273054 50.199115","upperCorner":"30.773057 50.699108"}},"Point":{"pos":"30.523056 50.449776"},"kind":"metro"}},"featureMember":[{"GeoObject":{"metaDataProperty":{"GeocoderMetaData":{"kind":"metro","text":"Украина, Киев, Куреневско-Красноармейская линия метро, метро Площадь Независимости","precision":"other","AddressDetails":{"Country":{"AddressLine":"Киев, Куреневско-Красноармейская линия метро, метро Площадь Независимости","CountryNameCode":"UA","CountryName":"Украина","AdministrativeArea":{"AdministrativeAreaName":"Киев","Locality":{"LocalityName":"Киев","Thoroughfare":{"ThoroughfareName":"Куреневско-Красноармейская линия метро","Premise":{"PremiseName":"метро Площадь Независимости"}}}}}}}},"description":"Куреневско-Красноармейская линия метро, Киев, Украина","name":"метро Площадь Независимости","boundedBy":{"Envelope":{"lowerCorner":"30.515968 50.444774","upperCorner":"30.532425 50.455282"}},"Point":{"pos":"30.524196 50.450028"}}}]}}}'

#     @httpretty.activate
#     def test_update_address_metro(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, responses=[
#                                httpretty.Response(body=self.response_coordinates_first),
#                                httpretty.Response(body=self.response_address_first),
#                                httpretty.Response(body=self.response_coordinates_second),
#                                httpretty.Response(body=self.response_address_second)])
#         address = Address.objects.create(
#             user=self.user,
#             city='Москва',
#             street='Строителей',
#             house=3
#         )
#         address = Address.objects.get(id=address.id)
#         self.assertEqual(address.metro.name, 'метро Университет')
#         self.assertEqual(address.metro.line, self.metro_line)

#         address.street = 'кравченко'
#         address.house = 7
#         address.save()

#         address = Address.objects.get(id=address.id)
#         self.assertEqual(address.metro.name, 'метро Проспект Вернадского')
#         self.assertEqual(address.metro.line, self.metro_line)

#     @httpretty.activate
#     def test_update_address_metro_bad_address(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, body=self.response_coordinates_bad_address, content_type="application/json")
#         address = Address.objects.create(
#             user=self.user,
#             city='city',
#             street='street',
#             house=33333333333
#         )
#         self.assertEqual(address.metro, None)

#     @httpretty.activate
#     def test_update_address_metro_kyiv_address(self):
#         httpretty.register_uri(httpretty.GET, settings.YANDEX_MAPS_API_URL, responses=[
#                                httpretty.Response(body=self.response_kyiv_coordinates),
#                                httpretty.Response(body=self.response_kyiv_metro)])
#         address = Address.objects.create(
#             user=self.user,
#             country='Украина',
#             city='Київ',
#             street='вулиця Хрещатик',
#             house=20
#         )
#         address = Address.objects.get(id=address.id)
#         self.assertEqual(address.metro.name, 'метро Площадь Независимости')
#         self.assertEqual(address.metro.line.name, 'Куреневско-Красноармейская линия метро')
