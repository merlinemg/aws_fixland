from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from sitegate.models import EmailConfirmation
from core.utils import send_html_email
from django.core.mail import mail_admins
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
import logging

log = logging.getLogger(__name__)


def send_activation_email(user, template='emails/email_activation.html'):
    code = EmailConfirmation.add(user)
    link = 'http://{}{}'.format(settings.HOST_URL, reverse('activate_account', args=(code.code,)))
    send_html_email(_('Письмо активации'), [user.email],
                    template,
                    {'link': link})


def email_feedback_to_admin(feedback):
    subject = _('Поступил новый отзыв')
    context = {
        'HOST_URL': settings.HOST_URL,
        'feedback': feedback,
    }
    message = render_to_string('emails/email_feedback.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_master_email_to_admin(user):
    subject = _('Зарегистрировался новый мастер {}').format(user.get_full_name())
    link = 'http://{}{}'.format(settings.HOST_URL, reverse('admin:masters_master_change', args=(user.master.id,)))
    context = {
        'user': user,
        'link': link,
    }
    message = render_to_string('emails/new_master_email_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass


def new_partner_email_to_admin(user):
    subject = _('Зарегистрировался новый партнер {}').format(user.get_full_name())
    link = 'http://{}{}'.format(settings.HOST_URL, reverse('admin:partners_partner_change', args=(user.partner.id,)))
    context = {
        'user': user,
        'link': link,
    }
    message = render_to_string('emails/new_partner_email_to_admin.html', context)
    try:
        mail_admins(subject, message, fail_silently=True, html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass

def send_email_answer(email, answer):
    subject = _('fixland.ru - Спасибо за отзыв!')
    context = {
        'answer': answer,
    }
    message = render_to_string('emails/feedback_answer_email.html', context)
    try:
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], html_message=message)
    except Exception:
        log.exception('Send email exception')
        pass

