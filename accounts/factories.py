import factory

from .models import User, Address, MetroStation, City, Region, MetroLine


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'username{}'.format(n))
    email = factory.Sequence(lambda n: 'test{}@mail.com'.format(n))
    is_active = True
    first_name = factory.Sequence(lambda n: 'First{}'.format(n))
    last_name = factory.Sequence(lambda n: 'Last{}'.format(n))

    @classmethod
    def _prepare(cls, create, **kwargs):
        password = kwargs.pop('password', None)
        user = super(UserFactory, cls)._prepare(create, **kwargs)
        if password:
            user.set_password(password)
            if create:
                user.save()
        return user


class AddressFactory(factory.DjangoModelFactory):
    class Meta:
        model = Address

    user = factory.SubFactory(UserFactory)
    raw = factory.Sequence(lambda n: 'raw address {}'.format(n))


class RegionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Region

    name = factory.Sequence(lambda n: 'Region {}'.format(n))
    dadata_name = factory.Sequence(lambda n: 'Region {}'.format(n))


class CityFactory(factory.DjangoModelFactory):
    class Meta:
        model = City

    name = factory.Sequence(lambda n: 'city {}'.format(n))
    dadata_name = factory.Sequence(lambda n: 'city {}'.format(n))
    region = factory.SubFactory(RegionFactory)


class MetroLineFactory(factory.DjangoModelFactory):
    class Meta:
        model = MetroLine

    name = factory.Sequence(lambda n: 'metro line {}'.format(n))
    city = factory.SubFactory(CityFactory)


class MetroStationFactory(factory.DjangoModelFactory):
    class Meta:
        model = MetroStation

    name = factory.Sequence(lambda n: 'metro station {}'.format(n))
    line = factory.SubFactory(MetroLineFactory)
