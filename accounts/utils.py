# coding: utf-8
import logging

from datetime import timedelta
from django.utils.timezone import now as utc_now
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext_lazy as _

from core import utils as core_utils
from core.geoip_utils import get_geoip_record_by_ip
from .models import SmsConfirmation, Address, MetroLine, MetroStation, City, Region

log = logging.getLogger(__name__)


def create_and_send_code(user):
    codes = SmsConfirmation.objects.filter(user=user)
    if not codes or codes and not codes.filter(time_created__gt=utc_now() - timedelta(minutes=5)).exists():
        new_code = SmsConfirmation.add(user=user)
        core_utils.send_sms(user.get_phone_number(), new_code.code)
    else:
        raise PermissionDenied


def send_new_password_sms(phone, context):
    text = _('Новый пароль - {password}').format(password=context.get('password'))
    core_utils.send_sms(phone, text)


def update_address_metro(address_id):
    address = Address.objects.get(id=address_id)
    metro_data = core_utils.get_metro_by_address(address)
    if not metro_data:
        return
    if not metro_data.get('city', None):
        return

    city, created = City.objects.get_or_create(name=metro_data.get('city'))
    metro_line, created = MetroLine.objects.get_or_create(name=metro_data.get('line'), city=city)
    metro_station, created = MetroStation.objects.get_or_create(name=metro_data.get('name'), line=metro_line)
    address.metro = metro_station
    address.save()


def get_region_by_ip(ip_address):
    geoip_record = get_geoip_record_by_ip(ip_address)
    if geoip_record is None:
        log.error('No geoip record for ip address %s', ip_address)
        return None
    if not geoip_record['region_name']:
        log.error('Geoip record region_name is empty for ip address %s, geoip record: %s', ip_address, geoip_record)
        return None
    region = Region.objects.filter(geoip_name=geoip_record['region_name']).first()
    if not region:
        log.error('Region with geoip_name %s does not exists', geoip_record['region_name'])
    return region
