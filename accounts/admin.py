# coding: utf-8
from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _


from .models import User, Phone, Address, SmsConfirmation, FeedBack, MetroLine, MetroStation, Region, City
from accounts.forms import FeedbackAdminForm

class AdminUserCreationForm(UserCreationForm):
    error_messages = {
        'duplicate_email': _("Пользователь с таким email уже существует."),
        'password_mismatch': _("Пароли не совпадают."),
    }
    username = None
    email = forms.EmailField(label=_("Email"), max_length=255)
    password1 = forms.CharField(
        label=_("Пароль"), widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_("Подтверждение пароля"),
        widget=forms.PasswordInput,
        help_text=_("Введите пароль еще раз."))

    class Meta:
        model = User
        fields = ("email",)

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )


class AdminUserChangeForm(UserChangeForm):
    username = None
    email = forms.EmailField(label=_("Email"), max_length=255, required=False)


class PhoneAdminInline(admin.TabularInline):
    extra = 0
    model = Phone


class AddressAdminInline(admin.TabularInline):
    extra = 0
    model = Address


class IsMasterListFilter(admin.SimpleListFilter):
    title = _('Фильтр мастеров')
    parameter_name = 'is_master'

    def lookups(self, request, model_admin):
        return (
            ('true', _('Мастер')),
            ('false', _('Обычный пользователь')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(master__isnull=False)
        if self.value() == 'false':
            return queryset.filter(master__isnull=True)


def is_master(self):
    return self.is_master()

def is_partner(self):
    return self.is_partner()

is_partner.boolean = True
is_master.short_description = _('Статус партнера')

is_master.boolean = True
is_master.short_description = _('Статус мастера')


class UserAdmin(DjangoUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password', 'email_confirmed', 'phone_confirmed')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'middle_name', 'email', 'gender', 'avatar')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('username', 'email', 'first_name', 'last_name', 'middle_name', 'is_staff', is_master, is_partner, 'date_joined')
    search_fields = ('first_name', 'last_name', 'email')
    list_display_links = ('username', 'email', 'first_name', 'last_name')
    ordering = ('-date_joined',)
    form = AdminUserChangeForm
    add_form = AdminUserCreationForm
    inlines = (PhoneAdminInline, AddressAdminInline)
    date_hierarchy = 'date_joined'
    list_filter = (IsMasterListFilter, 'is_staff', 'is_superuser', 'is_active', 'groups')
    change_form_template = 'loginas/change_form.html'

    def get_queryset(self, request):
        queryset = super(UserAdmin, self).get_queryset(request)
        return queryset.prefetch_related('master')


class SmsConfirmationAdmin(admin.ModelAdmin):
    readonly_fields = ('code', 'time_accepted')


class FeedBackAdmin(admin.ModelAdmin):
    list_display = ('name', 'contact', 'feedback_text', 'created_at', '_is_answered')
    form = FeedbackAdminForm

    def _is_answered(self, obj):
        if obj.feedback_answer:
            return _('Да')
        else:
            return _('Нет')
    _is_answered.short_description = _('Ответ')

    class Media:
        js = ('js/confirm_feedback_answer.js', )


class CityAdmin(admin.ModelAdmin):
    title = _('Города')
    list_display = ('region', 'name', 'is_metro_related')


class RegionAdmin(admin.ModelAdmin):
    title = _('Регионы')
    list_display = ('name', 'is_central_region')


class MetroStationAdmin(admin.ModelAdmin):
    title = _('Станции Метро')
    list_display = ('name', 'line')

admin.site.register(User, UserAdmin)
admin.site.register(SmsConfirmation, SmsConfirmationAdmin)
admin.site.register(FeedBack, FeedBackAdmin)
admin.site.register(MetroLine)
admin.site.register(MetroStation, MetroStationAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Phone)
admin.site.register(Address)
