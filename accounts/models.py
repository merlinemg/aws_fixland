# coding: utf-8
import uuid
from random import randint
from datetime import timedelta
from constance import config
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.db import models, IntegrityError
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now as utc_now


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        ('male', _('Мужчина')),
        ('female', _('Женщина')),
    )
    username = models.CharField(unique=True, max_length=255)
    first_name = models.CharField(_('Имя'), max_length=255, blank=True)
    last_name = models.CharField(_('Фамилия'), max_length=255, blank=True)
    middle_name = models.CharField(_('Отчество'), max_length=255, blank=True)
    email = models.EmailField(blank=True, null=True)

    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Может заходить в админку?'))
    is_active = models.BooleanField(
        _('active'), default=False,
        help_text=_('Активный? Может войти на сатй?'))
    date_joined = models.DateTimeField(_('Дата регистрации'), default=timezone.now)
    email_confirmed = models.BooleanField(_('Email подтвержден?'), default=False)
    phone_confirmed = models.BooleanField(_('Телефон подтвержден?'), default=False)
    gender = models.CharField(_('Пол'), max_length=255, choices=GENDER_CHOICES, default='male', blank=True, null=True)
    avatar = models.ImageField(upload_to='avatars/', blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')

    @staticmethod
    def autocomplete_search_fields():
        return ('first_name__icontains', 'last_name__icontains', 'middle_name__icontains')

    def save(self, *args, **kwargs):
        if not self.username:
            if self.email:
                self.username = self.email
            else:
                self.username = uuid.uuid4()
        super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.get_full_name()

    @property
    def avatar_url(self):
        try:
            return self.avatar.url
        except:
            return settings.DEFAULT_USER_AVATAR

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def get_phone(self):
        if getattr(self, '_phone_cache', None) is None:
            self._phone_cache = self.phones.first()
        return self._phone_cache

    def get_phone_number(self):
        return self.get_phone().number if self.get_phone() else None

    def get_phone_type(self):
        return self.get_phone().phone_type if self.get_phone() else None

    def get_phone_type_display(self):
        return self.get_phone().get_phone_type_display() if self.get_phone() else None

    def has_own_phone(self):
        return self.get_phone_type() == Phone.OWN_PHONE

    def is_master(self):
        from masters.models import Master

        try:
            return bool(self.master)
        except Master.DoesNotExist:
            return False

    def is_partner(self):
        from partners.models import Partner

        try:
            return bool(self.partner)
        except Partner.DoesNotExist:
            return False

    def get_address(self):
        return self.address_set.first()


class Phone(models.Model):
    OWN_PHONE = 'OWN'
    FRIEND_PHONE = 'FRIEND'

    PHONE_TYPE_CHOICES = (
        (OWN_PHONE, _('Мой')),
        (FRIEND_PHONE, _('Друга')),
    )
    user = models.ForeignKey(User, related_name='phones')
    number = models.CharField(_('Номер телефона'), max_length=255)
    phone_type = models.CharField(_('Тип'), choices=PHONE_TYPE_CHOICES, default=OWN_PHONE, max_length=255)

    class Meta:
        verbose_name = _('Номер телефона')
        verbose_name_plural = _('Номера телефонов')


class MetroLine(models.Model):
    name = models.CharField(_('Название'), max_length=255)
    city = models.ForeignKey('City', verbose_name=_('Город'), blank=True, null=True)
    color_hex = models.CharField(max_length=6, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Ветка метро')
        verbose_name_plural = _('Ветки метро')


class MetroStation(models.Model):
    line = models.ForeignKey(MetroLine, verbose_name=_('Ветка метро'), related_name='station')
    name = models.CharField(_('Название'), max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Станция метро')
        verbose_name_plural = _('Станции метро')

    @property
    def line_display(self):
        return self.line.name

    @property
    def line_color(self):
        return self.line.color_hex


class City(models.Model):
    region = models.ForeignKey('Region', verbose_name=_(u'Регион'), related_name='zone_cities', null=True)
    name = models.CharField(_('Название'), max_length=255)
    dadata_name = models.CharField(_('Название dadata'), max_length=255)
    is_metro_related = models.BooleanField(_('Есть метро?'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _(u'Город')
        verbose_name_plural = _(u'Города')


class Region(models.Model):
    name = models.CharField(_(u'Название'), max_length=255)
    dadata_name = models.CharField(_('Название dadata'), max_length=255)
    geoip_name = models.CharField(_('Название GeoIp'), max_length=255, blank=True, null=True)
    is_central_region = models.BooleanField(_('Центральный регион?'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _(u'Регион')
        verbose_name_plural = _('Регионы')

    @classmethod
    def get_default(cls):
        return cls.objects.filter(dadata_name=config.DEFAULT_REGION_FOR_PRICE).first() or cls.objects.first()


class Address(models.Model):
    user = models.ForeignKey(User)
    raw = models.CharField(_('В сыром виде'), max_length=255)
    country = models.CharField(_('Страна'), max_length=255, blank=True, null=True)
    region = models.CharField(_('Регион'), max_length=255, blank=True, null=True)
    city = models.CharField(_('Город'), max_length=255, blank=True, null=True)
    zip_code = models.CharField(_('Индекс'), max_length=255, blank=True, null=True)
    street = models.CharField(_('Улица'), max_length=255, blank=True, null=True)
    house = models.CharField(_('Дом'), max_length=255, blank=True, null=True)
    appartment = models.CharField(_('Квартира'), max_length=255, blank=True, null=True)
    metro = models.ForeignKey(MetroStation, verbose_name=_('Станция метро'), blank=True, null=True)
    lon = models.FloatField(_('Широта'), blank=True, null=True)
    lat = models.FloatField(_('Долгота'), blank=True, null=True)

    class Meta:
        verbose_name = _('Адресс')
        verbose_name_plural = _('Адресса')

    def __str__(self):
        return self.raw

    def __init__(self, *args, **kwargs):
        super(Address, self).__init__(*args, **kwargs)
        self._orig_country = self.country
        self._orig_region = self.region
        self._orig_city = self.city
        self._orig_street = self.street
        self._orig_house = self.house

    def save(self, *args, **kwargs):
        self.clean_search_fields()
        super(Address, self).save(*args, **kwargs)

    def clean_search_fields(self):
        if self.country:
            self.country = self.country.strip()
        if self.region:
            self.region = self.region.strip()
        if self.city:
            self.city = self.city.strip()

    def changed(self):
        if self._orig_country != self.country or self._orig_region != self.region or self._orig_city != self.city or self._orig_street != self.street or self._orig_house != self.house:
            return True
        return False

    @property
    def minimum_address(self):
        return self.street or self.city or self.region or self.country or ''

    def any_dadata_exists(self):
        return any((self.country, self.region, self.city, self.zip_code, self.street, self.house, self.appartment))

    def get_address(self):
        items_list = [self.country, self.region, self.city, self.street, self.house]
        return ' '.join(str(item) for item in items_list if item)


class SmsConfirmation(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('Пользователь'))
    code = models.IntegerField(_('Код'))
    time_created = models.DateTimeField(_('Дата создания'), auto_now_add=True)
    time_accepted = models.DateTimeField(_('Дата подтверждения'), null=True)

    def __str__(self):
        return '{} - {}'.format(self.user, self.code)

    class Meta:
        verbose_name = _('Код подтверждения')
        verbose_name_plural = _('Коды подтверждения')

    @classmethod
    def get_valid_code(cls, code):
        return cls.objects.filter(code=code, time_accepted__isnull=True,
                                  time_created__gt=utc_now() - timedelta(days=1)).first()

    def generate_code(self, user):
        code = randint(1000, 9999)
        if SmsConfirmation.objects.filter(user=user, code=code).exists():
            return self.generate_code(user)
        else:
            return code

    @classmethod
    def add(cls, user):
        new_code = cls(user=user)
        new_code.save(force_insert=True)
        return new_code

    def save(self, force_insert=False, force_update=False, **kwargs):
        if not self.code:
            while True:
                self.code = self.generate_code(self.user)
                try:
                    super(SmsConfirmation, self).save(force_insert, force_update, **kwargs)
                except IntegrityError:
                    pass
                else:
                    break
        else:
            super(SmsConfirmation, self).save(force_insert, force_update, **kwargs)

    def accept(self):
        self.time_accepted = timezone.now()
        self.save()
        user = self.user
        user.is_active = True
        user.phone_confirmed = True
        user.save()


class FeedBack(models.Model):
    name = models.CharField(_('Имя'), max_length=255, help_text=_('Имя пользователя, отправившего отзыв'))
    contact = models.CharField(_('Контактная информация'), max_length=255,
                               help_text=_('Контактная информация пользователя, отправившего отзыв'))
    feedback_text = models.TextField(_('Текст отзыва'))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Отзыв создан')
    feedback_answer = models.TextField(_('Ответ'), blank=True, null=True)

    class Meta:
        verbose_name = _('Отзыв')
        verbose_name_plural = _('Отзывы')

    def __init__(self, *args, **kwargs):
        super(FeedBack, self).__init__(*args, **kwargs)
        self._orig_feedback_answer = self.feedback_answer

    def is_answer_added(self):
        return not self._orig_feedback_answer and self.feedback_answer

    def is_email_contact(self):
        return '@' in self.contact


@receiver(post_save, sender=FeedBack)
def send_sms_or_email_answer(sender, instance, created, **kwargs):
    from accounts.tasks import send_answer_by_email, send_answer_by_sms
    if instance.is_answer_added():
        if instance.is_email_contact():
            send_answer_by_email.delay(email=instance.contact, answer=instance.feedback_answer)
        else:
            send_answer_by_sms.delay(phone=instance.contact, answer=instance.feedback_answer)


@receiver(post_save, sender=Address)
def update_address_metro(sender, instance, created, **kwargs):
    from .tasks import update_address_metro

    if created:
        update_address_metro.apply_async(countdown=3, args=(instance.id,))
    elif instance.changed():
        update_address_metro.apply_async(countdown=3, args=(instance.id,))
