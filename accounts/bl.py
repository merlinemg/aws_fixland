from .models import Phone, Address, User
from masters.models import Master
from partners.models import Partner
from core import utils
from . import tasks


def create_master(form):
    user = create_user_for_master(form)
    Master.objects.create(
        user=user,
        registration_comment=form.cleaned_data.get('comment')
    )

    tasks.send_activation_email_task.delay(user.id)
    tasks.create_and_send_code_task.delay(user.id)
    tasks.new_master_email_to_admin_task.delay(user.id)
    return user


def create_partner(form):
    user = create_user_for_master(form)
    Partner.objects.create(
        user=user,
        registration_comment=form.cleaned_data.get('comment')
    )

    tasks.send_activation_email_task.delay(user.id)
    tasks.create_and_send_code_task.delay(user.id)
    tasks.new_partner_email_to_admin_task.delay(user.id)
    return user


def create_user_for_master(form):
    phone_number = utils.clean_phone(form.cleaned_data.get('phone'))
    phone = Phone.objects.filter(number=phone_number, phone_type=Phone.OWN_PHONE, user__master__isnull=True).first()
    if phone:
        user = phone.user
    else:
        user = User()
    user.username = form.cleaned_data.get('email')
    user.first_name = form.cleaned_data.get('first_name')
    user.last_name = form.cleaned_data.get('last_name')
    user.email = form.cleaned_data.get('email')
    user.gender = form.cleaned_data.get('gender')

    user.set_password(form.cleaned_data['password1'])
    user.save()
    if not phone:
        Phone.objects.create(
            user=user,
            number=phone_number
        )
    Address.objects.create(
        user=user,
        raw=form.cleaned_data.get('city'),
        city=form.cleaned_data.get('city')
    )
    return user
