import logging

from fixland.celery import celery_app
from django.core.exceptions import MultipleObjectsReturned
from core import utils as core_utils
from .models import User, Address, MetroLine, MetroStation, City, FeedBack
from . import email
from accounts.email import send_email_answer
from .utils import create_and_send_code
from core.utils import send_sms


log = logging.getLogger(__name__)


@celery_app.task
def send_activation_email_task(user_id):
    user = User.objects.get(id=user_id)
    email.send_activation_email(user)
    return user


@celery_app.task
def create_and_send_code_task(user_id):
    user = User.objects.get(id=user_id)
    create_and_send_code(user)


@celery_app.task
def new_master_email_to_admin_task(user_id):
    user = User.objects.get(id=user_id)
    email.new_master_email_to_admin(user)


@celery_app.task
def new_partner_email_to_admin_task(user_id):
    user = User.objects.get(id=user_id)
    email.new_partner_email_to_admin(user)


@celery_app.task(bind=True)
def update_address_metro(self, address_id):
    try:
        address = Address.objects.get(id=address_id)
        metro_data = core_utils.get_metro_by_address(address)
        if metro_data:
            city, created = City.objects.get_or_create(name=metro_data.get('city'))
            metro_line, created = MetroLine.objects.get_or_create(name=metro_data.get('line'), city=city)
            try:
                metro_station, created = MetroStation.objects.get_or_create(name__iexact=metro_data.get('name').lower(), line=metro_line)
                address.metro = metro_station
                address.save()
            except MultipleObjectsReturned:
                log.warning('Multiple metro station %s', metro_data.get('name'))
        update_address_coordinates.apply_async(countdown=3, args=(address_id, ))
    except Exception as exc:
        log.warning('Update Address Metro Exception')
        raise self.retry(exc=exc, countdown=10, max_retries=3)


@celery_app.task(bind=True)
def update_address_coordinates(self, address_id):
    try:
        address = Address.objects.get(id=address_id)
        coordinates = core_utils.get_coordinates(address.get_address())
        address.lat = coordinates.split(' ')[0]
        address.lon = coordinates.split(' ')[1]
        address.save()
    except Exception as exc:
        log.exception('Update Address Coordinates Exception')
        raise self.retry(exc=exc, countdown=10, max_retries=3)


@celery_app.task
def email_feedback_to_admin(feedback_id):
    feedback = FeedBack.objects.get(id=feedback_id)
    email.email_feedback_to_admin(feedback)


@celery_app.task
def send_answer_by_sms(phone, answer):
    send_sms(to=phone, text=answer)


@celery_app.task
def send_answer_by_email(email, answer):
    send_email_answer(email=email, answer=answer)
