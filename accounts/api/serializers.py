from rest_framework import serializers

from ..models import User, Region, City, MetroStation


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'email_confirmed', 'phone_confirmed', 'name', 'phone', 'gender', 'avatar', 'avatar_url')
        read_only_fields = ('id', 'email', 'email_confirmed', 'phone_confirmed', 'name', 'phone', 'gender', 'avatar')

    def get_name(self, obj):
        return obj.get_full_name()

    def get_phone(self, obj):
        return obj.get_phone_number() if self.context.get('request').user == obj else None


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ('id', 'region', 'name', 'is_metro_related')


class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Region
        fields = ('id', 'name', 'is_central_region')


class MetroStationSerializer(serializers.ModelSerializer):

    class Meta:
        model = MetroStation
        fields = ('id', 'name', 'line', 'line_color')
