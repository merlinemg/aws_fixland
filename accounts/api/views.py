import json
from rest_framework.exceptions import ValidationError
from rest_framework import generics
from rest_framework import status
from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from ..models import User, City, Region, MetroStation
from .serializers import UserSerializer, CitySerializer, RegionSerializer, MetroStationSerializer
from .permissions import UserPermissions
from ..forms import ResetPasswordForm


class ResetPasswordView(generics.CreateAPIView):
    serializer_class = None
    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    def create(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        form = ResetPasswordForm(data)
        if not form.is_valid():
            return Response(form.errors, status=status.HTTP_400_BAD_REQUEST)
        form.save()
        return Response({
            'success': True
        }, status=status.HTTP_200_OK)


class UserViewSet(mixins.UpdateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (UserPermissions,)
    renderer_classes = (JSONRenderer,)


class RegionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Region.objects
    serializer_class = RegionSerializer
    permission_classes = (AllowAny,)


class CityViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = City.objects
    serializer_class = CitySerializer
    permission_classes = (AllowAny,)

    def get_queryset(self, *args, **kwargs):
        region_ids = self.request.query_params.get('region_id')
        if region_ids is not None:
            if not region_ids:
                raise ValidationError({'errors': ['region_id is empty']})
            try:
                return self.queryset.filter(region_id=int(self.request.query_params.get('region_id')))
            except ValueError:
                raise ValidationError({'errors': ['region_id should be integer value']})
        return self.queryset.all()


class MetroStationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MetroStation.objects
    serializer_class = MetroStationSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self, *args, **kwargs):
        if self.request.query_params.get('city_id'):
            try:
                return self.queryset.filter(line__city_id__in=map(int, self.request.query_params.get('city_id').split(',')))
            except ValueError:
                raise ValidationError({'errors': ['region_id should be integer value']})
        return self.queryset.all()
