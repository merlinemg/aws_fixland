from django.db import models


class SoapLog(models.Model):
    TYPE_CONFIRM_ORDER = 'confirm_order'
    TYPE_CANCEL_ORDER = 'cancel_order'
    TYPE_CANCEL_ORDER_ITEM = 'cancel_order_item'
    TYPE_CALLBACK = 'callback'

    TYPES = (
        (TYPE_CONFIRM_ORDER, 'confirm order'),
        (TYPE_CANCEL_ORDER, 'cancel order'),
        (TYPE_CANCEL_ORDER_ITEM, 'cancel order item'),
        (TYPE_CALLBACK, 'callback'),
    )
    request = models.TextField(blank=True, null=True)
    response = models.TextField(blank=True, null=True)
    type = models.CharField(choices=TYPES, max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Soap log'

    @classmethod
    def create_request_log(cls, document, type):
        return cls.objects.create(request=document, type=type)
