import logging
import xmltodict

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseBadRequest

from devices.models import Vendor
from masters.models import PartOrderItem, MasterPart
from .models import SoapLog


log = logging.getLogger(__name__)


@csrf_exempt
def soap1s_callback(request):
    soap_log = SoapLog.create_request_log(request.body, SoapLog.TYPE_CALLBACK)
    log.warning('soap1s_callback, request.POST: %s', request.POST)
    try:
        data = request.POST['rwID']
        cleaned_data = data.replace('\\r', '').replace('\\n', '').replace('\\t', '')
        dict_data = xmltodict.parse(cleaned_data)
        api_key = dict_data['ФайлОбмена']['Ключи']['@Ключ']
        vendor = Vendor.objects.filter(integration_soap1s_api_key=api_key).first()
        if not vendor:
            soap_log.response = 'bad api key: {}'.format(api_key)
            log.warning('bad api key: %s', api_key)
            return HttpResponseBadRequest()
        orders = dict_data['ФайлОбмена']['Заказы']['Заказ']
        if not isinstance(orders, list):
            orders = [orders]
        set_order_statuses(vendor, orders)
        soap_log.response = 'success'
        soap_log.save()
        return HttpResponse('success')
    except Exception as e:
        log.exception('Soap 1S callback exception: %s', e)
        soap_log.response = 'Soap 1S callback exception: {}'.format(e)
        soap_log.save()
        return HttpResponseBadRequest()


def set_order_statuses(vendor, orders):
    for order in orders:
        part_order_items = PartOrderItem.objects.filter(
            order_number=order['@НомерЗаказа'],
            vendor_part__article=order['@Артикул'],
        )
        part_order_items.update(status=PartOrderItem.STATUS_RECEIVED)
        for part_order_item in part_order_items:
            status = MasterPart.STATUS_INSTOCK
            if part_order_item.for_order and part_order_item.for_order.is_done():
                status = MasterPart.STATUS_CHARGE_OFF
            MasterPart.objects.create(
                master=part_order_item.master,
                part=part_order_item.vendor_part.part,
                status=status
            )
