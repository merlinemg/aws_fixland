# Fixland.pro
Fixland.pro website development

The Fixland.ru is a marketplace designed to help clients to find and call for a serviceman in order to repair client's devices (currently - a smartphone, a tablet or a laptop). The serviceman comes to the place and at the time which the client has entered when leaving the order and fixes the issues of the client's devices.

Our philosophy is to make a business which doesn't need to be managed by many people, due to all major business processes are fully automated. Therefore, although the website looks pretty simple for clients, it is very complicated inside.

One of the core algorithms is calculation of the repairmen’s internal rating. It depends on different indicators, such as number of issues fixed, marks received from the clients, number of orders cancellations made by the repairman or the purchasing discipline, reflecting if the repairman is using the certified spare parts for his repairs or not. Based on the rating the servicemen have different statuses - bronze, silver or gold. Each status means different commission rates payable to the website by the servicemen. The higher is status, the less commission a serviceman pays to us.

Another important thing is the purchasing automation. The website “knows” which spare parts are required for every issue. Moreover, it is connected with accounting systems of parts suppliers, and this connection allows us to check the availability and prices for every spare part in all the outlets of all the suppliers connected to our website. Furthermore, when a serviceman selects where to buy the parts, he has to submit a purchasing order via our website. The purchasing order goes directly into the supplier’s accounting system, making the parts stock reserve. After all, the repairman just needs to go to the suppliers outlet and to collect the ordered parts and pay for them.
