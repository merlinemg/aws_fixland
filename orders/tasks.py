import logging
from fixland.celery import celery_app

from datetime import datetime, timedelta
from django.utils.translation import ugettext_lazy as _
from constance import config
from accounts.models import User
from masters.models import Master, MasterReview, PartOrderItem, MasterRatingLog
from core.utils import send_sms
from core.bl import parse_and_update_address
from core.models import Transaction
from devices.models import VendorPart
from . import email as email_utils
from . import sms
from . import utils
from .models import RepairOrder, SellOrder, RejectedOrder, Coupon, RepairOrderNotifiedMaster, RepairOrderNotExistsPart, RepairOrderItemPart

log = logging.getLogger(__name__)


@celery_app.task
def process_repair_order(order_id):
    order = RepairOrder.objects.get(id=order_id)
    sms.send_new_repair_order_sms(order)
    if order.coupon and order.discount and order.discount > order.get_max_master_order_commission():
        email_utils.discount_more_then_commision_to_admin(order)
    order_address = order.address
    if not order_address.region:
        parse_and_update_address(order_address)
    notify_masters_about_new_order.apply_async(countdown=config.NEW_REPAIR_ORDER_NOTIFY_DELAY * 60, args=(order_id, ))
    email_utils.new_repair_order_to_admin(order)


@celery_app.task
def process_sell_order(order_id):
    order = SellOrder.objects.get(id=order_id)
    sms.send_new_sell_order_sms(order)


@celery_app.task
def notify_master_about_new_repair_order(order_id, master_id=None, is_send_sms=False):
    order = RepairOrder.objects.get(id=order_id)
    if not master_id:
        if not order.master_id:
            return
        master = order.master
    else:
        master = Master.objects.get(id=master_id)
    phone_number = master.user.get_phone_number()
    if master.send_sms_repair_order_notification and phone_number and is_send_sms:
        text = _('Поступил новый заказ {}').format(order.get_absolute_url())
        send_sms(phone_number, text)
    if master.send_email_repair_order_notification and master.user.email:
        email_utils.new_repair_order_to_master(master.user.email, order)
    RepairOrderNotifiedMaster.objects.create(repair_order=order, master=master)


@celery_app.task
def notify_about_new_repair_order_if_not_selected(order_id, master_ids, after_minutes):
    order = RepairOrder.objects.get(id=order_id)
    if order.master:
        return
    masters = Master.objects.filter(id__in=master_ids)
    for master in masters:
        phone_number = master.user.get_phone_number()
        if phone_number:
            text = _('Поступил новый заказ {}').format(order.get_absolute_url())
            send_sms(phone_number, text)
        if master.user.email:
            email_utils.new_repair_order_to_master(master.user.email, order)
        RepairOrderNotifiedMaster.objects.create(repair_order=order, master=master)


@celery_app.task
def notify_admin_about_new_repair_order_if_not_selected(order_id, after_minutes):
    order = RepairOrder.objects.get(id=order_id)
    if order.master:
        return
    email_utils.new_repair_order_to_admin_if_not_selected(order, after_minutes)
    sms.send_new_repair_order_sms_admin_if_not_selected(order, after_minutes)


@celery_app.task
def master_reject_repair_order_to_admin(rejected_order_id):
    rejected_order = RejectedOrder.objects.get(id=rejected_order_id)
    email_utils.master_reject_repair_order_to_admin(rejected_order)


@celery_app.task
def send_sms_if_no_review(order_id, invite_link):
    log.warning('send_sms_if_no_review. order_id: %s, invite_link: %s', order_id, invite_link)
    order = RepairOrder.objects.get(id=order_id, )
    master_review = MasterReview.objects.filter(repair_order_id=order_id).first()
    if not master_review:
        log.warning('NO MASTER REVIEW!! send_sms_if_no_review. order_id: %s, invite_link: %s, number: %s', order_id, invite_link, order.user.get_phone_number())
        sms.send_order_review_invite_sms(order.user.get_phone_number(), order, invite_link)


@celery_app.task
def create_and_send_order_review_invite(order_id):
    order = RepairOrder.objects.get(id=order_id)
    if order.user.email:
        log.warning('scheduled email review: %s', order_id)
        invite_link = utils.create_order_review_invite_and_get_link(order)
        email_utils.repair_order_review_invite(order.user.email, order, invite_link)
        if config.SEND_SMS_IF_NO_RIVIEW and order.user.has_own_phone():
            log.warning('scheduled sms review if no review: %s', order_id)
            send_sms_if_no_review.apply_async(countdown=60 * config.SEND_SMS_IF_NO_RIVIEW_TIME, args=(order_id, invite_link))
    elif order.user.has_own_phone():
        invite_link = utils.create_order_review_invite_and_get_link(order)
        log.warning('scheduled sms review: %s, number: %s', order_id, order.user.get_phone_number())
        sms.send_order_review_invite_sms(order.user.get_phone_number(), order, invite_link)


@celery_app.task
def send_coupon(user_id, email, coupon_id, order_id):
    user = User.objects.get(id=user_id)
    coupon = Coupon.objects.filter(id=coupon_id).first()
    order = RepairOrder.objects.get(id=order_id)
    if coupon and email:
        email_utils.send_coupon(user, email, coupon, order)


@celery_app.task
def notify_masters_about_new_order(repair_order_id):
    from . import bl
    repair_order = RepairOrder.objects.get(id=repair_order_id)
    bl.notify_masters_about_new_order(repair_order)


@celery_app.task
def cancel_parts_orders_for_order(repair_order_id, master_id):
    from masters.bl import cancel_parts_orders
    repair_order = RepairOrder.objects.filter(id=repair_order_id).first()
    master = Master.objects.filter(id=master_id).first() if master_id else None
    if not repair_order or not master:
        return
    parts_orders = repair_order.parts_orders.select_related('vendor_part', 'vendor_part__vendor').filter(status=PartOrderItem.STATUS_CREATED, master=master)
    if parts_orders:
        cancel_parts_orders(parts_orders)
        email_utils.master_not_taken_parts_to_admin(master, repair_order, parts_orders)


@celery_app.task
def save_not_exists_order_parts(repair_order_id):
    repair_order = RepairOrder.objects.filter(id=repair_order_id).first()
    if not repair_order:
        return
    master_parts_list = repair_order.master.get_instock_parts().values_list('part_id', flat=True)
    order_parts_list = repair_order.get_order_parts().exclude(
        part_id__in=master_parts_list
    ).distinct('part_id').values_list('part_id', flat=True)
    not_exists_vendor_parts = []
    for part_id in order_parts_list:
        if not VendorPart.objects.filter(part_id__in=order_parts_list, exists=True).exists():
            not_exists_vendor_parts.append(part_id)
    for part_id in not_exists_vendor_parts:
        RepairOrderNotExistsPart.objects.get_or_create(part_id=part_id, repair_order_id=repair_order_id)
    RepairOrderNotExistsPart.objects.filter(repair_order_id=repair_order_id).exclude(part_id__in=not_exists_vendor_parts).delete()


@celery_app.task
def parts_control(repair_order_id, charge_off_parts):
    log.warning('parts_control task started order_id: %s', repair_order_id, extra={'charge_off_parts': charge_off_parts})
    repair_order = RepairOrder.objects.filter(id=repair_order_id).first()
    if not repair_order:
        return
    master = repair_order.master
    master_parts_list = (
        master
        .get_instock_parts()
        .distinct('part_id')
        .values_list('part_id', flat=True)
    )
    order_parts_list = (
        repair_order
        .get_order_parts()
        .distinct('part_id')
        .values_list('id', 'part_id')
    )
    not_exists_part = (
        repair_order
        .not_exists_part.all()
        .values_list('part_id', flat=True)
    )
    final_rating = 0
    penalty_order_parts = []
    for order_part_id, part_id in order_parts_list:
        if part_id not in charge_off_parts and part_id not in master_parts_list and part_id not in not_exists_part:
            if MasterRatingLog.objects.filter(
                    master=master,
                    rating_type=MasterRatingLog.RATING_PENALTY_PART,
                    amount=config.PENALTY_PART_RATING,
                    penalty_order_part_id=order_part_id,
                    created_at__gte=datetime.now()-timedelta(seconds=30),
                    created_at__lte=datetime.now()+timedelta(seconds=30)
            ).exists():
                log.warning(
                    'Master rating log already exists',
                    extra={
                        'master': master,
                        'rating_type': MasterRatingLog.RATING_PENALTY_PART,
                        'order_part_id': order_part_id,
                        'date': datetime.now()
                    }
                )
            else:
                MasterRatingLog.objects.create(
                    master_id=master.id,
                    rating_type=MasterRatingLog.RATING_PENALTY_PART,
                    amount=config.PENALTY_PART_RATING,
                    penalty_order_part_id=order_part_id,
                )
            final_rating += config.PENALTY_PART_RATING
            penalty_order_parts.append(order_part_id)
    if final_rating != 0:
        master.rating = master.rating + final_rating
        master.save()
    log.warning('parts_control finished', extra={
        'master_parts_list': master_parts_list,
        'order_parts_list': order_parts_list,
        'not_exists_part': not_exists_part,
        'penalty_order_parts': penalty_order_parts,
    })


@celery_app.task
def new_warranty_order_to_admin(order_id):
    repair_order = RepairOrder.objects.filter(id=order_id).first()
    if not repair_order:
        return
    email_utils.new_warranty_order_to_admin(repair_order)


@celery_app.task
def pay_warranty_order_compensation(order_id):
    repair_order = RepairOrder.objects.filter(id=order_id).first()
    if not repair_order:
        return
    master = repair_order.master
    items_price_without_commission = sum(i.price - i.get_master_commission(master, False) for i in repair_order.repair_order_items.all())
    order_parts = RepairOrderItemPart.objects.filter(repair_order_item__repair_order_id=repair_order.id)
    parts_price = sum(i.price for i in order_parts)
    total_amount = items_price_without_commission + parts_price
    Transaction.objects.create(
        recipient=master.user,
        type=Transaction.WARRANTY_ORDER_COMPENSATION,
        amount=total_amount,
        status=Transaction.SUCCESS,
        content_object=repair_order,
        description='Компенсация дохода мастера за выполнение гарантийного ремонта №{}'.format(repair_order.number),
    )


@celery_app.task
def email_about_not_finished_order_to_admin(order_data):
    email_utils.email_about_not_finished_order_to_admin(order_data)
