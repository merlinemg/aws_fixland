# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import orders.utils


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0018_auto_20150817_1715'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coupon',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('code', models.CharField(default=orders.utils.generate_coupon_code, max_length=255)),
                ('discount', models.IntegerField(verbose_name='Скидка')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('time_accepted', models.DateTimeField(null=True, verbose_name='Date accepted', editable=False)),
                ('expired', models.BooleanField(verbose_name='Expired', default=False)),
            ],
            options={
                'verbose_name_plural': 'Купоны на скидку',
                'verbose_name': 'Купон на скидку',
            },
        ),
    ]
