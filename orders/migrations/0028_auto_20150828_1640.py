# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0027_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='coupon',
            name='time_accepted',
        ),
        migrations.AddField(
            model_name='coupon',
            name='type',
            field=models.CharField(verbose_name='Тип', choices=[('regular', 'Обычный'), ('repeated_order', 'Повторный заказ')], default='regular', max_length=255),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='coupon',
            field=models.ForeignKey(verbose_name='Купон', blank=True, related_name='orders', to='orders.Coupon', null=True),
        ),
    ]
