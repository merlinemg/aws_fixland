# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0017_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairorder',
            name='address',
            field=models.ForeignKey(to='accounts.Address', null=True, verbose_name='Адресс', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, verbose_name='Пользователь', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='sellorder',
            name='address',
            field=models.ForeignKey(to='accounts.Address', null=True, verbose_name='Адресс', on_delete=django.db.models.deletion.SET_NULL),
        ),
        migrations.AlterField(
            model_name='sellorder',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, verbose_name='Пользователь', on_delete=django.db.models.deletion.SET_NULL),
        ),
    ]
