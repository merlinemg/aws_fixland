# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0002_add_courses'),
        ('orders', '0004_merge'),
    ]

    operations = [
        # migrations.AddField(
        #     model_name='repairorder',
        #     name='master',
        #     field=models.ForeignKey(to='masters.Master', null=True, blank=True, verbose_name='Мастер'),
        # ),
        # migrations.AddField(
        #     model_name='repairorder',
        #     name='status',
        #     field=models.CharField(default='new', choices=[('new', 'Новый'), ('done', 'Выполнен')], max_length=255, verbose_name='Статус'),
        # ),
    ]
