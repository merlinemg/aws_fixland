# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0010_auto_20150616_1232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairorder',
            name='status',
            field=models.CharField(max_length=255, default='new', choices=[('new', 'Новый'), ('done', 'Выполнен'), ('cancelled', 'Отменён')], verbose_name='Статус'),
        ),
    ]
