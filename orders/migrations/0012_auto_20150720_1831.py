# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_auto_20150626_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='sellorder',
            name='status',
            field=models.CharField(choices=[('new', 'Новый'), ('done', 'Выполнен'), ('cancelled', 'Отменён')], max_length=255, default='new', verbose_name='Статус'),
        ),
    ]
