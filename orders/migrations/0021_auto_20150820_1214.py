# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0023_auto_20150814_1157'),
        ('orders', '0020_auto_20150819_1343'),
    ]

    operations = [
        migrations.CreateModel(
            name='RepairOrderNotifiedMaster',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('master', models.ForeignKey(related_name='notified_orders', to='masters.Master', verbose_name='Мастер')),
            ],
            options={
                'verbose_name_plural': 'Оповещены мастера',
                'verbose_name': 'Оповещен мастер',
            },
        ),
        migrations.AddField(
            model_name='repairordernotifiedmaster',
            name='repair_order',
            field=models.ForeignKey(related_name='notified_masters', to='orders.RepairOrder', verbose_name='Заказ'),
        ),
    ]
