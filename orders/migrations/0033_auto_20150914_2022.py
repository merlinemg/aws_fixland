# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0031_auto_20150911_1433'),
        ('orders', '0032_auto_20150909_0911'),
    ]

    operations = [
        migrations.CreateModel(
            name='RepairOrderItemPart',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('price', models.DecimalField(verbose_name='Цена', decimal_places=2, max_digits=14)),
                ('part', models.ForeignKey(verbose_name='Запчасть', to='devices.Part')),
                ('repair_order_item', models.ForeignKey(to='orders.RepairOrderItem')),
            ],
            options={
                'verbose_name': 'Запчасть єлемента заказа',
                'verbose_name_plural': 'Запчасти єлементов заказа',
            },
        ),
    ]
