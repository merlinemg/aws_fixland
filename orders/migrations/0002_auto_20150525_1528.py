# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0006_aded_text_color_field_to_devices'),
        ('accounts', '0006_auto_20150520_1318'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='repairorder',
            options={'verbose_name': 'Заказ ремонта', 'verbose_name_plural': 'Заказы ремонта'},
        ),
        migrations.AlterModelOptions(
            name='repairorderitem',
            options={'verbose_name': 'Элемент заказа', 'verbose_name_plural': 'Элементы заказа'},
        ),
        migrations.RemoveField(
            model_name='repairorder',
            name='ip_address',
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='address',
            field=models.ForeignKey(verbose_name='Адресс', to='accounts.Address'),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='as_soon_as_possible',
            field=models.BooleanField(verbose_name='Как можно быстрее', default=False),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='created_at',
            field=models.DateTimeField(verbose_name='Заказ создан', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='device_model_color',
            field=models.ForeignKey(verbose_name='Модель устройства', to='devices.DeviceModelColor'),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='endtime',
            field=models.DateTimeField(null=True, verbose_name='Удобное время до', blank=True),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='starttime',
            field=models.DateTimeField(null=True, verbose_name='Удобное время от', blank=True),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='updated_at',
            field=models.DateTimeField(verbose_name='Заказ обновлен', auto_now=True),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='user',
            field=models.ForeignKey(verbose_name='Пользователь', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='repairorderitem',
            name='device_model_repair',
            field=models.ForeignKey(verbose_name='Поломка', to='devices.DeviceModelRepair'),
        ),
        migrations.AlterField(
            model_name='repairorderitem',
            name='price',
            field=models.DecimalField(verbose_name='Цена', max_digits=14, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='repairorderitem',
            name='repair_order',
            field=models.ForeignKey(verbose_name='Заказ', to='orders.RepairOrder'),
        ),
    ]
