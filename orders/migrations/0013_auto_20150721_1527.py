# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('masters', '0017_merge'),
        ('orders', '0012_auto_20150720_1831'),
    ]

    operations = [
        migrations.CreateModel(
            name='RejectedOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('comment', models.CharField(verbose_name='Комментарий', max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Cоздан')),
                ('updated_at', models.DateTimeField(verbose_name='Обновлен', auto_now=True)),
                ('master', models.ForeignKey(to='masters.Master', related_name='rejected_orders', verbose_name='Мастер')),
            ],
            options={
                'verbose_name_plural': 'Отказы от заказов',
                'verbose_name': 'Отказ от заказа',
            },
        ),
        migrations.AlterModelOptions(
            name='repairorder',
            options={'verbose_name_plural': 'Заказы ремонта', 'verbose_name': 'Заказ ремонта', 'ordering': ('-pk',)},
        ),
        migrations.AlterModelOptions(
            name='sellorder',
            options={'verbose_name_plural': 'Заказы продажи', 'verbose_name': 'Заказ продажи', 'ordering': ('-pk',)},
        ),
        migrations.AddField(
            model_name='rejectedorder',
            name='repair_order',
            field=models.ForeignKey(to='orders.RepairOrder', related_name='rejected', verbose_name='Заказ ремонта'),
        ),
    ]
