# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0040_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='is_warranty',
            field=models.BooleanField(verbose_name='Гарантийный', default=False),
        ),
        migrations.AddField(
            model_name='repairorder',
            name='parent',
            field=models.ForeignKey(verbose_name='Родительский', null=True, to='orders.RepairOrder', blank=True),
        ),
        migrations.AlterField(
            model_name='repairorder',
            name='status',
            field=models.CharField(verbose_name='Статус', choices=[('new', 'Новый'), ('done', 'Выполнен'), ('cancelled', 'Отменён'), ('processing', 'В обработке'), ('void', 'Void')], max_length=255, default='new'),
        ),
    ]
