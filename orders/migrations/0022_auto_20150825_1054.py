# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import orders.utils


def update_codes(apps, schema_editor):
    OrderReviewInvite = apps.get_model("orders", "OrderReviewInvite")
    for invite in OrderReviewInvite.objects.all():
        invite.code = invite.invite.code
        invite.expired = invite.invite.expired
        invite.time_accepted = invite.invite.time_accepted
        invite.save()


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0021_auto_20150820_1214'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderreviewinvite',
            name='code',
            field=models.CharField(verbose_name='Инвайт код', max_length=255, default=''),
        ),
        migrations.AddField(
            model_name='orderreviewinvite',
            name='expired',
            field=models.BooleanField(db_index=True, verbose_name='Expired', default=False),
        ),
        migrations.AddField(
            model_name='orderreviewinvite',
            name='time_accepted',
            field=models.DateTimeField(null=True, verbose_name='Date accepted', editable=False),
        ),
        migrations.RunPython(update_codes),
    ]
