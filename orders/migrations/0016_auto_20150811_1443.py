# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('sitegate', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0015_auto_20150807_1145'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderReviewInvite',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('invite', models.ForeignKey(verbose_name='Инвайт', to='sitegate.InvitationCode')),
            ],
            options={
                'verbose_name_plural': 'Инвайты на отзыв за работу мастера',
                'verbose_name': 'Инвайт на отзыв за работу мастера',
            },
        ),
        migrations.AddField(
            model_name='orderreviewinvite',
            name='repair_order',
            field=models.ForeignKey(verbose_name='Заказ ремонта', to='orders.RepairOrder'),
        ),
    ]
