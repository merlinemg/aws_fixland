# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import orders.utils


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0022_auto_20150825_1054'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderreviewinvite',
            name='invite',
        ),
        migrations.AlterField(
            model_name='orderreviewinvite',
            name='code',
            field=models.CharField(default=orders.utils.generate_review_code_code, max_length=255, verbose_name='Инвайт код'),
        ),
    ]
