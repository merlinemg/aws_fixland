# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0013_auto_20150721_1527'),
        ('orders', '0013_merge'),
    ]

    operations = [
    ]
