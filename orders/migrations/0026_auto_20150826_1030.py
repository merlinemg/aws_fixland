# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import orders.utils


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0025_auto_20150826_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairorder',
            name='number',
            field=models.CharField(db_index=True, verbose_name='Номер заказа', max_length=255, unique=True, default=orders.utils.generate_repair_order_number),
        ),
    ]
