# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0001_initial'),
        ('orders', '0035_auto_20150914_2042'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairorder',
            name='partner',
            field=models.ForeignKey(verbose_name='Партнер', null=True, blank=True, to='partners.Partner'),
        ),
    ]
