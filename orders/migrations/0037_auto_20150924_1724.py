# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0035_auto_20150924_1644'),
        ('orders', '0036_repairorder_partner'),
    ]

    operations = [
        migrations.CreateModel(
            name='RepairOrderNotExistsPart',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('part', models.ForeignKey(verbose_name='Запчасть', to='devices.Part')),
            ],
        ),
        migrations.AddField(
            model_name='repairordernotexistspart',
            name='repair_order',
            field=models.ForeignKey(to='orders.RepairOrder', verbose_name='Заказ', related_name='not_exists_part'),
        ),
    ]
