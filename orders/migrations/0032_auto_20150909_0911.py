# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0031_auto_20150907_1112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairorder',
            name='status',
            field=models.CharField(choices=[('new', 'Новый'), ('done', 'Выполнен'), ('cancelled', 'Отменён'), ('void', 'Void')], verbose_name='Статус', default='new', max_length=255),
        ),
        migrations.AlterField(
            model_name='sellorder',
            name='status',
            field=models.CharField(choices=[('new', 'Новый'), ('done', 'Выполнен'), ('cancelled', 'Отменён'), ('void', 'Void')], verbose_name='Статус', default='new', max_length=255),
        ),
    ]
