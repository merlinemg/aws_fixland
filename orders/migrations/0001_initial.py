# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0005_auto_20150501_1021'),
        ('accounts', '0003_address_phone'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='RepairOrder',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('starttime', models.DateTimeField(blank=True, null=True)),
                ('endtime', models.DateTimeField(blank=True, null=True)),
                ('as_soon_as_possible', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('address', models.ForeignKey(to='accounts.Address')),
                ('device_model_color', models.ForeignKey(to='devices.DeviceModelColor')),
            ],
        ),
        migrations.CreateModel(
            name='RepairOrderItem',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=14)),
                ('device_model_repair', models.ForeignKey(to='devices.DeviceModelRepair')),
                ('repair_order', models.ForeignKey(to='orders.RepairOrder')),
            ],
            options={
                'verbose_name': 'Заказ ремонта',
                'verbose_name_plural': 'Заказы ремонта',
            },
        ),
        migrations.AddField(
            model_name='repairorder',
            name='device_model_repair',
            field=models.ManyToManyField(through='orders.RepairOrderItem', to='devices.DeviceModelRepair'),
        ),
        migrations.AddField(
            model_name='repairorder',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
