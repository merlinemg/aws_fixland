# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairorderitem',
            name='repair_order',
            field=models.ForeignKey(verbose_name='Заказ', related_name='repair_order_items', to='orders.RepairOrder'),
        ),
    ]
