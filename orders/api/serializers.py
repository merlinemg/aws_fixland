from itertools import chain
from rest_framework import serializers

from accounts.models import Address, User, MetroStation
from devices.api.serializers import PartSerializer
from devices.models import DeviceModelRepair
from ..models import RepairOrder, RepairOrderItem, RepairOrderItemPart
from masters.rating.calculation import get_score_for_slug
from masters.bl import is_master_have_pars_for_order
from orders.tasks import save_not_exists_order_parts


class MetroSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetroStation
        fields = ('name', 'line_display', 'line_color')


class AddressSerializer(serializers.ModelSerializer):
    metro = MetroSerializer(many=False, read_only=True)

    class Meta:
        model = Address
        fields = ('minimum_address', 'raw', 'metro', 'lat', 'lon')


class OnlyMetroAddressSerializer(serializers.ModelSerializer):
    metro = MetroSerializer(many=False, read_only=True)

    class Meta:
        model = Address
        fields = ('metro', )


class RepairOrderItemSerializer(serializers.ModelSerializer):
    device_model_repair = serializers.SerializerMethodField()
    commission = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = RepairOrderItem
        fields = ('price', 'device_model_repair', 'commission')

    def get_price(self, obj):
        if obj.repair_order.is_warranty:
            return obj.price - obj.get_master_commission(self.context.get('request').user.master, False)
        return obj.price

    def get_device_model_repair(self, obj):
        if obj.device_model_repair.repair:
            return obj.device_model_repair.repair.name
        return ' + '.join(obj.device_model_repair.repairs.all().values_list('name', flat=True))

    def get_commission(self, obj):
        return obj.get_master_commission(self.context.get('request').user.master)


class OrderUserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('name', 'phone')

    def get_name(self, obj):
        return obj.get_full_name()

    def get_phone(self, obj):
        return obj.get_phone_number()


class RepairOrderItemPartSerializer(serializers.ModelSerializer):
    part = PartSerializer(many=False)

    class Meta:
        model = RepairOrderItemPart
        fields = ('quantity', 'part', 'price')


class RepairOrderSerializer(serializers.ModelSerializer):
    device_model_color = serializers.SerializerMethodField()
    repair_order_items = RepairOrderItemSerializer(many=True, read_only=True)
    parts = serializers.SerializerMethodField()
    device_photo = serializers.SerializerMethodField()
    model = serializers.SerializerMethodField()
    device = serializers.SerializerMethodField()
    rejected_data = serializers.SerializerMethodField()
    selected = serializers.SerializerMethodField()
    master = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    metro = serializers.SerializerMethodField()
    need_update_vendor_parts = serializers.SerializerMethodField()
    address = AddressSerializer(many=False, read_only=True)

    class Meta:
        model = RepairOrder
        ordering = ('-id', )
        fields = ('number', 'status', 'order_name', 'device_model_color', 'device_photo', 'repair_order_items', 'address', 'master', 'starttime', 'endtime', 'as_soon_as_possible', 'created_at', 'updated_at', 'total_amount', 'parts', 'model', 'device', 'discount', 'selected', 'is_rejected', 'rejected_data', 'comment', 'rating', 'metro', 'need_update_vendor_parts', 'total_amount_with_discount', 'is_warranty')
        lookup_field = 'number'

    def get_metro(self, obj):
        if obj.address and obj.address.metro:
            return MetroSerializer(obj.address.metro, many=False).data
        return None

    def get_rejected_data(self, obj):
        return obj.rejected.values('comment')

    def get_selected(self, obj):
        return True if obj.master else False

    def get_master(self, obj):
        if obj.master and obj.master == self.context.get('request').user.master:
            return obj.master_id
        else:
            return None

    def get_device_model_color(self, obj):
        return obj.device_model_color.__str__()

    def get_device_photo(self, obj):
        return obj.device_model_color.photo_url

    def get_parts(self, obj):
        return RepairOrderItemPartSerializer(obj.get_order_parts(), many=True).data

    def get_model(self, obj):
        return obj.device_model_color.device_model.id

    def get_device(self, obj):
        return obj.device_model_color.device_model.device.id

    def get_rating(self, obj):
        breakdowns = obj.device_model_repair.count()
        score = get_score_for_slug(obj.device_model_color.device_model.slug) * breakdowns
        return score

    def get_need_update_vendor_parts(self, obj):
        if obj.show_user_contacts:
            return False
        master = self.context.get('request').user.master
        if obj.show_user_contacts:
            return False
        if obj.master and obj.master == master and is_master_have_pars_for_order(master, obj, True):
            return True
        return False


class RepairOrderForUserSerializer(serializers.ModelSerializer):
    repairs = serializers.SerializerMethodField()
    model = serializers.SerializerMethodField()
    device = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()

    class Meta:
        model = RepairOrder
        ordering = ('-id', )
        fields = ('number', 'order_name', 'repairs', 'address', 'model', 'device', 'color', 'completed_at')

    def get_address(self, obj):
        return obj.address.raw if obj.address else None

    def repair_order_items(self, obj):
        return obj.device_model_repair.values_list('reapir_id', flat=True)

    def get_model(self, obj):
        return obj.device_model_color.device_model.slug

    def get_device(self, obj):
        return obj.device_model_color.device_model.device.slug

    def get_color(self, obj):
        return obj.device_model_color.device_color.slug

    def get_repairs(self, obj):
        repairs = obj.device_model_repair.filter(repair__isnull=False).values_list('id', flat=True)
        grouped_repairs = obj.device_model_repair.filter(repairs__isnull=False).values_list('repairs__id', flat=True)
        grouped_model_repairs = (
            DeviceModelRepair.objects
            .filter(
                device_model=obj.device_model_color.device_model,
                repair_id__in=grouped_repairs
            ).values_list('id', flat=True)
        )
        obj.device_model_repair.filter(repairs__isnull=False).values_list('id', flat=True),
        return list(chain(repairs, grouped_model_repairs))


class RepairOrderUserSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    address = AddressSerializer(many=False)

    class Meta:
        model = RepairOrder
        fields = ('number', 'user', 'address')

    def _can_see_order_user_data(self, obj):
        if obj.show_user_contacts:
            return True
        if getattr(self, '_can_see_order_user_data_cache', None) is None:
            master = self.context.get('request').user.master
            self._can_see_order_user_data_cache = False
            if obj.master and obj.master == master and is_master_have_pars_for_order(master, obj):
                self._can_see_order_user_data_cache = True
                obj.show_user_contacts = True
                obj.save()
                save_not_exists_order_parts.delay(obj.id)
        return self._can_see_order_user_data_cache

    def get_user(self, obj):
        if self._can_see_order_user_data(obj):
            return OrderUserSerializer(obj.user).data
        return None


class CompleteRepairOrderSerializer(RepairOrderSerializer):
    read_only_fields = ('id', 'status', 'order_name', 'device_model_color', 'repair_order_items', 'user', 'address', 'master', 'starttime', 'endtime', 'as_soon_as_possible', 'created_at', 'updated_at', 'total_amount', 'parts')
