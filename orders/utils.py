import uuid
import random
from django.core.urlresolvers import reverse


def create_order_review_invite_and_get_link(order):
    from .models import OrderReviewInvite
    invite = OrderReviewInvite.objects.create(
        repair_order=order
    )
    return reverse('order_review', args=(invite.code,))


def generate_coupon_code():
    from .models import Coupon
    code = str(uuid.uuid4())[:6]
    obj = Coupon.objects.filter(code=code)
    if obj:
        return generate_review_code_code()
    return code


def generate_review_code_code():
    from .models import OrderReviewInvite
    code = str(uuid.uuid4())[:5]
    obj = OrderReviewInvite.objects.filter(code=code).first()
    if obj:
        return generate_review_code_code()
    return code


def generate_repair_order_number():
    from .models import RepairOrder
    while True:
        number = '{}-{}'.format(random.randint(1000, 9999), random.randint(1000, 9999))
        if not RepairOrder.objects.filter(number=number).exists():
            return number
