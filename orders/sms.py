import logging

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from core.utils import send_sms

log = logging.getLogger(__name__)


def send_sms_to_admin(text):
    for receiver in settings.SEND_SMS_TO:
        send_sms(receiver, text)


def send_new_repair_order_sms(order):
    text = _('Поступил заказ на ремонт №{order_id}').format(order_id=order.number)
    send_sms_to_admin(text)


def send_new_repair_order_sms_admin_if_not_selected(order, after_minutes):
    text = _('Заказ №{order_id} не забрали. {after_minutes} мин').format(order_id=order.number, after_minutes=after_minutes)
    send_sms_to_admin(text)


def send_new_sell_order_sms(order):
    text = _('Поступил заказ на продажу №{order_id}').format(order_id=order.id)
    send_sms_to_admin(text)


def send_order_review_invite_sms(phone, order, link):
    link = 'http://{}{}'.format(settings.HOST_URL, link)
    text = _('Здравствуйте, {user_name}. Просьба оценить работу мастера Fixland.ru и получить купон на скидку за 30 сек: {link}').format(
        user_name=order.user.get_full_name(),
        link=link
    )
    log.warning('send_order_review_invite_sms. phone: %s, text: %s', phone, text)
    send_sms(phone, text)
