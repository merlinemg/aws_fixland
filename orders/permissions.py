from .models import RepairOrder, Coupon
from masters.bl import is_master_have_pars_for_order


def can_reject_order(order, master):
    return order.status == RepairOrder.STATUS_NEW and order.master and order.master == master


def can_use_coupon(coupon, order):
    if coupon.type == Coupon.REPEATED_ORDER_TYPE:
        return True
    repairs = order.device_model_repair.values('repair_id')
    model_slug = order.device_model_color.device_model.slug
    return coupon.is_valid() and can_use_coupon_by_order_data(coupon, repairs, model_slug)


def can_use_coupon_by_order_data(coupon, repairs, model_slug):
    if not coupon.device_models.exists() and not coupon.repairs.exists():
        return True
    if coupon.device_models.exists() and not coupon.device_models.filter(slug=model_slug).exists():
        return False
    if coupon.repairs.exists() and not coupon.repairs.filter(id__in=repairs).exists():
        return False
    return True


def can_see_order_user_data(repair_order, master):
    return repair_order.master and repair_order.master == master and is_master_have_pars_for_order(master, repair_order)
