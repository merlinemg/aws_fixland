from django.test import TestCase

from .. import bl


class SaveOrderTestCase(TestCase):
    good_data = {'userEmail': 'taraslyapun@gmail.com', 'isSoonAsPossible': True, 'selectedRepairs': [{'slug': 'battery', 'name': 'Батарея', 'price': 500, 'key': 2}, {'slug': 'slug', 'name': 'Экран', 'price': 1000, 'key': 1}], 'userPhone': '1312312', 'userName': 'Тарас', 'userAddress': 'asdsadas'}

    # Taras: Broken test
    # def test_save_good_data_for_new_user(self):
    #     bl.save_order(self.good_data)
