from django.test import TestCase

from accounts.models import User
from masters.models import Master
from masters.factories import MasterFactory, create_master_statuses
from .. import factories as order_factories


class CouponTestCase(TestCase):
    def setUp(self):
        self.master_password = User.objects.make_random_password()
        master = MasterFactory()
        master_user = User.objects.get(id=master.user.id)
        master_user.set_password(self.master_password)
        master_user.save()

        self.master = Master.objects.get(id=master.id)
        self.master.rating = 600
        self.master.save()

        create_master_statuses(self.master)
        order_factories.create_order(self.master)

    def test_coupon(self):
        pass
