from django.test import TestCase

from accounts.factories import AddressFactory
from accounts.models import Address
from core.bl import parse_and_update_address


class TestParseAddressCase(TestCase):

    def test_parse_and_update_address(self):
        address = AddressFactory(raw='г Москва, ул Островитянова, д 4, кв 36')
        address = Address.objects.get(id=address.id)
        parse_and_update_address(address)
        address = Address.objects.get(id=address.id)
        self.assertEqual(address.region, 'Moscow')
        self.assertEqual(address.city, 'Moskva')
        self.assertEqual(address.zip_code, '117513')
        self.assertEqual(
            address.street, 'Yugo-Zapadnyy administrativnyy okrug')
        self.assertEqual(address.house, '')
        self.assertEqual(address.appartment, '')
