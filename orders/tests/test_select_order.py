import json
from decimal import Decimal
from django.test import TestCase
from django.core.urlresolvers import reverse

from accounts.factories import UserFactory
from accounts.models import User
from core.factories import TransactionFactory
from masters.models import Master
from masters.factories import MasterFactory, create_master_statuses
from .. import factories as order_factories
from devices import factories as devices_factories
from ..models import RepairOrder


class SelectOrderTestCase(TestCase):
    def setUp(self):
        self.user_password = User.objects.make_random_password()
        self.master_password = User.objects.make_random_password()
        self.user = UserFactory()
        self.user.set_password(self.user_password)
        self.user.save()
        master = MasterFactory()
        master_user = User.objects.get(id=master.user.id)
        master_user.set_password(self.master_password)
        master_user.save()

        self.master = Master.objects.get(id=master.id)
        self.master.rating = 600
        self.master.save()

        create_master_statuses(self.master)
        self.order = order_factories.RepairOrderFactory()

        dmr1 = devices_factories.DeviceModelRepairFactory(
            device_model=self.order.device_model_color.device_model,
            price=Decimal(1001)
        )
        dmr2 = devices_factories.DeviceModelRepairFactory(
            device_model=self.order.device_model_color.device_model,
            price=Decimal(2002)
        )

        devices_factories.DeviceModelRepairCommissionFactory(
            model_repair=dmr1,
            status=self.master.get_status()
        )
        devices_factories.DeviceModelRepairCommissionFactory(
            model_repair=dmr2,
            status=self.master.get_status()
        )

        order_factories.RepairOrderItemFactory(repair_order=self.order, device_model_repair=dmr1)
        order_factories.RepairOrderItemFactory(repair_order=self.order, device_model_repair=dmr2)

    def test_select_order_by_user_no_master(self):
        self.client.login(username=self.user.username, password=self.user_password)
        response = self.client.get(reverse('select_repair_order', kwargs={'order_id': self.order.number}))
        self.assertEqual(response.status_code, 403)

    def test_select_order_with_master(self):
        self.client.login(username=self.master.user.username, password=self.master_password)
        order = RepairOrder.objects.get(id=self.order.id)
        order.master = MasterFactory()
        order.save()
        response = self.client.get(reverse('select_repair_order', kwargs={'order_id': self.order.number}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('result'), 'order_error')

    def test_select_order_master_no_money_should_give_credit(self):
        self.client.login(username=self.master.user.username, password=self.master_password)
        response = self.client.get(reverse('select_repair_order', kwargs={'order_id': self.order.number}))
        master = Master.objects.get(id=self.master.id)
        self.assertEqual(master.used_credit_amount, self.order.get_master_commission(self.master))
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('result'), 'ok')

    def test_select_order_shoud_return_result_ok(self):
        master_start_balanse = 1000
        TransactionFactory(recipient=self.master.user, amount=master_start_balanse)
        self.client.login(username=self.master.user.username, password=self.master_password)
        response = self.client.get(reverse('select_repair_order', kwargs={'order_id': self.order.number}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode('utf-8')).get('result'), 'ok')
        master = Master.objects.get(id=self.master.id)
        self.assertEqual(master.balance, master_start_balanse - self.order.get_master_commission(master))
