from django.test import TestCase
from django.db.models.signals import post_save

from accounts import factories as accounts_factories
from masters.factories import MasterPlaceFactory, create_master_statuses
from orders import factories as order_factories
from orders.bl import get_master_orders, get_order_masters
from orders.models import RepairOrder, register_master_to_update_rating_if_needed, order_post_save_signal_handler


def disconnect_signals():
    post_save.disconnect(register_master_to_update_rating_if_needed, sender=RepairOrder)
    post_save.disconnect(order_post_save_signal_handler, sender=RepairOrder)


class MasterOrdersTestCase(TestCase):
    def setUp(self):
        disconnect_signals()
        self.metro_station = accounts_factories.MetroStationFactory()
        metro_station2 = accounts_factories.MetroStationFactory()
        self.master_place = MasterPlaceFactory(
            region=self.metro_station.line.city.region,
            city=self.metro_station.line.city,
        )
        MasterPlaceFactory(
            master=self.master_place.master,
            region=metro_station2.line.city.region,
            city=metro_station2.line.city,
        )
        self.master = self.master_place.master
        self.master.rating = 600
        self.master.save()
        create_master_statuses(self.master)
        self.order = order_factories.create_order(self.master)
        order_address = self.order.address
        order_address.region = self.metro_station.line.city.region.name
        order_address.city = self.metro_station.line.city.name
        order_address.save()
        order_factories.RepairOrderNotifiedMasterFactory(repair_order=self.order, master=self.master)
        self.order_address = accounts_factories.AddressFactory(
            region=self.master_place.region.name,
            city=self.master_place.city.name,
            metro=self.metro_station
        )
        self.order.address = self.order_address
        self.order.save()

        self.master.certified_models.add(self.order.device_model_color.device_model)
        self.master.save()

    def test_master_address_equal_master_place(self):
        self.master_place.metro_stations.add(self.metro_station)
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 1)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 1)

    def test_by_master_city(self):
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 1)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 1)

    def test_master_whith_another_metro_should_return_empty_array(self):
        self.master_place.metro_stations.add(accounts_factories.MetroStationFactory())
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 0)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 0)

    def test_order_with_another_metro_should_return_empty_array(self):
        self.master_place.metro_stations.add(self.metro_station)
        self.order_address.metro = accounts_factories.MetroStationFactory()
        self.order_address.save()
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 0)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 0)

    def test_when_order_without_metro_master_has_metro(self):
        self.master_place.metro_stations.add(self.metro_station)
        self.order_address.metro = None
        self.order_address.save()
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 0)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 0)

    def test_when_order_and_master_without_metro(self):
        self.order_address.metro = None
        self.order_address.save()
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 1)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 1)

    def test_by_region(self):
        self.master_place.city = None
        self.master_place.save()
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 1)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 1)

    def test_bad_master_region_should_return_empty_array(self):
        self.master_place.city = None
        self.master_place.region = accounts_factories.RegionFactory()
        self.master_place.save()
        orders = get_master_orders('new', self.master)
        self.assertEqual(len(orders), 0)
        masters = get_order_masters(self.order)
        self.assertEqual(len(masters), 0)

    def tearDown(self):
        post_save.connect(register_master_to_update_rating_if_needed, sender=RepairOrder)
        post_save.connect(order_post_save_signal_handler, sender=RepairOrder)
