# coding: utf-8
import logging
from decimal import Decimal
from django.db import models
from django.db.models import Q, Sum

from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now as utc_now

from . import email
from . import utils
from accounts.models import Region
from masters.models import MasterStatus
from core.models import Transaction

log = logging.getLogger(__name__)


class RepairOrder(models.Model):
    STATUS_NEW = 'new'
    STATUS_DONE = 'done'
    STATUS_CANCELLED = 'cancelled'
    STATUS_VOID = 'void'
    STATUS_PROCESSING = 'processing'

    STATUS_CHOICES = (
        (STATUS_NEW, _('Новый')),
        (STATUS_DONE, _('Выполнен')),
        (STATUS_CANCELLED, _('Отменён')),
        (STATUS_PROCESSING, _('В обработке')),
        (STATUS_VOID, _('Void')),
    )

    number = models.CharField(_('Номер заказа'), unique=True, db_index=True, default=utils.generate_repair_order_number, max_length=255)
    device_model_color = models.ForeignKey('devices.DeviceModelColor', verbose_name=_('Модель устройства'))
    device_model_repair = models.ManyToManyField('devices.DeviceModelRepair', through='RepairOrderItem', through_fields=('repair_order', 'device_model_repair'))
    user = models.ForeignKey('accounts.User', verbose_name=_('Пользователь'), null=True, on_delete=models.SET_NULL)
    address = models.ForeignKey('accounts.Address', verbose_name=_('Адресс'), null=True, on_delete=models.SET_NULL)
    master = models.ForeignKey('masters.Master', verbose_name=_('Мастер'), blank=True, null=True)
    status = models.CharField(_('Статус'), max_length=255, default=STATUS_NEW, choices=STATUS_CHOICES)
    coupon = models.ForeignKey('orders.Coupon', verbose_name=_('Купон'), blank=True, null=True, related_name='orders')
    partner = models.ForeignKey('partners.Partner', verbose_name=_('Партнер'), blank=True, null=True)

    starttime = models.DateTimeField(blank=True, null=True, verbose_name=_('Удобное время от'), db_index=True)
    endtime = models.DateTimeField(blank=True, null=True, verbose_name=_('Удобное время до'))
    as_soon_as_possible = models.BooleanField(default=False, verbose_name=_('Как можно быстрее'), db_index=True)
    comment = models.CharField(_('Комментарий'), max_length=255, null=True, blank=True)
    show_user_contacts = models.BooleanField(_('Показывать контакты'), default=False)

    is_warranty = models.BooleanField(_('Гарантийный'), default=False)
    parent = models.ForeignKey('self', verbose_name=_('Родительский'), blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Заказ создан'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Заказ обновлен'))
    completed_at = models.DateTimeField(verbose_name=_('Заказ выполнен'), null=True, blank=True)

    class Meta:
        verbose_name = _('Заказ ремонта')
        verbose_name_plural = _('Заказы ремонта')
        ordering = ('-starttime', )

    def __init__(self, *args, **kwargs):
        super(RepairOrder, self).__init__(*args, **kwargs)
        self._original_master_id = self.master_id
        self._original_status = self.status

    def __str__(self):
        return _('Заказ #{}').format(self.number)

    def save(self, *args, **kwargs):
        from .bl import process_order_done
        if self._original_status and self._original_status != self.status and self.status == RepairOrder.STATUS_DONE and self.completed_at:
                process_order_done(self)
        super(RepairOrder, self).save(*args, **kwargs)

    @property
    def device_name(self):
        return self.device_model_color.__str__()

    @property
    def model(self):
        return self.device_model_color.device_model

    @property
    def total_amount(self):
        if getattr(self, '_total_amount', None) is None:
            self._total_amount = sum(i.price for i in self.repair_order_items.all())
        return self._total_amount

    @property
    def order_name(self):
        return '{} {}'.format(self.device_model_color.device_model.device.name, self.device_model_color.device_model.name)

    def get_absolute_url(self):
        return 'http://{}/master/#/order/{}'.format(settings.HOST_URL, self.number)

    def get_order_parts(self):
        return RepairOrderItemPart.objects.select_related('part').filter(
            Q(part__colors__isnull=True) | Q(part__colors=self.device_model_color.device_color),
            repair_order_item__in=self.repair_order_items.all()
        )

    def get_commission(self):
        if not self.master:
            return 0
        if getattr(self, '_commission', None) is None:
            comission_transactions = Transaction.objects.filter(
                object_id=self.id,
                sender=self.master.user,
                type=Transaction.REPAIR_ORDER_COMMISSION
            )
            commission_sum = comission_transactions.aggregate(Sum('amount'))['amount__sum'] or 0
            reverse_commission_sum = Transaction.objects.filter(
                object_id=self.id,
                recipient=self.master.user,
                type=Transaction.REVERSE,
                parent__in=comission_transactions
            ).aggregate(Sum('amount'))['amount__sum'] or 0
            self._commission = commission_sum - reverse_commission_sum
        return self._commission

    def get_master_commission(self, master=None):
        if self.is_warranty:
            return 0
        from .bl import get_repair_order_comission
        if master:
            commission = get_repair_order_comission(self, master)
        else:
            commission = get_repair_order_comission(self)
        commission_with_discount = commission - self.discount
        return commission_with_discount if commission_with_discount > 0 else 0

    def get_max_master_order_commission(self):
        from .bl import get_repair_order_comission_by_status
        max_status = MasterStatus.get_max()
        return get_repair_order_comission_by_status(self, max_status)

    @property
    def discount(self):
        if not self.coupon:
            return 0
        return self.get_discount_for_coupon(self.coupon)

    def get_discount_for_coupon(self, coupon):
        if coupon.discount:
            return coupon.discount
        if coupon.discount_percent:
            return round(self.total_amount * (Decimal(coupon.discount_percent) / 100))

    @property
    def total_amount_with_discount(self):
        return self.total_amount - self.discount

    @property
    def is_rejected(self):
        return True if self.rejected.count() > 0 else False

    @property
    def repairs_str(self):
        return ', '.join(self.device_model_repair.all().values_list('repair__name', flat=True))

    def is_done(self):
        return self.status == self.STATUS_DONE


class RepairOrderNotifiedMaster(models.Model):
    repair_order = models.ForeignKey(RepairOrder, related_name='notified_masters', verbose_name=_('Заказ'))
    master = models.ForeignKey('masters.Master', related_name='notified_orders', verbose_name=_('Мастер'))

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Оповещен мастер')
        verbose_name_plural = _('Оповещены мастера')


class RepairOrderItem(models.Model):
    repair_order = models.ForeignKey('RepairOrder', verbose_name=_('Заказ'), related_name='repair_order_items')
    device_model_repair = models.ForeignKey('devices.DeviceModelRepair', verbose_name=_('Поломка'))
    price = models.DecimalField(max_digits=14, decimal_places=2, verbose_name=_('Цена'))

    class Meta:
        verbose_name = _('Элемент заказа')
        verbose_name_plural = _('Элементы заказа')

    def __str__(self):
        return _('Отремонтированная поломка {} (цена = {})').format(self.id, self.price)

    @property
    def title(self):
        return '{model} - {repair} - {price}'.format(
            model=self.device_model_repair.device_model,
            repair=self.device_model_repair.repair,
            price=self.price
        )

    def get_master_commission(self, master, warranty=True):
        if warranty and self.repair_order.is_warranty:
            return 0
        status = master.get_status()
        commission = None
        if self.repair_order.address:
            commission = self.device_model_repair.commissions.filter(
                region__dadata_name=self.repair_order.address.region,
                status=status
            ).first() or self.device_model_repair.commissions.filter(
                region=Region.get_default(),
                status=status
            ).first()
        else:
            log.warning('Repair order (%s) has no address', self.repair_order)
        if not commission:
            return 0
        if commission.percentage:
            return self.price * (Decimal(commission.commission) / 100)
        else:
            return commission.commission


class RepairOrderItemPart(models.Model):
    repair_order_item = models.ForeignKey(RepairOrderItem)
    part = models.ForeignKey('devices.Part', verbose_name=_('Запчасть'))
    price = models.DecimalField(max_digits=14, decimal_places=2, verbose_name=_('Цена'))
    quantity = models.IntegerField(_('Количество'), default=1)

    class Meta:
        verbose_name = _('Запчасть єлемента заказа')
        verbose_name_plural = _('Запчасти єлементов заказа')

    def __str__(self):
        return self.part.name


class RepairOrderNotExistsPart(models.Model):
    repair_order = models.ForeignKey('RepairOrder', verbose_name=_('Заказ'), related_name='not_exists_part')
    part = models.ForeignKey('devices.Part', verbose_name=_('Запчасть'))


class SellOrder(models.Model):
    STATUS_NEW = 'new'
    STATUS_DONE = 'done'
    STATUS_CANCELLED = 'cancelled'
    STATUS_VOID = 'void'

    STATUS_CHOICES = (
        (STATUS_NEW, _('Новый')),
        (STATUS_DONE, _('Выполнен')),
        (STATUS_CANCELLED, _('Отменён')),
        (STATUS_VOID, _('Void')),
    )

    device_model_sell = models.ForeignKey('devices.DeviceModelSell')
    price = models.DecimalField(max_digits=14, decimal_places=2, verbose_name=_('Цена'))

    user = models.ForeignKey('accounts.User', verbose_name=_('Пользователь'), null=True, on_delete=models.SET_NULL)
    address = models.ForeignKey('accounts.Address', verbose_name=_('Адресс'), null=True, on_delete=models.SET_NULL)
    status = models.CharField(_('Статус'), max_length=255, default=STATUS_NEW, choices=STATUS_CHOICES)

    starttime = models.DateTimeField(blank=True, null=True, verbose_name=_('Удобное время от'))
    endtime = models.DateTimeField(blank=True, null=True, verbose_name=_('Удобное время до'))
    as_soon_as_possible = models.BooleanField(default=False, verbose_name=_('Как можно быстрее'))

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Заказ создан'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Заказ обновлен'))

    class Meta:
        verbose_name = _('Заказ продажи')
        verbose_name_plural = _('Заказы продажи')
        ordering = ('-starttime', )


class RejectedOrder(models.Model):
    repair_order = models.ForeignKey(RepairOrder, related_name='rejected', verbose_name=_('Заказ ремонта'))
    master = models.ForeignKey('masters.Master', related_name='rejected_orders', verbose_name=_('Мастер'))
    comment = models.CharField(_('Комментарий'), max_length=255)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Cоздан'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Обновлен'))

    class Meta:
        verbose_name = _('Отказ от заказа')
        verbose_name_plural = _('Отказы от заказов')


class OrderReviewInvite(models.Model):
    code = models.CharField(max_length=255, verbose_name=_('Инвайт код'), default=utils.generate_review_code_code)
    repair_order = models.ForeignKey(RepairOrder, verbose_name=_('Заказ ремонта'))

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    time_accepted = models.DateTimeField(_('Date accepted'), null=True, editable=False)
    expired = models.BooleanField(_('Expired'), db_index=True, default=False)

    class Meta:
        verbose_name = _('Инвайт на отзыв за работу мастера')
        verbose_name_plural = _('Инвайты на отзыв за работу мастера')

    @property
    def user(self):
        return self.repair_order.user

    def accept(self):
        self.expired = True
        self.time_accepted = utc_now()
        self.save()


class Coupon(models.Model):
    REGULAR_TYPE = 'regular'
    REPEATED_ORDER_TYPE = 'repeated_order'
    TYPE_CHOICES = (
        (REGULAR_TYPE, 'Обычный'),
        (REPEATED_ORDER_TYPE, 'Повторный заказ')
    )

    code = models.CharField(max_length=255, default=utils.generate_coupon_code)
    discount = models.IntegerField(_('Скидка фиксированная'), blank=True, null=True)
    discount_percent = models.IntegerField(_('Скидка в процентах'), blank=True, null=True)

    use_count = models.IntegerField('Количество раз использования', blank=True, null=True)
    expiration_date = models.DateTimeField(_('Срок действия'), blank=True, null=True)
    type = models.CharField(_('Тип'), max_length=255, choices=TYPE_CHOICES, default=REGULAR_TYPE)
    auto_appliable = models.BooleanField(_('Применять автоматически?'), default=False)

    device_models = models.ManyToManyField('devices.DeviceModel', blank=True, verbose_name=_('Модели'))
    repairs = models.ManyToManyField('devices.Repair', blank=True, verbose_name=_('Поломки'))
    description = models.CharField(_('Описание'), max_length=255, blank=True, null=True)

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    expired = models.BooleanField(_('Expired'), default=False)

    class Meta:
        verbose_name = _('Купон на скидку')
        verbose_name_plural = _('Купоны на скидку')

    def __str__(self):
        return self.code

    @property
    def used_count(self):
        return self.orders.count()

    def is_valid(self):
        return (
            self.expired is False
            and (not self.expiration_date or self.expiration_date > utc_now())
            and (not self.use_count or self.used_count < self.use_count)
        )

    def accept(self):
        if self.used_count == self.use_count:
            self.expired = True
            self.save()


@receiver(post_save, sender=RejectedOrder)
def register_master_to_update_rating_if_needed_for_rejected_order(sender, instance, created, **kwargs):
    from masters.rating import register_master_to_update_rating
    if instance.master_id:
        register_master_to_update_rating(instance.master_id)


@receiver(post_delete, sender=RejectedOrder)
def register_master_to_update_rating_if_needed_for_rejected_order_delete(sender, instance, **kwargs):
    from masters.rating import register_master_to_update_rating
    if instance.master_id:
        register_master_to_update_rating(instance.master_id)


@receiver(post_save, sender=SellOrder)
def notify_admin_about_new_sell_order(sender, instance, created, **kwargs):
    if created:
        email.new_sell_order_to_admin(instance)


@receiver(post_save, sender=RepairOrder)
def order_post_save_signal_handler(sender, instance, created, **kwargs):
    from .bl import create_reverse_transactions_for_void_repair_order
    if instance._original_status != instance.status and instance.status == RepairOrder.STATUS_VOID:
        create_reverse_transactions_for_void_repair_order(instance)


@receiver(post_save, sender=RepairOrder)
def register_master_to_update_rating_if_needed(sender, instance, created, **kwargs):
    from masters.rating import register_master_to_update_rating
    if instance.master_id:
        register_master_to_update_rating(instance.master_id)


@receiver(post_save, sender=RepairOrder)
def sent_new_order_notification_to_masters(sender, instance, created, **kwargs):
    from .tasks import notify_masters_about_new_order
    if instance.is_warranty and instance._original_status != instance.status and instance.status == RepairOrder.STATUS_NEW:
        notify_masters_about_new_order.delay(instance.id)
